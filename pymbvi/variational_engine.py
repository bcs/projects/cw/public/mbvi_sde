# Gradient based variational engine vor moment-based approximation of Markov processes 

import numpy as np
from scipy.interpolate import interp1d
from scipy.integrate import solve_ivp, quad, quad_vec
import pymbvi.util as ut
from copy import deepcopy
import time
import sys


class BaseVariationalEngine(object):

    def set_tspan(self, tspan):
        """
        Compute a default time step depending on the observation times
        """
        if (tspan is None):
            delta = (self.obs_times.max()-self.obs_times.min())/self.num_intervals
            t_min = self.obs_times.min()-delta
            t_max = self.obs_times.max()+delta
            tspan = np.array([t_min, t_max])
        return tspan

    def set_num_intervals(self):
        try:
            num_intervals = len(self.obs_times)+1
        except TypeError:
            num_intervals = 1
        return(num_intervals)

    def set_num_obs(self):
        try:
            num_obs = len(self.obs_times)
        except TypeError:
            num_obs = 0
        return(num_obs)

    def initialize_initial(self):
        if isinstance(self.initial_model, np.ndarray):
            initial = self.initial_model.copy()
        else:
            try:
                initial = self.initial_model.get_moments()
            except AttributeError:
                raise AttributeError('initial_model must be numpy ndarray or object with member function get_moments')
        return(initial)

    def initialize_rates(self):
        """
        Initialize the rate vector
        """
        rates = np.array(self.transition_model.rates)
        return(rates)


class VariationalEngine(object):
    """
    Class for running the variational inference procedure
    """

    # construction and setup

    def __init__(self, initial_model, transition_model, obs_model, obs_times=None, obs_data=None, time_grid=None, num_controls=10, subsample=100, tspan=None, options={}):
        """ 
        Constructor method
        Input
            initial_model: the prior distribution, can be a single vector of the forward function
            model: underlying ctmc model
            obs_model: observation model
            obs_times: array of observation times
            obs_data: array of observation vectors 
            options: a dictionary with options. currently supported options
                    'mode': 'smoothing', 'inference'
        """
        # store stuff
        self.initial_model = initial_model
        self.transition_model = transition_model
        self.obs_model = obs_model
        self.obs_times = obs_times
        self.obs_data = obs_data
        self.num_intervals = self.set_num_intervals()
        self.num_obs = self.set_num_obs()
        # set additional stuff
        self.num_controls = num_controls
        self.subsample = subsample
        self.tspan = self.set_tspan(tspan)
        # set up model and variational parameters
        self.initial = self.initialize_initial()
        self.rates = self.initialize_rates()
        self.obs_param = self.obs_model.get_parameters()
        self.control = np.zeros((self.num_intervals, self.num_controls, self.transition_model.num_controls()))
        # gradients of model and variational parameters
        self.initial_gradient = np.zeros(self.initial.shape)
        self.control_gradient = np.zeros((self.num_intervals, self.num_controls, self.transition_model.num_controls()))
        self.rates_gradient = np.zeros(self.rates.shape) #np.zeros((self.transition_model.num_param()))
        self.obs_param_gradient = None
        # containers for dynamic functions
        self.time = self.set_time()
        self.forward = np.zeros((self.num_intervals, self.num_controls, self.subsample+1, self.transition_model.num_states()))
        self.backward = np.zeros((self.num_intervals, self.num_controls, self.subsample+1, self.transition_model.num_states()))
        # supbarts of the full objective function
        self.initial_kl = np.array(0.0)
        self.kl = np.zeros((self.num_intervals, self.num_controls, self.subsample+1))
        self.residual = np.zeros(self.num_obs)
        self.log_rates_prior = np.array(0.0)
        self.log_obs_param_prior = np.array(0.0)
        self.elbo = np.array(0.0)
        # set options
        self.options = self.set_options(options)

    def set_num_intervals(self):
        try:
            num_intervals = len(self.obs_times)+1
        except TypeError:
            num_intervals = 1
        return(num_intervals)

    def set_num_obs(self):
        try:
            num_obs = len(self.obs_times)
        except TypeError:
            num_obs = 0
        return(num_obs)

    def set_options(self, options):
        """
        Set standard argument for the options
        Set interface function according to chosen options
        """
        # set default options
        options.setdefault('smoothing', True)
        options.setdefault('inference', False)
        options.setdefault('varinitial', False)
        options.setdefault('obs_inference', False)
        options.setdefault('gradient_regularizer', 1e-6)
        options.setdefault('solver_method', 'RK45')
        # set interface functions according to options
        # joint smoothing, initial estimation, dynamic parameter inference and observation parameter inference
        if (options['smoothing'] and options['inference'] and options['varinitial'] and options['obs_inference']):
            self.gradient_update = self.gradient_update_initial_smoothing_inference_obs
            self.objective_function = self.objective_function_initial_smoothing_inference_obs
        # joint smoothing, initial estimation and parameter inference
        elif (options['smoothing'] and options['inference'] and options['varinitial'] and not options['obs_inference']):
            self.gradient_update = self.gradient_update_initial_smoothing_inference
            self.objective_function = self.objective_function_initial_smoothing_inference
        # joint smoothing and initial estimation
        elif (options['smoothing'] and not options['inference'] and options['varinitial'] and not options['obs_inference']):
            self.gradient_update = self.gradient_update_initial_smoothing
            self.objective_function = self.objective_function_initial_smoothing
        # joint smoothing and inferene
        elif (options['smoothing'] and options['inference'] and not options['varinitial'] and not options['obs_inference']):
            self.objective_function = self.objective_function_smoothing_inference
            self.gradient_update = self.gradient_update_smoothing_inference
        # smoothing only
        elif (options['smoothing'] and not options['inference'] and not options['varinitial'] and not options['obs_inference']):
            self.objective_function = self.objective_function_smoothing
            self.gradient_update = self.gradient_update_smoothing
        # inference only
        elif (not options['smoothing'] and options['inference'] and not options['varinitial'] and not options['obs_inference']):
            self.objective_function = self.objective_function_inference
            self.gradient_update = self.gradient_update_inference
        # initial only
        elif (not options['smoothing'] and not options['inference'] and options['varinitial'] and not options['obs_inference']):
            self.objective_function = self.objective_function_initial
            self.gradient_update = self.gradient_update_initial
        # observation parameters only
        elif (not options['smoothing'] and not options['inference'] and not options['varinitial'] and options['obs_inference']):
            self.objective_function = self.objective_function_obs
            self.gradient_update = self.gradient_update_obs
        else:
            raise ValueError('Invalid options')
        return(options)

    def set_tspan(self, tspan):
        """
        Compute a default time step depending on the observation times
        """
        if (tspan is None):
            delta = (self.obs_times.max()-self.obs_times.min())/self.num_intervals
            t_min = self.obs_times.min()-delta
            t_max = self.obs_times.max()+delta
            tspan = np.array([t_min, t_max])
        return tspan

    def set_time(self):
        """
        Construct timte grid
        """
        if self.obs_times is not None:
            start = np.concatenate([self.tspan[[0]], self.obs_times])
            end = np.concatenate([self.obs_times, self.tspan[[1]]])
        else:
            start = self.tspan[[0]]
            end = self.tspan[[1]]
        time = np.zeros((self.num_intervals, self.num_controls, self.subsample+1))
        for i in range(self.num_intervals):
            delta = np.linspace(start[i], end[i], self.num_controls+1)
            #print(delta)
            for j in range(self.num_controls):
                time[i, j, :] = np.linspace(delta[j], delta[j+1], self.subsample+1)
        return(time)

    def initialize_initial(self):
        if isinstance(self.initial_model, np.ndarray):
            initial = self.initial_model.copy()
        else:
            try:
                initial = self.initial_model.get_moments()
            except AttributeError:
                raise AttributeError('initial_model must be numpy ndarray or object with member function get_moments')
        return(initial)

    def initialize_rates(self):
        """
        Initialize the rate vector
        """
        rates = np.array(self.transition_model.rates)
        return(rates)

    def initialize_control(self, time, control):
        """
        Initialize the control to the prior rates
        If no control is provided, use prior rates
        """
        tmp = interp1d(time, control, axis=0)(self.time[:, :, 0].flatten())
        self.control = tmp.reshape(self.control.shape)
        return

    def initialize(self, time=None, control=None):
        if time is None and control is None:
            time = self.tspan
            control = np.zeros((2, self.transition_model.num_controls()))
        # initialize from rates
        self.initialize_control(time, control)
        return

    def update_observations(self, obs_times, obs_data, tspan):
        # save current stuff
        time = self.time[:, :, 0].flatten()
        control = self.control.copy().reshape((-1, self.control.shape[-1])) 
        # update observation and related
        self.tspan = tspan
        self.obs_times = obs_times
        self.obs_data = obs_data
        self.num_intervals = len(obs_times)+1
        self.time = self.set_time()
        self.forward = np.zeros((self.num_intervals, self.num_controls, self.subsample+1, self.transition_model.num_states()))
        self.backward = np.zeros((self.num_intervals, self.num_controls, self.subsample+1, self.transition_model.num_states()))
        self.control_gradient = np.zeros((self.num_intervals, self.num_controls, self.transition_model.num_controls()))
        self.residual = np.zeros(len(obs_times))
        self.num_obs = self.set_num_obs()
        # set control
        new_time = self.time[:, :, 0].flatten()
        new_control = np.zeros((self.num_intervals*self.num_controls, self.transition_model.num_controls()))
        ind = new_time <= time[-1]
        tmp = interp1d(time, control, kind='zero', axis=0)(new_time[ind])
        new_control[ind, :] = tmp
        self.control = new_control.reshape((self.num_intervals, self.num_controls, self.transition_model.num_controls()))
        return
    
    # main dynamic functions

    def forward_update(self):
        try:
            # preparations
            if isinstance(self.initial_model, np.ndarray):
                initial = self.initial.copy()
            else:
                initial = self.initial_model.get_moments(self.initial)
            # iterate over the rest
            for i in range(self.num_intervals):
                for j in range(self.num_controls):
                    # wrap function for fixed control
                    def odefun(time, state):
                        return(self.transition_model.forward(time, state, self.control[i, j], self.rates))
                    tspan = self.time[i, j, [0, -1]]
                    sol = solve_ivp(odefun, tspan, initial, t_eval=self.time[i, j], method=self.options['solver_method'])
                    self.forward[i, j] = sol['y'].transpose()
                    initial = sol['y'][:, -1]
        except ValueError as e:
            ident = 'could not broadcast input array from shape'
            if ident in str(e):
                msg = 'Forward pass failed.'
                raise RuntimeError(msg)
                
    def backward_update(self):
        try:
            # set constraint in last interval 
            terminal = np.zeros(self.transition_model.num_states())
            for j in reversed(range(self.num_controls)):
                # wrap function for fixed control
                def odefun(time, state):
                    return(self.transition_model.backward(time, state, self.control[-1, j], self.time[-1, j], self.forward[-1, j], self.rates))
                tspan = self.time[-1, j, [-1, 0]]
                sol = solve_ivp(odefun, tspan, terminal, t_eval=self.time[-1, j, ::-1], method=self.options['solver_method'])
                self.backward[-1, j] = sol['y'].transpose()[::-1, :]
                terminal = sol['y'][:, -1]
            # iterate backward
            for i in reversed(range(self.num_intervals-1)):
                # update terminal state with observations
                terminal += self.obs_model.get_terminal(self.forward[i, -1, -1], self.obs_data[i], self.obs_times[i], self.obs_param)
                for j in reversed(range(self.num_controls)):
                    # wrap function for fixed control
                    def odefun(time, state):
                        return(self.transition_model.backward(time, state, self.control[i, j], self.time[i, j], self.forward[i, j], self.rates))
                    tspan = self.time[i, j, [-1, 0]]
                    sol = solve_ivp(odefun, tspan, terminal, t_eval=self.time[i, j, ::-1], method=self.options['solver_method'])
                    self.backward[i, j] = sol['y'].transpose()[::-1, :]
                    terminal = sol['y'][:, -1]
        except ValueError as e:
            ident = 'could not broadcast input array from shape'
            if ident in str(e):
                msg = 'Backward pass failed.'
                raise RuntimeError(msg)

    def gradient_update_smoothing(self):
        """
        Gradient update for smoothing only
        """
        self.control_gradient = self.transition_model.control_gradient(self.time, self.control, self.forward, self.backward, self.rates)
        return

    def gradient_update_initial(self):
        """
        Update the gradient with respect to the initial condition
        """
        if isinstance(self.initial_model, np.ndarray):
            self.initial_gradient = -self.backward[0, 0, 0]
        else:
            self.initial_gradient = self.initial_model.gradient(self.initial, self.backward[0, 0, 0])
        return

    def gradient_update_inference(self):
        """
        Gradient update for smoothing only
        """
        self.rates_gradient = self.transition_model.rates_gradient(self.time, self.control, self.forward, self.backward, self.rates)
        return

    def gradient_update_obs_old(self):
        """
        Gradient update for learning observation parameters
        """
        tmp = np.zeros((len(self.obs_times), self.obs_model.num_param()))
        for i in range(len(self.obs_times)):
            tmp[i] = self.obs_model.gradient(self.forward[i, -1, -1, :], self.obs_data[i], self.obs_times[i], self.obs_param)
        self.obs_param_gradient = tmp.sum(axis=0)
        return

    def gradient_update_obs(self):
        """
        Gradient update for learning observation parameters
        """
        self.obs_param_gradient = self.obs_model.gradient(self.forward, self.obs_data, self.obs_times, self.obs_param)
        return
    
    def gradient_update_initial_smoothing(self):
        """
        Update gradient w.r.t. controls and initial
        """
        self.gradient_update_initial()
        self.gradient_update_smoothing()
        return

    def gradient_update_smoothing_inference(self):
        """
        Gradient update for joint smoothing and inference
        """
        self.control_gradient, self.rates_gradient = self.transition_model.joint_gradient(self.time, self.control, self.forward, self.backward, self.rates, self.options)
        return

    def gradient_update_initial_smoothing_inference(self):
        """
        Gradient update for joint smoothing, inference and initial estimation
        """
        self.gradient_update_initial()
        self.gradient_update_smoothing_inference()
        return

    def gradient_update_initial_smoothing_inference_obs(self):
        """
        Gradient update for joint smoothing, inference and initial estimation
        """
        self.gradient_update_initial()
        self.gradient_update_smoothing_inference()
        self.gradient_update_obs()
        return

    # objective function evaluation

    def eval_functional(self):
        """
        evaluate the objective function based on the current combination of initial and control
        """
        try:
            self.forward_update()
            self.evaluate_initial_kl()
            self.evaluate_kl()
            self.evaluate_residuals()
            self.evaluate_rates_prior()
            self.evaluate_obs_param_prior()
            self.elbo = self.kl.sum()+self.residual.sum()+self.initial_kl+self.log_rates_prior+self.log_obs_param_prior
        except RuntimeError as e:
            if str(e) == 'Forward pass failed.':
                print('Warning: '+ str(e))
                self.elbo = np.inf
            if str(e) == 'Backward pass failed.':
                print('Warning: '+ str(e))
                self.elbo = np.inf
        return

    def eval_functional_grad(self):
        """
        evaluate the objective function based on the current combination of initial and control
        also evaluate the gradient
        """
        try: 
            self.forward_update()
            self.backward_update()
            self.evaluate_kl()
            self.evaluate_initial_kl()
            self.evaluate_residuals()
            self.gradient_update()
            self.evaluate_rates_prior()
            self.evaluate_obs_param_prior()
            # print("kl: ", self.kl.sum())
            # print("residual: ", self.residual.sum())
            self.elbo = self.kl.sum()+self.residual.sum()+self.initial_kl+self.log_rates_prior+self.log_obs_param_prior
        except RuntimeError as e:
            if str(e) == 'Forward pass failed.':
                print('Warning: '+ str(e))
                self.elbo = np.inf
            if str(e) == 'Backward pass failed.':
                print('Warning: '+ str(e))
                self.elbo = np.inf
        return

    def objective_function_smoothing(self, control, get_gradient=True):
        # set control
        if isinstance(control, list):
            self.control = control[0]
        elif isinstance(control, np.ndarray):
            self.control = control
        else:
            msg = f'Unsupported type {type(control)} for argument control'
            raise TypeError(msg)
        # evaluate backward integration only if gradient information is required 
        if get_gradient:
            # update all sub-components
            self.eval_functional_grad()
            # get objective function value
            elbo = self.elbo
            # get gradient and remove nans
            grad = self.control_gradient.copy()
            grad[np.isnan(grad)] = 0.0
            if isinstance(control, list):
                return(elbo, [grad])
            elif isinstance(control, np.ndarray):
                return(elbo, grad)
        else:
            # update required sub-components
            self.eval_functional()
            # get objecive function value
            elbo = self.elbo
        return(elbo)

    def objective_function_initial(self, initial, get_gradient=True):
        # set initial
        if isinstance(initial, list):
            self.initial = initial[0]
        elif isinstance(initial, np.ndarray):
            self.initial = initial
        else:
            msg = f'Unsupported type {type(initial)} for argument control'
            raise TypeError(msg)
        if get_gradient:
            # update all sub-components
            self.eval_functional_grad()
            # get objective function value
            elbo = self.elbo
            # get gradient and remove nans
            grad = self.initial_gradient.copy()
            grad[np.isnan(grad)] = 0.0
            return(elbo, grad)
        else:
            # update required sub-components
            self.eval_functional()
            # get objecive function value
            elbo = self.elbo
            return(elbo)

    def objective_function_obs(self, arg, get_gradient=True):
        if isinstance(arg, list):
            self.obs_param = arg[0]
        elif isinstance(arg, np.ndarray):
            self.obs_param = arg
        else:
            msg = f'Unsupported type {type(arg)} for argument control'
            raise TypeError(msg)
        # set observation parameters
        if get_gradient:
            # update all sub-components
            self.eval_functional_grad()
            # get objective function value
            elbo = self.elbo
            # get gradient and remove nans
            grad = self.obs_param_gradient.copy()
            grad[np.isnan(grad)] = 0.0
            return(elbo, grad)
        else:
            # update required sub-components
            self.eval_functional()
            # get objecive function value
            elbo = self.elbo
            return(elbo)

    def objective_function_inference(self, arg, get_gradient=True):
        # set observation parameters
        if isinstance(arg, list):
            self.rates_gradient = arg[0]
        elif isinstance(arg, np.ndarray):
            self.rates = arg
        else:
            msg = f'Unsupported type {type(arg)} for argument control'
            raise TypeError(msg)    
        if get_gradient:
            # update all sub-components
            self.eval_functional_grad()
            # get objective function value
            elbo = self.elbo
            # get gradient and remove nans
            grad = self.rates_gradient.copy()
            grad[np.isnan(grad)] = 0.0
            return(elbo, grad)
        else:
            # update required sub-components
            self.eval_functional()
            # get objecive function value
            elbo = self.elbo
            return(elbo)

    def objective_function_initial_smoothing(self, arg, get_gradient=True):
        # set initial and control
        self.initial = arg[0]
        self.control = arg[1]
        # evaluate backward integration only if gradient information is required 
        if get_gradient:
            # update all sub-components
            self.eval_functional_grad()
            # get objective function value
            elbo = self.elbo
            # get gradient and remove nans
            grad = [self.initial_gradient.copy(), self.control_gradient.copy()]
            grad[0][np.isnan(grad[0])] = 0.0
            grad[1][np.isnan(grad[1])] = 0.0
            return(elbo, grad)
        else:
            # update required sub-components
            self.eval_functional()
            # get objecive function value
            elbo = self.elbo
            return(elbo)

    def objective_function_smoothing_inference(self, arg, get_gradient=True):
        # set control and rates
        self.control = arg[0]
        self.rates = arg[1]
        # evaluate backward integration only if gradient information is required 
        if get_gradient:
            # update all sub components
            self.eval_functional_grad()
            # get objective function value
            elbo = self.elbo
            # update the gradient (uses stats)
            grad = [self.control_gradient.copy(), self.rates_gradient.copy()]
            grad[0][np.isnan(grad[0])] = 0.0
            grad[1][np.isnan(grad[1])] = 0.0
            return(elbo, grad)
        else:
            # update required sub-components
            self.eval_functional()
            # get objective function value
            elbo = self.elbo
            return(elbo)
    
    def objective_function_initial_smoothing_inference(self, arg, get_gradient=True):
        # set initial, control and rates
        self.initial = arg[0]
        self.control = arg[1]
        self.rates = arg[2]
        # evaluate backward integration only if gradient information is required 
        if get_gradient:
            # update all sub components
            self.eval_functional_grad()
            # get objective function value
            elbo = self.elbo
            # update the gradient (uses stats)
            grad = [self.initial_gradient.copy(), self.control_gradient.copy(), self.rates_gradient.copy()]
            grad[0][np.isnan(grad[0])] = 0.0
            grad[1][np.isnan(grad[1])] = 0.0
            grad[2][np.isnan(grad[2])] = 0.0
            return(elbo, grad)
        else:
            # update required sub-components
            self.eval_functional()
            # get ojbective function value
            elbo = self.elbo
            return(elbo)

    def objective_function_initial_smoothing_inference_obs(self, arg, get_gradient=True):
        # set initial, control and rates
        self.initial = arg[0]
        self.control = arg[1]
        self.rates = arg[2]
        self.obs_param = arg[3]
        # evaluate backward integration only if gradient information is required 
        if get_gradient:
            # update all sub components
            self.eval_functional_grad()
            # get objective function value
            elbo = self.elbo
            # update the gradient (uses stats)
            grad = [self.initial_gradient.copy(), self.control_gradient.copy(), self.rates_gradient.copy(), self.obs_param_gradient.copy()]
            grad[0][np.isnan(grad[0])] = 0.0
            grad[1][np.isnan(grad[1])] = 0.0
            grad[2][np.isnan(grad[2])] = 0.0
            grad[3][np.isnan(grad[3])] = 0.0
            return(elbo, grad)
        else:
            # update required sub-components
            self.eval_functional()
            # get ojbective function value
            elbo = self.elbo
            return(elbo)

    def evaluate_residuals(self):
        """
        For each observation point, compute the residual contribution 
        corresonding to the expected negative log likliehood of the observation model
        """
        for i in range(self.num_obs):
            self.residual[i] = self.obs_model.get_residual(self.forward[i, -1, -1, :], self.obs_data[i], self.obs_times[i], self.obs_param)

    # evaluate the evidence lower bound
    def evaluate_kl(self):
        """
        Evaluate objective function based on stored values in stats
        """
        # prior kl contribution
        self.kl = self.transition_model.kl_prior(self.time, self.control, self.forward, self.rates)
        return

    def evaluate_initial_kl(self):
        """
        Evaluate the divergence of variational and prior initial
        - if initial_model is an array, the initial is treated as known
          learning the initial corresponds to learning both prior and posterior
          kl contribution is zero
        - if we have a fixed prior model we compute the kl
        """
        if isinstance(self.initial_model, np.ndarray):
            self.initial_kl = np.array(0.0)
        else:
            self.initial_kl = self.initial_model.kl(self.initial)
        return

    def evaluate_rates_prior(self):
        """
        Evaluate the contribution of the rates prior to the objective function
        """
        self.log_rates_prior = self.transition_model.log_prior(self.rates)
        return

    def evaluate_obs_param_prior(self):
        """
        Evaluate the contribution of the observation parameter prior to the objective function
        """
        self.log_obs_param_prior = self.obs_model.log_prior(self.obs_param)
        return

    # getters 

    def get_time(self):
        """
        Return time as an array of shape (time_steps,)
        """
        return(self.time.flatten().copy())

    def get_control(self):
        """
        Return forward states as an array of shape (time_steps, num_states)
        """
        return(self.control.copy().reshape(-1, self.transition_model.num_controls()))

    def get_forward(self):
        """
        Return forward states as an array of shape (time_steps, num_states)
        """
        return(self.forward.copy().reshape(-1, self.transition_model.num_states()))

    def get_backward(self):
        """
        Return backward states as an array of shape (time_steps, num_states)
        """
        return(self.backward.copy().reshape(-1, self.transition_model.num_states()))

    def get_gradient(self):
        """
        Return gradients as an array of shape (time_steps, num_controls)
        """
        return(self.control_gradient.copy().reshape(-1, self.transition_model.num_controls()))

    # store and restore

    def state_dict(self):
        state_dict = deepcopy(vars(self))
        return(state_dict)

    def load_state_dict(self, state_dict):
        for key, value in state_dict.items():
            setattr(self, key, value)
        self.options = self.set_options(self.options)
        return
           