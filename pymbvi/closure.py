import torch
import numpy as np

"""
This model contains moment closures schemes
"""

def lognorm_closure(m, M, alpha):
    """
    General lognormal closure E[X^\alpha] (multi-index notation)
    Input
        m: (n,) tensor of moments E[X]
        M: (n, n) tensor of moments E[XX^T]
        alpha: (d, n) tensor of powers for each component where d is the dimensionof the output
    """
    tmp1 = m**(2*alpha)/torch.diag(M)**(0.5*alpha)
    power = 0.5 * (alpha.unsqueeze(2) @ alpha.unsqueeze(1)).reshape((alpha.shape[0], -1))
    tmp2 = (M.flatten()/torch.ger(m, m).flatten())**power
    res = tmp1.prod(axis=1)*tmp2.prod(axis=1)
    return(res)


def gaussian_third_order(m, M, beta):
    """
    Gaussian closure for moments of the form E[X_i X_j X_k] for general dimensionality
    Input: 
        m: (n,) tensor of moments E[X]
        M: (n, n) tensor of moments E[(X-m)(X-m)^T]
        beta: (d, 3) tensor of powers for each component where d is the dimensionof the output    
    """
    # mean contribution
    res = m[beta[:, 0]] * m[beta[:, 1]] * m[beta[:, 2]]
    # mixed contributions
    res += M[beta[:, 0], beta[:, 1]] * m[beta[:, 2]]
    res += M[beta[:, 0], beta[:, 2]] * m[beta[:, 1]]
    res += M[beta[:, 1], beta[:, 2]] * m[beta[:, 0]]
    return(res)


def gaussian_fourth_order_central(m, M, beta):
    res = M[beta[:, 0], beta[:, 1]]*M[beta[:, 2], beta[:, 3]]
    res += M[beta[:, 0], beta[:, 2]]*M[beta[:, 1], beta[:, 3]]
    res += M[beta[:, 0], beta[:, 3]]*M[beta[:, 1], beta[:, 2]]
    return(res)

