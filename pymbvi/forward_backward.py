"""
Experimental class to implement forward-backward type smoothing
"""

import numpy as np
from scipy.interpolate import interp1d
from scipy.integrate import solve_ivp
import pymbvi.util as ut

# suppress zero division errors
np.seterr(divide='ignore')


class ForwardBackward(object):
    """
    Class for solving the discretized forward backward filter for a given model
    - the model is discritized over space only 
    - the resulting ODE is used via standard ODE solver
    - an observation model is required to perform the filtering updates
    """

    def __init__(self, initial, transition_model, obs_model, obs_times, obs_data, subsample=100, tspan=None):
        """ 
        Constructor method
        Input
            initial_model: the prior distribution, can be a single vector of the forward function
            model: underlying ctmc model
            obs_model: observation model
            obs_times: array of observation times
            obs_data: array of observation vectors 
            options: a dictionary with options. currently supported options
                    'mode': 'smoothing', 'inference'
        """
        # store stuff
        self.initial = initial
        self.transition_model = transition_model
        self.obs_model = obs_model
        self.obs_times = obs_times
        self.obs_data = obs_data
        self.num_intervals = len(obs_times)+1
        # # set additional stuff
        self.subsample = subsample
        self.tspan = self.set_tspan(tspan)
        # set up model and variational parameters
        self.rates = self.initialize_rates()
        self.obs_param = self.obs_model.get_parameters()
        # self.control = np.zeros((self.num_intervals, self.num_controls, self.transition_model.num_controls()))
        # # gradients of model and variational parameters
        # self.initial_gradient = np.zeros(self.initial.shape)
        # self.control_gradient = np.zeros((self.num_intervals, self.num_controls, self.transition_model.num_controls()))
        # self.rates_gradient = None #np.zeros((self.transition_model.num_param()))
        # self.obs_param_gradient = None
        # containers for dynamic functions
        self.time = self.set_time()
        self.forward = np.zeros((self.num_intervals, self.subsample+1, self.transition_model.get_num_states()))
        self.backward = np.ones((self.num_intervals, self.subsample+1, self.transition_model.get_num_states()))
        # # supbarts of the full objective function
        #self.obs_llh = np.zeros((self.num_intervals-1, self.transition_model.num_states()))
        #self.get_obs_llh()
        # self.initial_kl = np.array(0.0)
        # self.kl = np.zeros((self.num_intervals, self.num_controls, self.subsample+1))
        # self.residual = np.zeros(len(obs_times))
        # self.log_rates_prior = np.array(0.0)
        # self.log_obs_param_prior = np.array(0.0)
        # self.elbo = np.array(0.0)
        # # set options
        # self.options = self.set_options(options)

    # set up and initialization    

    def set_tspan(self, tspan):
        """
        Compute a default time step depending on the observation times
        """
        if (tspan is None):
            delta = (self.obs_times.max()-self.obs_times.min())/self.num_intervals
            t_min = self.obs_times.min()-delta
            t_max = self.obs_times.max()+delta
            tspan = np.array([t_min, t_max])
        return tspan

    def set_time(self):
        """
        Construct timte grid
        """
        start = np.concatenate([self.tspan[[0]], self.obs_times])
        end = np.concatenate([self.obs_times, self.tspan[[1]]])
        time = np.zeros((self.num_intervals, self.subsample+1))
        for i in range(self.num_intervals):
            time[i, :] = np.linspace(start[i], end[i], self.subsample+1)
        return(time)

    def initialize_rates(self):
        """
        Initialize the rate vector
        """
        rates = np.array(self.transition_model.rates)
        return(rates)

    # main functions

    def forward_update(self):
        # wrap function for fixed control
        def odefun(time, state):
            return(self.transition_model.forward(time, state, self.rates))
        # preparations
        initial = self.initial.copy()
        # iterate over the rest
        for i in range(self.num_intervals-1):
            tspan = self.time[i, [0, -1]]
            sol = solve_ivp(odefun, tspan, initial, t_eval=self.time[i])
            self.forward[i] = sol['y'].transpose()
            #initial = self.forward_obs_update(sol['y'][:, -1], self.obs_llh[i])
            initial = self.obs_model.forward_filter_update(self.obs_times[i], sol['y'][:, -1], self.obs_data[i], self.obs_param)
            #initial = sol['y'][:, -1].copy()
        tspan = self.time[-1, [0, -1]]
        sol = solve_ivp(odefun, tspan, initial, t_eval=self.time[-1])
        self.forward[-1] = sol['y'].transpose()
        return

    def backward_update(self):
        # wrap function for fixed control
        def odefun(time, state):
            return(self.transition_model.backward(time, state, self.rates))
        # # set constraint in last interval 
        # terminal = np.ones(self.initial.shape)
        # tspan = self.time[-1, [-1, 0]]
        # sol = solve_ivp(odefun, tspan, terminal, t_eval=self.time[-1, ::-1])
        # self.backward[-1] = sol['y'].transpose()[::-1, :]
        self.backward[-1] = np.ones(self.backward[-1].shape)
        # iterate backward
        for i in reversed(range(self.num_intervals-1)):
            # update terminal state with observations
            #terminal = self.backward_obs_update(self.backward[i+1, 0], self.obs_llh[i])
            terminal = self.obs_model.backward_filter_update(self.obs_times[i], self.backward[i+1, 0], self.obs_data[i], self.obs_param)
            tspan = self.time[i, [-1, 0]]
            sol = solve_ivp(odefun, tspan, terminal, t_eval=self.time[i, ::-1])
            self.backward[i] = sol['y'].transpose()[::-1, :]
        return

    def get_time(self):
        """
        Return time as an array of shape (time_steps,)
        """
        return(self.time.flatten())

    def get_forward(self):
        """
        Return forward states as an array of shape (time_steps, num_states)
        """
        return(self.forward.reshape(-1, self.transition_model.get_num_states()))

    def get_backward(self):
        """
        Return backward states as an array of shape (time_steps, num_states)
        """
        return(self.backward.reshape(-1, self.transition_model.get_num_states()))

    def get_smoothed(self):
        """
        Return smoothed states as an array of shape (time_steps, num_states)
        """
        tol = 1e-18
        self.forward[self.forward<tol] = tol
        self.backward[self.backward<tol] = tol
        smoothed = np.log(self.forward)
        smoothed += np.log(self.backward)
        smoothed = smoothed.reshape(-1, self.transition_model.get_num_states())
        max_val = np.max(smoothed, axis=1, keepdims=True)
        smoothed -= max_val
        np.exp(smoothed, smoothed) # inplace to prevent memory overflow
        norm = np.sum(smoothed, axis=1, keepdims=True)
        smoothed /= norm
        return(smoothed)


class FilterObsModel():

    def __init__(self, model, obs_model, t_obs, obs_data):
        self.t_obs = t_obs
        self.obs_data = obs_data
        self.model = model
        self.obs_model = obs_model
        self.llh_map = self.compute_llh_map(t_obs, obs_data)

    def llh_eff(self, time, obs_data, obs_param):
        return(self.llh_map[time])

    def forward_filter_update(self, time, state, obs_data, obs_param):
        state[state<0] = 0
        res = np.log(state) + self.llh_eff(time, obs_data, obs_param)
        max_res = np.max(res)
        res = np.exp(res-max_res)
        res = res / np.sum(res)
        check = np.linalg.norm(res-state)
        return(res)

    def backward_filter_update(self, time, state, obs_data, obs_param):
        state[state<0] = 0.0
        res = np.log(state) + self.llh_eff(time, obs_data, obs_param)
        max_res = np.max(res)
        res = np.exp(res-max_res)
        return(res)
        
    def compute_llh_map(self, t_obs, obs_data):
        # compute states
        #dim = self.model.dim
        num_states = self.model.get_num_states()
        state_map = self.model.get_states()
        # compute likelihoods
        llh_map = {}
        for i, t in enumerate(t_obs):
            llh_map[t] = self.obs_model.llh_vec(state_map, t, obs_data[i])
        return(llh_map)

    def get_parameters(self):
        return(self.obs_model.get_parameters())