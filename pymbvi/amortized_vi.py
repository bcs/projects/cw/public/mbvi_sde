"""
This module contains classes and helper functions to realize amortized variational inference.
The implementation is based on a wrapper around the variational engine in form
of a PyTorch autograd function
"""

import numpy as np 
import torch
import torch.multiprocessing as mp
from pymbvi.variational_engine import VariationalEngine


class MBVIFunction(torch.autograd.Function):
    """
    Autograd wrapper for a variational engine
    """

    @staticmethod
    def forward(ctx, control, obs_data, vi_engine):
        # convert to numpy and reshape
        control_np = control.detach().numpy().reshape(vi_engine.control.shape)
        obs_data_np = obs_data.numpy().reshape(vi_engine.obs_data.shape)
        # evlaute output and gradient using the variational engine
        vi_engine.obs_data = obs_data_np
        elbo_np, grad_np = vi_engine.objective_function(control_np)
        # convert back to torch, store, return
        elbo = torch.tensor(elbo_np)
        grad = torch.from_numpy(grad_np.flatten())
        ctx.save_for_backward(grad)
        return(elbo)

    @staticmethod
    def backward(ctx, grad_output):
        grad_control = ctx.saved_tensors[0]*grad_output
        grad_obs_data = None
        grad_vi_engine = None
        return(grad_control, grad_obs_data, grad_vi_engine)


class MBVI(torch.nn.Module):
    """
    Variational inference Module
    """

    def __init__(self, vi_engine):
        super(MBVI, self).__init__()
        self.vi_engine = vi_engine

    def forward(self, control, obs_data):
        return(MBVIFunction.apply(control, obs_data, self.vi_engine))


def vi_fun(ind, control, obs_data, elbo, control_grad, mbvi_batched):
    """
    this funtion is required to solve the vi_problem in parallel for different observations
    """
    # convert to numpy and reshape
    control_np = control[ind].detach().numpy().reshape(mbvi_batched.control_shape)
    obs_data_np = obs_data[ind].numpy().reshape(mbvi_batched.obs_shape)
    # set up variational engine
    vi_engine = VariationalEngine(mbvi_batched.initial, mbvi_batched.transition_model, mbvi_batched.obs_model, mbvi_batched.obs_times, obs_data_np, num_controls=mbvi_batched.num_controls, subsample=mbvi_batched.subsample, tspan=mbvi_batched.tspan, options=mbvi_batched.options)
    vi_engine.initialize()
    # evlaute output and gradient using the variational engine
    elbo_np, grad_np = vi_engine.objective_function(control_np)
    # convert back to torch, store, return
    elbo[ind] = torch.tensor(elbo_np)
    control_grad[ind] = torch.from_numpy(grad_np.flatten())
    return()


class MBVIFunctionBatched(torch.autograd.Function):
    """ 
    Minibatch wrapper of the autograd module
    """

    @staticmethod
    def forward(ctx, control, obs_data, mbvi_batched):
        # get batch size
        batch_size = obs_data.shape[0]
        elbo = torch.zeros(batch_size)
        elbo.share_memory_()
        control_grad = torch.zeros(control.shape)
        control_grad.share_memory_()
        # create multiple instances
        processes = []
        for j in range(batch_size):
            p = mp.Process(target=vi_fun, args=(j, control.detach(), obs_data, elbo, control_grad, mbvi_batched))
            p.start()
            processes.append(p)
        for p in processes:
            p.join()
        ctx.save_for_backward(control_grad)
        print(control_grad)
        return(elbo.sum())

    @staticmethod
    def backward(ctx, grad_output):
        grad_control = ctx.saved_tensors[0]*grad_output
        return(grad_control, None, None)


class MBVIbatched(torch.nn.Module):
    """
    Similar to the MBVI class but using multiprocessing
    to deal with minibatches
    """

    def __init__(self, initial_model, transition_model, obs_model, obs_times, num_controls=10, subsample=100, tspan=None, options={}):
        super(MBVIbatched, self).__init__()
        self.initial = initial_model
        self.transition_model = transition_model
        self.obs_model = obs_model
        self.obs_times = obs_times
        self.num_controls = num_controls
        self.subsample = subsample
        self.tspan = tspan
        self.options = options
        self.control_shape = (len(obs_times)+1, num_controls, transition_model.num_controls())
        self.obs_shape = (len(obs_times), obs_model.num_species)
        self.rates = torch.nn.Parameter(torch.Tensor(transition_model.rates.shape))

    def forward(self, control, obs_data):
        return(MBVIFunctionBatched.apply(control, obs_data, self))



def vi_fun_control_rates(ind, control, rates, obs_data, elbo, control_grad, rates_grad, mbvi_batched):
    """
    this funtion is required to solve the vi_problem in parallel for different observations
    """
    # convert to numpy and reshape
    control_np = control[ind].detach().numpy().reshape(mbvi_batched.control_shape)
    rates_np = rates.detach().numpy()
    obs_data_np = obs_data[ind].numpy().reshape(mbvi_batched.obs_shape)
    # set up variational engine
    vi_engine = VariationalEngine(mbvi_batched.initial, mbvi_batched.transition_model, mbvi_batched.obs_model, mbvi_batched.obs_times, obs_data_np, num_controls=mbvi_batched.num_controls, subsample=mbvi_batched.subsample, tspan=mbvi_batched.tspan, options=mbvi_batched.options)
    vi_engine.initialize()
    # evlaute output and gradient using the variational engine
    elbo_np, grad_joint = vi_engine.objective_function((control_np, rates_np))
    # convert back to torch, store, return
    elbo[ind] = torch.tensor(elbo_np)
    control_grad[ind] = torch.from_numpy(grad_joint[0].flatten())
    rates_grad[ind] = torch.from_numpy(grad_joint[1])
    return()


class MBVIFunctionBatchedControlRates(torch.autograd.Function):
    """ 
    Minibatch wrapper of the autograd module
    """

    @staticmethod
    def forward(ctx, control, rates, obs_data, mbvi_batched):
        # get batch size
        batch_size = obs_data.shape[0]
        elbo = torch.zeros(batch_size)
        elbo.share_memory_()
        control_grad = torch.zeros(control.shape)
        control_grad.share_memory_()
        rates_grad = torch.zeros((batch_size,)+rates.shape)
        rates_grad.share_memory_()
        # create multiple instances
        processes = []
        for j in range(batch_size):
            p = mp.Process(target=vi_fun_control_rates, args=(j, control, rates, obs_data, elbo, control_grad, rates_grad, mbvi_batched))
            p.start()
            processes.append(p)
        for p in processes:
            p.join()
        ctx.save_for_backward(control_grad, rates_grad.sum(axis=0))
        return(elbo.sum())

    @staticmethod
    def backward(ctx, grad_output):
        grad_control = ctx.saved_tensors[0]*grad_output
        grad_rates = ctx.saved_tensors[1]*grad_output
        return(grad_control, grad_rates, None, None)


class MBVIbatchedControlRates(torch.nn.Module):
    """
    Similar to the MBVI class but using multiprocessing
    to deal with minibatches
    """

    def __init__(self, initial_model, transition_model, obs_model, obs_times, num_controls=10, subsample=100, tspan=None, options={}):
        super(MBVIbatchedControlRates, self).__init__()
        self.initial = initial_model
        self.transition_model = transition_model
        self.obs_model = obs_model
        self.obs_times = obs_times
        self.num_controls = num_controls
        self.subsample = subsample
        self.tspan = tspan
        self.options = options
        self.control_shape = (len(obs_times)+1, num_controls, transition_model.num_controls())
        self.obs_shape = (len(obs_times), obs_model.num_species)
        self.rates = torch.nn.Parameter(transition_model.rates.clone())

    def forward(self, control, obs_data):
        return(MBVIFunctionBatchedControlRates.apply(control, self.rates, obs_data, self))
