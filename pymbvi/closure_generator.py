import numpy as np
import sympy as sp
from collections import Counter

def base_repr(number, base):
    res = []
    while number:
        tmp = number % base
        number //= base
        res.append(tmp)
    res.reverse()
    return(res)


class GaussianMGF():
    """
    class for generating gaussian closures for arbitrary dimensions and orders
    using the sympy symbolic computation package
    """

    def __init__(self, dim):
        self.dim = dim
        self.x, self.mu, self.S = self.build_var(dim)
        self.mgf = self.build_mgf(self.x, self.mu, self.S)

    def build_var(self, dim):
        x = []
        for i in range(dim):
            tmp = sp.symbols(f'x{i}')
            x.append(tmp)
        mu = []
        for i in range(dim):
            tmp = sp.symbols(f'mu{i}')
            mu.append(tmp)
        S = []
        for i in range(dim):
            for j in range(i+1):
                tmp = sp.symbols(f'S{i}{j}')
                S.append(tmp)
        return(x, mu, S)

    def build_mgf(self, x, mu, S):
        dim = len(x)
        mgf = 0
        for i in range(dim):
            mgf += x[i]*mu[i]
            ind = int(i*(i+1)/2)
            for j in range(i):
                mgf += x[i]*x[j]*S[ind+j]
            mgf += 0.5*x[i]**2*S[ind+i]
        mgf = sp.exp(mgf)
        return(mgf)

    def get_moment(self, alpha, param=None):
        """
        compute moment given by alpha multi index
        """
        # compute derivative
        res = self.mgf
        for (i, ind) in enumerate(alpha):
            res = sp.diff(res, self.x[ind])
        # evalute at zero
        for i in range(self.dim):
            res = res.subs(self.x[i], 0)
        #replace parameters
        if param is not None:
            for i in range(self.dim):
                res = res.subs(self.mu[i], param[0][i])
                ind = int(i*(i+1)/2)
                for j in range(i+1):
                    res = res.subs(self.S[ind+j], param[1][ind+j])
        return(res)


class LognormClosure():
    """
    class for generating gaussian closures for arbitrary dimensions and orders
    using the sympy symbolic computation package
    """

    def __init__(self, dim):
        self.dim = dim

    def get_moment(self, alpha, param=None):
        """
        compute moment given by alpha multi index
        """
        # compute derivative
        res = self.mgf
        for (i, ind) in enumerate(alpha):
            res = sp.diff(res, self.x[ind])
        # evalute at zero
        for i in range(self.dim):
            res = res.subs(self.x[i], 0)
        #replace parameters
        if param is not None:
            for i in range(self.dim):
                res = res.subs(self.mu[i], param[0][i])
                ind = int(i*(i+1)/2)
                for j in range(i+1):
                    res = res.subs(self.S[ind+j], param[1][ind+j])
        return(res)


class Cumulants():

    def __init__(self, max_order):
        self.max_order = max_order
        self.moments = self.build_moments()

    def build_moments(self):
        m = []
        for i in range(self.max_order):
            tmp = sp.symbols(f'm{i+1}')
            m.append(tmp)
        return(m)

    def get_cumulant(self, order):
        cumulant = 0
        for i in range(order):
            m_sub = [self.moments[i] for i in range(order-i)]
            factor = (-1)**(i)*sp.factorial(i)
            cumulant += factor * sp.bell(order, i+1, m_sub)
        return(cumulant)

    def get_closure(self, order):
        # get cumulant
        cumulant = self.get_cumulant(order)
        # solve for highest order moment
        sol = sp.solve(cumulant, self.moments[order-1])
        return(sol[0])


class MultivariateCumulants():

    def __init__(self, dim, max_order):
        self.dim = dim
        self.max_order = max_order
        self.var = self.build_var()
        self.indices, self.counts = self.build_indices()
        self.moments = self.build_moments()
        self.mgf = self.build_mgf()

    def build_var(self):
        x = []
        for i in range(self.dim):
            tmp = sp.symbols(f'x_{i}')
            x.append(tmp)
        return(x)

    def build_indices(self):
        indices = []
        counts = []
        for i in range(self.max_order):
            tmp = self.get_indices(i+1)
            tmp = Counter([str(el) for el in tmp])
            indices.append([eval(el) for el in tmp.keys()])
            counts.append(list(tmp.values()))
        return(indices, counts)

    def get_indices(self, order):
        """
        Construct all indices for given dimension and order
        """
        indices = []
        for i in range(self.dim**order):
            ind = base_repr(i, base=self.dim)
            ind = [0 for i in range(order-len(ind))] + ind
            indices.append(np.sort(ind).tolist())
        return(indices)

    def build_moments(self):
        m = []
        for ind_col in self.indices:
            m_sub = []
            for ind in ind_col:
                ind = '_'.join([str(el) for el in ind])
                tmp = sp.symbols('m_'+ind)
                m_sub.append(tmp)
            m.append(m_sub)
        return(m)

    def build_mgf(self):
        mgf = 1
        for i in range(self.max_order):
            for j in range(len(self.indices[i])):
                tmp = self.counts[i][j]*self.moments[i][j]/sp.factorial(i+1)
                for k in self.indices[i][j]:
                    tmp *= self.var[k]
                mgf += tmp
        return(mgf)

    def get_cumulant(self, alpha):
        """
        compute cumulant given by alpha multi index
        """
        # compute derivative
        res = sp.log(self.mgf)
        for (i, ind) in enumerate(alpha):
            res = sp.diff(res, self.var[ind])
        # evalute at zero
        for i in range(self.dim):
            res = res.subs(self.var[i], 0)
        # #replace parameters
        # if param is not None:
        #     for i in range(self.dim):
        #         res = res.subs(self.mu[i], param[0][i])
        #         ind = int(i*(i+1)/2)
        #         for j in range(i+1):
        #             res = res.subs(self.S[ind+j], param[1][ind+j])
        return(res)

    def get_closure(self, alpha):
        # get cumulant
        alpha = np.sort(alpha).tolist()
        moment = 'm_' + '_'.join([str(el) for el in alpha])
        moment = sp.symbols(moment)
        cumulant = self.get_cumulant(alpha)
        # solve for highest order moment
        sol = sp.solve(cumulant, moment)
        return(sol[0])

    def get_moment(self, alpha, param=None):
        """
        compute moment given by alpha multi index
        """
        # compute derivative
        res = self.mgf
        for (i, ind) in enumerate(alpha):
            res = sp.diff(res, self.var[ind])
        # evalute at zero
        for i in range(self.dim):
            res = res.subs(self.var[i], 0)
        #replace parameters
        if param is not None:
            for i in range(self.dim):
                res = res.subs(self.mu[i], param[0][i])
                ind = int(i*(i+1)/2)
                for j in range(i+1):
                    res = res.subs(self.S[ind+j], param[1][ind+j])
        return(res)