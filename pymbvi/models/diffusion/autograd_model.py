# This files contains a class of variational models that use the torch autograd framework to compute required derivatives

import numpy as np
import torch
from scipy.linalg import solve
import  pymbvi.models.autograd_model as am
import pymbvi.util as ut

# global options
torch.set_default_dtype(torch.float64)


class AutogradModel(am.AutogradModel):
    """
    More specific instance of the autograd model for diffusions
    The derived models require a custom implmentation of the set_... functions
    """

    def fisher_loc(self, time, control, forward, rates):
        """
        Required for natural gradient mode
        """
        return(self.stat_tensor_torch(forward, rates))

    def kl_contrib_torch(self, time, state, control, rates):
        """
        compute kl contribution in terms of stat tensor
        """
        stat_tensor = self.stat_tensor_torch(state, rates)
        kl_contrib = 0.5 * control @ (stat_tensor @ control)
        return(kl_contrib)

    def constraint_gradient(self, time, control, forward, backward, rates):
        """
        gradient contribution of dynamic constraint only
        """
        # preparations
        dim = forward.shape
        control_grad = np.zeros(dim[0:3]+control.shape[-1:])
        rates_torch = torch.from_numpy(rates)
        # iterate over stored values
        with torch.set_grad_enabled(True):
            for i in range(dim[0]):
                for j in range(dim[1]):
                    control_torch = torch.tensor(control[i, j], requires_grad=True)
                    for k in range(dim[2]):
                        # forward contribution
                        forward_torch = torch.from_numpy(forward[i, j, k])
                        backward_torch = torch.from_numpy(backward[i, j, k])
                        tmp = self.forward_torch(time, forward_torch, control_torch, rates_torch)
                        tmp.backward(backward_torch)
                        control_grad[i, j, k] = np.array(control_torch.grad)
                        control_torch.grad.zero_()
        # integrate over subsamples
        control_grad = ut.integrate_subsamples(time, control_grad)
        return(control_grad)

    def stat_tensor(self, time, forward, rates):
        # preparations
        dim = forward.shape[:3] + (self.num_controls(), self.num_controls())
        stat_tensor = np.zeros(dim)
        rates_torch = torch.from_numpy(rates)
        # iterate over stored values
        for i in range(dim[0]):
            for j in range(dim[1]):
                for k in range(dim[2]):
                    forward_torch = torch.from_numpy(forward[i, j, k])
                    stat_tensor[i, j, k] = self.stat_tensor_torch(forward_torch, rates_torch).numpy()
        stat_tensor = ut.integrate_subsamples(time, stat_tensor)
        return(stat_tensor)  

    # functions required by subclasses

    def forward_torch(self, time, state, control, rates):
        """
        Identical to forward but expecting torch tensor input
        """
        raise NotImplementedError

    def stat_tensor_torch(self, state, rates):
        raise NotImplementedError

    def num_controls(self):
        raise NotImplementedError


class FullAutogradModel(am.AutogradModel):
    """
    More specific instance of the autograd model for diffusions
    The derived models require a custom implmentation of the set_... functions
    """

    # constructor and setup

    def fisher_loc(self, time, control, forward, rates):
        """
        Required for natural gradient mode
        """
        def fun(x):
            return(self.kl_contrib_torch(time, forward, x, rates))
        fisher = ut.autograd_hessian(fun, control).numpy()
        return(fisher)

    # functions required by subclasses

    def forward_torch(self, time, state, control, rates):
        """
        Identical to forward but expecting torch tensor input
        """
        raise NotImplementedError

    def kl_contrib_torch(self, time, state, control, rates):
        """
        kl kontribution
        """
        raise NotImplementedError

    def num_controls(self):
        raise NotImplementedError
