# This files contains clases that may be used for solving the forward and backward
# Fokker-Planck equations of a model using the method of lines

import numpy as np


class Dicrete1D(object):
    """
    Spatially discretized one d model
    - time independent drift and diffusion terms
    """

    # constructor and setup

    def __init__(self, discrete_state, rates, timedep=False):
        """
        Constructor method
        """
        # store inputs
        self.states_vec = discrete_state.flatten()
        self.rates = rates
        self.drift_vec = self.drift(0.0, self.states_vec)
        self.diff_vec = self.diff(0.0, self.states_vec)
        if timedep:
            self.get_drift = self.get_drift_timedep
        else:
            self.get_drift = self.get_drift_timeind

    def forward(self, time, state, rates):
        # initialize output
        dydt = np.zeros(state.shape)
        # get drift
        drift_vec = self.get_drift(time, state, rates)
        # drift contribution
        dydt[1:-1] = -(drift_vec[2:]*state[2:] - drift_vec[:-2]*state[:-2])/(self.states_vec[2:]-self.states_vec[:-2])
        # diffusion contribution
        tmp = self.diff_vec*state
        dydt[1:-1] += 2 * (tmp[2:] + tmp[:-2] - 2*tmp[1:-1])/(self.states_vec[2:]-self.states_vec[:-2])**2
        return(dydt)

    def backward(self, time, state, rates):
        # initialize output
        dydt = np.zeros(state.shape)
        # get drift
        drift_vec = self.get_drift(time, state, rates)
        # drift contribution 
        dydt[1:-1] = drift_vec[1:-1]*(state[2:] - state[:-2])/(self.states_vec[2:]-self.states_vec[:-2])
        # diffusion contribution
        dydt[1:-1] += 2 * self.diff_vec[1:-1] * (state[2:] + state[:-2] - 2*state[1:-1])/(self.states_vec[2:]-self.states_vec[:-2])**2
        return(-dydt)

    def get_drift_timeind(self, time, state, rates):
        return(self.drift_vec)

    def get_drift_timedep(self, time, state, rates):
        drift_vec = self.drift(time, state)
        return(drift_vec)

    @property
    def num_states(self):
        return(len(self.states_vec))

    @property
    def dim(self):
        return(1)

    @property
    def states(self):
        return(np.expand_dims(self.states_vec, axis=1))

    # to be implemented by derived class

    def drift(self, time, state):
        raise NotImplementedError

    def diff(self, time, state):
        raise NotImplementedError
