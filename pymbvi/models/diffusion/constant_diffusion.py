# This files contains a class of variational models 

import numpy as np
import torch
from pymbvi.models.diffusion.autograd_model import AutogradModel
from pymbvi.models.diffusion.linear_control import MultivariateDiffLinearControl
import pymbvi.util as ut

# global options
torch.set_default_dtype(torch.float64)


class ConstantDiffusion(MultivariateDiffLinearControl):
    """
    Base class for models implemented using torch autograd
    The derived models require a custom implmentation of the set_... functions
    """

    # constructor and setup

    def __init__(self, initial, rates, tspan, gradient_mode='natural'):
        """
        Constructor method
        """
        # base class constructor 
        AutogradModel.__init__(self, initial, rates, tspan, gradient_mode=gradient_mode)
        # sde quantities
        self.num_species = int(-1.5+np.sqrt(2*self.num_states()+2.25))
        self.cov_map = self.set_cov_map()

    def forward_torch(self, time, state, control, rates):
        """
        A generic version of the moment function that requires
        implementation of E[a(X_t)] and E[a(X_t) X_t^T]
        """
        # get first moments and control
        m, M = self.get_moments(state)
        u0, u1 = self.get_controls(control)
        # get expected drift and model parameters
        a0, a1 = self.get_expected_drift(m, M, rates)
        sqrt_diff = self.get_diff(rates)
        # derivative of the mean
        dm = a0 + sqrt_diff @ (u0 + u1 @ m)
        # derivative of the covariance
        tmp = a1 + torch.ger(sqrt_diff @ u0, m) + sqrt_diff @ u1 @ (M + torch.ger(m, m))
        dM = tmp + tmp.T + sqrt_diff @ sqrt_diff.T - torch.ger(m, dm) - torch.ger(dm, m)
        # combine to single vector
        dydt = torch.zeros(state.shape)
        dydt[:self.num_species] = dm
        dydt[self.num_species:] = dM[self.cov_map[0], self.cov_map[1]]
        return(dydt)

    def get_expected_drift(self, m, M, rates):
        raise NotImplementedError

    def get_diff(self, rates):
        raise NotImplementedError


class ConstantDiffusionSampled(ConstantDiffusion):
    """
    this class takes the drift function as an argument and uses samples to approximate the expected drift
    """

    def __init__(self, initial, rates, tspan, sqrt_diff, num_samples=100, adjust_samples=True):
        """
        Constructor method
        """
        # base class constructor 
        ConstantDiffusion.__init__(self, initial, rates, tspan)
        # sde quantities
        self.sqrt_diff = torch.tensor(sqrt_diff)
        self.num_samples = num_samples
        self.samples = self.set_samples(adjust_samples)

    def get_expected_drift(self, m, M, rates):
        # preparations
        print(m.shape)
        print(M.shape)
        R = torch.cholesky(M+1e-3*torch.eye(self.num_species))
        samples = m + self.samples @ R.T
        # compute drift
        drift = self.drift(samples, rates)
        a0 = torch.mean(drift, axis=0)
        a1 = torch.mean(drift.unsqueeze(2) @ samples.unsqueeze(1), axis=0)
        return(a0, a1)

    def set_samples(self, adjust_samples):
        samples = torch.randn((self.num_samples, self.num_species))
        if adjust_samples:
            # compute mean and cov
            mean = samples.mean(axis=0)            
            tmp = samples-mean.unsqueeze(0)
            cov = torch.mean(tmp.unsqueeze(2) @ tmp.unsqueeze(1), axis=0)
            # standardize mean and cov
            R = torch.cholesky(cov)
            samples = torch.solve(tmp.unsqueeze(-1), R.unsqueeze(0))[0].reshape(samples.shape)
        return(samples)

    def get_diff(self, rates):
        return(self.sqrt_diff)

    def drift(self, state, rates):
        raise NotImplementedError  


class ConstantDiffusionUT(ConstantDiffusion):
    """
    this class takes the drift function as an argument and uses samples to approximate the expected drift
    """

    def __init__(self, initial, rates, tspan, sqrt_diff, alpha=1e-2, kappa=0.0, beta=2.0):
        """
        Constructor method
        """
        # base class constructor 
        ConstantDiffusion.__init__(self, initial, rates, tspan)
        # sde quantities
        self.drift = drift
        self.sqrt_diff = torch.tensor(sqrt_diff)
        self.alpha = alpha
        self.kappa = kappa
        self.beta = beta
        self.reg = 1e-5
        self.lam = alpha**2*(self.num_species+kappa) - self.num_species
        self.weights = self.set_weights()
        self.samples = self.set_samples()

    def get_expected_drift(self, m, M, rates):
        # preparations
        try:
            R = torch.cholesky(M)
        except:
            R = torch.cholesky(M+1e-3*torch.eye(self.num_species))
        samples = m + self.samples @ R.T
        # compute drift
        drift = self.drift(samples, rates)
        a0 = torch.mean(drift, axis=0)
        a1 = torch.mean(drift.unsqueeze(2) @ samples.unsqueeze(1), axis=0)
        return(a0, a1)

    def set_samples(self):
        """
        Generate the sigma points
        """
        tmp = torch.eye(self.num_species)
        samples = torch.zeros((2*self.num_species+1, self.num_species))
        for i in range(self.num_species):
            samples[i+1] = torch.sqrt(torch.tensor(self.num_species+self.lam))*tmp[i]
            samples[i+1+self.num_species] = -torch.sqrt(torch.tensor(self.num_species+self.lam))*tmp[i]
        return(samples)

    def set_weights(self):
        """
        Compute the weights
        """
        # mean weights
        weights = torch.zeros((2, 2*self.num_species+1))
        weights[0, 0] = self.lam/(self.num_species+self.lam)
        weights[0, 1:] = 0.5/(self.num_species+self.lam)
        # covariance weights
        weights[1, 0] = self.lam/(self.num_species+self.lam) + (1-self.alpha**2+self.beta)
        weights[1, 1:] = 0.5/(self.num_species+self.lam)
        return(weights)

    def get_diff(self, rates):
        return(self.sqrt_diff)

    def drift(self, state, rates):
        raise NotImplementedError    


class ConstantDiffusionNoisefree(ConstantDiffusion):
    """
    this class takes the drift function as an argument and uses samples to approximate the expected drift
    """

    def __init__(self, initial, rates, tspan, drift, sqrt_diff):
        """
        Constructor method
        """
        # base class constructor 
        ConstantDiffusion.__init__(self, initial, rates, tspan)
        # sde quantities
        self.drift = drift
        self.sqrt_diff = torch.tensor(sqrt_diff)
        self.samples = torch.randn((100, self.num_species))

    def get_expected_drift(self, m, M, rates):
        # preparations
        R = torch.cholesky(M+1e-3*torch.eye(self.num_species))
        samples = m + self.samples @ R.T
        # compute drift
        drift = self.drift(samples, rates)
        a0 = torch.mean(drift, axis=0)
        a1 = torch.mean(drift.unsqueeze(2) @ samples.unsqueeze(1), axis=0)
        return(a0, a1)

    def get_diff(self, rates):
        return(self.sqrt_diff)