import numpy as np
import torch
from scipy.linalg import solve

from pymbvi.models.model import Model
from pymbvi.models.diffusion.autograd_model import AutogradModel
import pymbvi.util as ut


class LinearControl(Model):
    """
    General class for diffusion with a linear control 
    """

    def kl_prior(self, time, control, forward, rates):
        """
        For the independent control, the kl divergence only depends on the control
        """
        # get diffusion tensor
        num_species = int(-1.5+np.sqrt(2*self.num_states()+2.25))
        dim = forward.shape[:2]+(num_species, num_species)
        stat_tensor = self.stat_tensor(time, forward, rates)
        #  compute kl fun
        kl = 0.5*(np.expand_dims(control, axis=-2)@stat_tensor@np.expand_dims(control, axis=-1)).squeeze()
        return(kl)

    def rgradient(self, time, control, forward, backward, rates):
        # get diffusion tensor
        num_species = int(-0.5+np.sqrt(self.num_states()+0.25))
        dim = forward.shape[:2]+(num_species, num_species)
        stat_tensor = self.stat_tensor(time, forward)
        # contribution from the constraint
        dim_forward = forward.shape[0:3]+(-1,)
        constraint_grad = self.control_gradient(forward.reshape(-1, forward.shape[-1]), backward.reshape(-1, backward.shape[-1]))
        # contribution from the prior kl divergence
        grad = stat_tensor@control - constraint_grad
        return(grad)

    def control_gradient(self, time, control, forward, backward, rates):
        # get diffusion tensor
        num_species = int(-1.5+np.sqrt(2*self.num_states()+2.25))
        dim = forward.shape[:2]+(num_species, num_species)
        stat_tensor = self.stat_tensor(time, forward, rates)
        # contribution from the constraint
        dim_forward = forward.shape[0:3]+(-1,)
        constraint_grad = self.constraint_gradient(time, control, forward, backward, rates)
        constraint_grad = self.fisher_transform(stat_tensor, constraint_grad)
        # contribution from the prior kl divergence
        grad = control - constraint_grad
        return(grad)

    def fisher_transform(self, diff, constraint_grad):
        # set up output
        dim = constraint_grad.shape
        transformed_grad = np.zeros(dim)
        # iterate over time
        for i in range(dim[0]):
            for j in range(dim[1]):
                G = diff[i, j]
                tmp = constraint_grad[i, j]
                transformed_grad[i, j] = solve(G, tmp, sym_pos=True)
        return(transformed_grad)

    def effective_control(self, control, rates):
        eff_control = np.outer(control, control)
        return(eff_control)

    def num_controls(self):
        return(2)


class DiffLinearControl(AutogradModel):
    """
    Diffusion model class for the linear control
    Implements the contributions to the objective function of the prior

    Right now only for 1d models
    """

    def kl_prior(self, time, control, forward, rates):
        """
        For the independent control, the kl divergence only depends on the control
        """
        # split control and state
        num_species = int(-0.5+np.sqrt(self.num_controls()+0.25))
        control_ind = np.expand_dims(control[:, :, :num_species], axis=2)
        control_lin = np.expand_dims(control[:, :, num_species:], axis=2)
        mean = forward[:, :, :, :num_species]
        cov = forward[:, :, :, num_species:]
        #  compute kl fun
        kl_mean = (control_ind+control_lin*mean)**2
        kl_cov = cov*control_lin**2
        kl = np.concatenate((kl_mean, kl_cov), axis=-1)
        # integrate over subsampling intervals
        kl = 0.5*ut.integrate_subsamples(time, kl)
        return(kl)

    # def rgradient(self, time, control, forward, backward):
    #     num_species = int(-0.5+np.sqrt(self.num_controls()+0.25))
    #     # initialize output
    #     grad = np.zeros(control.shape)
    #     # get relevant dimensions
    #     dim = forward.shape[0:3]+(-1,)
    #     # contribution from the constraint
    #     constraint_grad = self.control_gradient(forward.reshape(-1, forward.shape[-1]), backward.reshape(-1, backward.shape[-1]))
    #     # convert to non-central second moment and integrate
    #     stats = forward.copy()
    #     stats[:, :, :, 1] = stats[:, :, :, 1]+stats[:, :, :, 0]**2
    #     # perform integration over subsampling intervals 
    #     constraint_grad = ut.integrate_subsamples(time, constraint_grad, dim)
    #     stats = ut.integrate_subsamples(time, stats)
    #     # contribution from the prior kl divergence
    #     delta = time[:, :, [-1]]-time[:, :, [0]]
    #     grad[:, :, :num_species] = control[:, :, :num_species]*delta + control[:, :, num_species:]*stats[:, :, :num_species]
    #     grad[:, :, num_species:] = control[:, :, :num_species]*stats[:, :, :num_species] + control[:, :, num_species:]*stats[:, :, num_species:]
    #     # construct final gradien
    #     grad -= constraint_grad
    #     return(grad)

    def control_gradient(self, time, control, forward, backward, rates):
        # get number of species
        num_species = int(-0.5+np.sqrt(self.num_controls()+0.25))
        # get relevant dimensions
        dim = forward.shape[0:3]+(-1,)
        # contribution from the constraint
        constraint_grad = self.constraint_gradient(time, control, forward, backward, rates)
        # convert to non-central second moment and integrate
        stats = forward.copy()
        stats[:, :, :, 1] = stats[:, :, :, 1]+stats[:, :, :, 0]**2
        # perform integration over subsampling intervals 
        stats = ut.integrate_subsamples(time, stats)
        # construct inverse fisher information
        dim = control.shape[:2]+(2, 2)
        delta = time[:, :, [-1]]-time[:, :, [0]]
        inv_g = (np.concatenate([stats[:, :, [1]], -stats[:, :, [0]], -stats[:, :, [0]], delta], axis=-1)/(delta*stats[:, :, [1]]-stats[:, :, [0]]**2)).reshape(dim)
        # contribution from the prior kl divergence
        grad = control - (inv_g@np.expand_dims(constraint_grad, axis=-1)).squeeze()
        return(grad)

    # def rates_gradient(self, time, control, forward, backward, rates):
    #     # contribution from the constraint (no kl contribution due to diffusion rescaling)
    #     grad = self.constraint_rates_gradient(control, forward, backward, rates)
    #     # perform time integration
    #     grad = -ut.integrate(time, grad)
    #     return(grad)

    def joint_gradient(self, time, control, forward, backward, rates, options):
        control_grad = self.control_gradient(time, control, forward, backward, rates)
        rates_grad = self.rates_gradient(time, control, forward, backward, rates)
        return(control_grad, rates_grad)

    def num_controls(self):
        return(2)

    def stat_tensor_torch(self, forward, rates):
        diff = torch.ones(2, 2)
        diff[0, 1] = forward[0]
        diff[1, 0] = forward[0]
        diff[1, 1] = forward[1]+forward[0]**2
        return(diff)


class MultivariateLinearControl(AutogradModel):
    """
    Diffusion model class for the linear control for multivariate models with stats tensor
    """

    def kl_prior(self, time, control, forward, rates):
        """
        For the independent control, the kl divergence only depends on the control
        """
        # split control and state
        num_species = int(-1.5+np.sqrt(2*self.num_states()+2.25))
        diag_ind = [i for i in range(num_species)]
        u0 = np.expand_dims(control[:, :, :num_species], axis=2)
        u1 = np.expand_dims(control[:, :, num_species:], axis=2).reshape(u0.shape[:3]+(num_species, num_species))
        m = forward[:, :, :, :num_species]
        cov = np.zeros(forward.shape[:3]+(num_species, num_species))
        cov[:, :, :, self.cov_map[0], self.cov_map[1]] = forward[:, :, :, self.num_species:]
        cov = cov + np.swapaxes(cov, -1, -2)
        cov[:, :, :, diag_ind, diag_ind] = 0.5*cov[:, :, :, diag_ind, diag_ind] 
        # get stats
        D0, D1, D2 = self.stat_tensor(time, forward, rates)
        #  compute kl fun
        kl_u0_u0 = (np.expand_dims(u0, axis=-2) @ D0 @ np.expand_dims(u0, axis=-1)).squeeze(axis=-1)
        u0_u1 = u0.reshape(u0.shape+(1,1)) * np.expand_dims(u1, axis=-3)
        kl_u0_u1 = (u0_u1 * D1).reshape(forward.shape[0:3]+(-1,))
        u1_u1 = u1.reshape(u1.shape+(1,1)) * u1.reshape(u1.shape[:3]+(1,1)+u1.shape[3:])
        kl_u1_u1 = (np.swapaxes(u1_u1, -2, -3) * np.swapaxes(D2, -1, -2)).reshape(forward.shape[0:3]+(-1,))
        kl = np.concatenate([kl_u0_u0, 2*kl_u0_u1, kl_u1_u1], axis=-1)
        # integrate over subsampling intervals
        kl = 0.5*ut.integrate_subsamples(time, kl)
        return(kl)

    # def rgradient(self, time, control, forward, backward):
    #     # get num species and indices
    #     num_species = int(-1.5+np.sqrt(2*self.num_states()+2.25))
    #     diag_ind = [i for i in range(num_species)]
    #     # initialize output
    #     grad = np.zeros(control.shape)
    #     # get relevant dimensions
    #     dim = forward.shape[0:3]+(-1,)
    #     # contribution from the constraint
    #     constraint_grad = self.control_gradient(forward.reshape(-1, forward.shape[-1]), backward.reshape(-1, backward.shape[-1]))
    #     # convert to non-central second moment and integrate
    #     m_eff = self.integrate_subsamples(time, forward[:, :, :, :num_species])
    #     M = np.zeros(forward.shape[:3]+(num_species, num_species))
    #     M[:, :, :, self.cov_map[0], self.cov_map[1]] = forward[:, :, :, self.num_species:]
    #     M = M + np.swapaxes(M, -1, -2)
    #     M[:, :, :, diag_ind, diag_ind] *= 0.5
    #     M += np.expand_dims(forward[:, :, :, :num_species], axis=-1) @ np.expand_dims(forward[:, :, :, :num_species], axis=-2)
    #     M_eff = self.integrate_subsamples(time, M)
    #     constraint_grad = self.integrate_subsamples(time, constraint_grad, dim)
    #     # get controls
    #     u0 = control[:, :, :num_species]
    #     u1 = control[:, :, num_species:].reshape(u0.shape[:2]+(num_species, num_species))
    #     # contribution from the prior kl divergence
    #     delta = time[:, :, [-1]]-time[:, :, [0]]
    #     grad[:, :, :num_species] = u0*delta + (u1@np.expand_dims(m_eff, axis=-1)).squeeze()
    #     grad[:, :, num_species:] = (np.expand_dims(u0, axis=-1)@np.expand_dims(m_eff, axis=-2) + u1@M_eff).reshape(u0.shape[:2]+(num_species**2,))
    #     test = self.fisher_transform(delta, m_eff, M_eff, u0, u1, grad)
    #     # construct final gradien
    #     grad -= constraint_grad
    #     return(grad)

    def control_gradient(self, time, control, forward, backward, rates):
        # get num species and indices
        num_species = int(-1.5+np.sqrt(2*self.num_states()+2.25))
        diag_ind = [i for i in range(num_species)]
        # get diffusion tensor
        D0, D1, D2 = self.stat_tensor(time, forward, rates)
        D0 = ut.integrate_subsamples(time, D0)
        D1 = ut.integrate_subsamples(time, D1)
        D2 = ut.integrate_subsamples(time, D2)
        # get relevant dimensions
        dim = forward.shape[0:3]+(-1,)
        # contribution from the constraint
        constraint_grad = self.constraint_gradient(time, control, forward, backward, rates)
        constraint_grad = self.fisher_transform(D0, D1, D2, constraint_grad)
        # construct final gradien
        grad = control - constraint_grad
        return(grad)

    def compute_fisher_g(self, D0, D1, D2):
        # # set up output
        num_species = D0.shape[0]
        # u0 vs u0 contribution
        u0_u0 = D0
        # u1 vs u1 contribution
        u1_u1 = D2.swapaxes(-2, -3).reshape((num_species**2, num_species**2))
        # u0 vs u1
        u0_u1 = D1.reshape((num_species, num_species**2))
        # u1 vs u0
        u1_u0 = np.moveaxis(D1, [-1, -2, -3], [-2, -3, -1]).reshape((num_species**2, num_species))
        # concatenate and return
        fisher_g = np.block([[u0_u0, u0_u1], [u1_u0, u1_u1]])
        return(fisher_g)

    def fisher_transform(self, D0, D1, D2, constraint_grad):
        # set up output
        dim = constraint_grad.shape
        transformed_grad = np.zeros(dim)
        # iterate over time
        for i in range(dim[0]):
            for j in range(dim[1]):
                G = self.compute_fisher_g(D0[i, j], D1[i, j], D2[i, j])
                tmp = constraint_grad[i, j]
                transformed_grad[i, j] = solve(G, tmp, sym_pos=True)
        return(transformed_grad)

    def get_moments(self, state):
        """
        Convert state to mean and covariance matrix
        """
        # extract first moment
        m = state[:self.num_species]
        # construct second moment
        tmp = torch.zeros((self.num_species, self.num_species))
        tmp[self.cov_map[0], self.cov_map[1]] = state[self.num_species:]
        M = tmp+tmp.T-torch.diag(tmp.diag())
        # return mean and covariance
        return(m, M)
    
    def get_controls(self, control):
        """
        Convert vector to u0 vector and u1 matrix
        """
        u0 = control[:self.num_species]
        u1 = control[self.num_species:].reshape((self.num_species, self.num_species))
        return(u0, u1)

    def set_cov_map(self):
        """
        Construct a tensor that maps the reduced moments to a full covariance matrix
        """
        cov_map = torch.tril_indices(row=self.num_species, col=self.num_species, offset=0)
        return(cov_map)  

    def num_controls(self):
        return(self.num_species*(self.num_species+1))


class MultivariateDiffLinearControl(AutogradModel):
    """
    Diffusion model class for the linear control for multivariate models
    Implements the contributions to the objective function of the prior
    """

    def kl_prior(self, time, control, forward, rates):
        """
        For the independent control, the kl divergence only depends on the control
        """
        # split control and state
        num_species = int(-1.5+np.sqrt(2*self.num_states()+2.25))
        diag_ind = [i for i in range(num_species)]
        control_ind = np.expand_dims(control[:, :, :num_species], axis=2)
        control_lin = np.expand_dims(control[:, :, num_species:], axis=2).reshape(control_ind.shape[:3]+(num_species, num_species))
        mean = forward[:, :, :, :num_species]
        cov = np.zeros(forward.shape[:3]+(num_species, num_species))
        cov[:, :, :, self.cov_map[0], self.cov_map[1]] = forward[:, :, :, self.num_species:].squeeze()
        cov = cov + np.swapaxes(cov, -1, -2)
        cov[:, :, :, diag_ind, diag_ind] = 0.5*cov[:, :, :, diag_ind, diag_ind] 
        #  compute kl fun
        kl_mean = (control_ind + (control_lin@np.expand_dims(mean, axis=-1)).squeeze())**2
        kl_cov = (np.swapaxes(control_lin, -1, -2) @ control_lin) * cov  #cov*control_lin**2
        kl = np.concatenate([kl_mean, kl_cov.reshape(kl_cov.shape[:3]+(-1,))], axis=-1)
        # integrate over subsampling intervals
        kl = 0.5*ut.integrate_subsamples(time, kl)
        return(kl)

    # def rgradient(self, time, control, forward, backward):
    #     # get num species and indices
    #     num_species = int(-1.5+np.sqrt(2*self.num_states()+2.25))
    #     diag_ind = [i for i in range(num_species)]
    #     # initialize output
    #     grad = np.zeros(control.shape)
    #     # get relevant dimensions
    #     dim = forward.shape[0:3]+(-1,)
    #     # contribution from the constraint
    #     constraint_grad = self.control_gradient(forward.reshape(-1, forward.shape[-1]), backward.reshape(-1, backward.shape[-1]))
    #     # convert to non-central second moment and integrate
    #     m_eff = ut.integrate_subsamples(time, forward[:, :, :, :num_species])
    #     M = np.zeros(forward.shape[:3]+(num_species, num_species))
    #     M[:, :, :, self.cov_map[0], self.cov_map[1]] = forward[:, :, :, self.num_species:]
    #     M = M + np.swapaxes(M, -1, -2)
    #     M[:, :, :, diag_ind, diag_ind] *= 0.5
    #     M += np.expand_dims(forward[:, :, :, :num_species], axis=-1) @ np.expand_dims(forward[:, :, :, :num_species], axis=-2)
    #     M_eff = ut.integrate_subsamples(time, M)
    #     constraint_grad = ut.integrate_subsamples(time, constraint_grad, dim)
    #     # get controls
    #     u0 = control[:, :, :num_species]
    #     u1 = control[:, :, num_species:].reshape(u0.shape[:2]+(num_species, num_species))
    #     # contribution from the prior kl divergence
    #     delta = time[:, :, [-1]]-time[:, :, [0]]
    #     grad[:, :, :num_species] = u0*delta + (u1@np.expand_dims(m_eff, axis=-1)).squeeze()
    #     grad[:, :, num_species:] = (np.expand_dims(u0, axis=-1)@np.expand_dims(m_eff, axis=-2) + u1@M_eff).reshape(u0.shape[:2]+(num_species**2,))
    #     test = self.fisher_transform(delta, m_eff, M_eff, u0, u1, grad)
    #     # construct final gradien
    #     grad -= constraint_grad
    #     return(grad)

    def control_gradient(self, time, control, forward, backward, rates):
        # get num species and indices
        num_species = int(-1.5+np.sqrt(2*self.num_states()+2.25))
        diag_ind = [i for i in range(num_species)]
        # initialize output
        grad = np.zeros(control.shape)
        # get relevant dimensions
        dim = forward.shape[0:3]+(-1,)
        # contribution from the constraint
        constraint_grad = self.constraint_gradient(time, control, forward, backward, rates)
        # get controls
        u0 = control[:, :, :num_species]
        u1 = control[:, :, num_species:].reshape(u0.shape[:2]+(num_species, num_species))
        # contribution from constrained grad (fisher transformed)
        fisher_g = self.compute_fisher(time, control, forward, rates)
        constraint_grad = self.fisher_transform(fisher_g, constraint_grad)
        # construct final gradient
        grad = control - constraint_grad
        return(grad)

    # def rates_gradient(self, time, control, forward, backward, rates):
    #     # contribution from the constraint (no kl contribution due to diffusion rescaling)
    #     grad = self.constraint_rates_gradient(time, control, forward, backward, rates)
    #     return(grad)

    # def joint_gradient(self, time, control, forward, backward, rates, options):
    #     control_grad = self.control_gradient(time, control, forward, backward, rates)
    #     rates_grad = self.rates_gradient(time, control, forward, backward, rates)
    #     return(control_grad, rates_grad)

    # def compute_fisher_g(self, delta, m_eff, M_eff):
    #     # set up output
    #     num_species = len(m_eff)
    #     n = len(m_eff)*(len(m_eff)+1)
    #     fisher_g = np.zeros((n, n))
    #     # u0 vs u0 contribution
    #     u0_u0 = np.eye(num_species)*delta
    #     # u1 vs u1 contribution
    #     u1_u1 = (M_eff.reshape((1, num_species, 1, num_species))*np.eye(num_species).reshape((num_species, 1, num_species, 1))).reshape((num_species**2, num_species**2))
    #     # u0 vs u1
    #     u0_u1 = (np.eye(num_species).reshape((num_species, num_species, 1))*m_eff.reshape((1, 1, num_species))).reshape((num_species, num_species**2))
    #     # concatenate and return
    #     fisher_g = np.block([[u0_u0, u0_u1], [u0_u1.T, u1_u1]])
    #     return(fisher_g)


    def num_controls(self):
        return(self.num_species*(self.num_species+1))

    def get_moments(self, state):
        """
        Convert state to mean and covariance matrix
        """
        # extract first moment
        m = state[:self.num_species]
        # construct second moment
        tmp = torch.zeros((self.num_species, self.num_species))
        tmp[self.cov_map[0], self.cov_map[1]] = state[self.num_species:]
        M = tmp+tmp.T-torch.diag(tmp.diag())
        # return mean and covariance
        return(m, M)
    
    def get_controls(self, control):
        """
        Convert vector to u0 vector and u1 matrix
        """
        u0 = control[:self.num_species]
        u1 = control[self.num_species:].reshape((self.num_species, self.num_species))
        return(u0, u1)

    def set_cov_map(self):
        """
        Construct a tensor that maps the reduced moments to a full covariance matrix
        """
        cov_map = torch.tril_indices(row=self.num_species, col=self.num_species, offset=0)
        return(cov_map)       

    def forward_torch(self, time, state, control, rates):
        # get first moment and covariance
        m, M = self.get_moments(state)
        u0, u1 = self.get_controls(control)
        # derivative of the mean and vo
        dm, dM = self.moments(time, m, M, u0, u1, rates)
        # combine to single vector
        dydt = torch.zeros(state.shape)
        dydt[:self.num_species] = dm
        dydt[self.num_species:] = dM[self.cov_map[0], self.cov_map[1]]
        return(dydt)

    def stat_tensor_torch(self, forward, rates):
        # get first moment and covariance
        m, M = self.get_moments(forward)
        M = M + torch.ger(m, m)
        # u0 vs u0 contribution
        u0_u0 = torch.eye(self.num_species)
        # u1 vs u1 contribution
        u1_u1 = (M.reshape((1, self.num_species, 1, self.num_species))*torch.eye(self.num_species).reshape((self.num_species, 1, self.num_species, 1))).reshape((self.num_species**2, self.num_species**2))
        # u0 vs u1
        u0_u1 = (torch.eye(self.num_species).reshape((self.num_species, self.num_species, 1))*m.reshape((1, 1, self.num_species))).reshape((self.num_species, self.num_species**2))
        # concatenate and return
        stat_tensor = torch.cat([torch.cat([u0_u0, u0_u1], axis=1), torch.cat([u0_u1.T, u1_u1], axis=1)])
        return(stat_tensor)

    def moments(self, time, m, M, u0, u1, rates):
        raise NotImplementedError