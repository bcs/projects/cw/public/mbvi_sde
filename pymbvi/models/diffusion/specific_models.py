# This files contains a class of variational models that use the torch autograd framework to compute required derivatives

import numpy as np
import scipy.special as special
import torch

from pymbvi.models.diffusion.independent_control import IndependentControl, DiffIndependentControl
from pymbvi.models.diffusion.linear_control import LinearControl, DiffLinearControl, MultivariateDiffLinearControl, MultivariateLinearControl
from pymbvi.models.diffusion.autograd_model import AutogradModel
#from pymbvi.models.autograd_model import AutogradModel
import pymbvi.util as ut
import pymbvi.closure as cl


class OU_Process(DiffIndependentControl):
    """
    Ornstein Uhlenbeck process with independent control of the form
    u(x,t) = delta * u(t)
    """

    def forward_torch(self, time, state, control, rates):
        """
        forward equation for the ou process
        """
        dydt = torch.zeros(state.shape)
        dydt[0] = - rates[0]*(state[0]-rates[2]) + rates[1]*control
        dydt[1] = -2*rates[0]*state[1]+rates[1]**2
        return(dydt)


class OU_Process_Diff(IndependentControl):
    """
    Ornstein Uhlenbeck process with independent control of the form 
    u(x,t) = delta^2 * u(t)
    """

    def forward_torch(self, time, state, control, rates):
        """
        forward equation for the ou process
        """
        dydt = torch.zeros(state.shape)
        dydt[0] = - rates[0]*(state[0]-rates[2]) + rates[1]**2*control
        dydt[1] = -2*rates[0]*state[1]+rates[1]**2
        return(dydt)

    def stat_tensor_torch(self, forward, rates):
        tmp = forward.sum()
        diff = rates[1]**2+(tmp-tmp)
        return(diff.unsqueeze(0))


class OU_Process_Raw(IndependentControl):
    """
    Ornstein Uhlenbeck process with independent control of the form 
    u(x,t) = u(t)
    """

    def forward_torch(self, time, state, control, rates):
        """
        forward equation for the ou process
        """
        dydt = torch.zeros(state.shape)
        dydt[0] = - rates[0]*(state[0]-rates[2]) + control
        dydt[1] = -2*rates[0]*state[1]+rates[1]**2
        return(dydt)

    def stat_tensor_torch(self, forward, rates):
        tmp = forward.sum()
        diff = 1.0/rates[1]**2+(tmp-tmp)
        return(diff.unsqueeze(0))


class OU_Process_Linear(DiffLinearControl):
    """
    Ornstein Uhlenbeck process with linear control
    """

    def forward_torch(self, time, state, control, rates):
        """
        forward equation for the ou process
        """
        dydt = torch.zeros(state.shape)
        dydt[0] = - (rates[0]-rates[1]*control[1])*state[0] + rates[1]*control[0]
        dydt[1] = -2*(rates[0]-rates[1]*control[1])*state[1]+rates[1]**2
        return(dydt)


# class MOU_Process(AutogradModel, DiffIndependentControl):
#     """
#     Multivariate Ornstein Uhlenbeck process with independent control of the form
#     u(x,t) = delta * u(t)
#     """

#     def __init__(self, initial, tspan, drift, diff, mean, diffusion='raw'):
#         self.mean = mean
#         self.drift = drift
#         if diffusion == 'raw':
#             self.diff = diff @ diff.T
#         elif diffusion == 'squared':
#             self.diff = diff
#         self.sqrt_diff = torch.tensor(np.linalg.cholesky(self.diff))
#         rates = torch.cat([self.mean.flatten(), self.drift.flatten(), self.sqrt_diff.flatten()])
#         AutogradModel.__init__(self, initial, rates, tspan)
#         self.num_species = int(-1.5+np.sqrt(2*self.num_states()+2.25))
#         self.cov_map = self.set_cov_map()

#     def forward_torch(self, time, state, control, rates):
#         # get first moment and covariance
#         m, M = self.get_moments(state)
#         # get parameters
#         mean, drift, sqrt_diff = self.get_params(rates)
#         # derivative of the mean
#         dm = -drift @ (m - mean) + sqrt_diff @ control
#         # derivative of the covariance
#         dM = -drift @ M + sqrt_diff@sqrt_diff.T
#         # combine to single vector
#         dydt = torch.zeros(state.shape)
#         dydt[:self.num_species] = dm
#         dydt[self.num_species:] = dM[self.cov_map[0], self.cov_map[1]]
#         return(dydt)

#     def get_moments(self, state):
#         """
#         Convert state to mean and covariance matrix
#         """
#         # extract first moment
#         m = state[:self.num_species]
#         # construct second moment
#         tmp = torch.zeros((self.num_species, self.num_species))
#         tmp[self.cov_map[0], self.cov_map[1]] = state[self.num_species:]
#         M = tmp+tmp.T-torch.diag(tmp.diag())
#         # return mean and covariance
#         return(m, M)

#     def get_params(self, rates):
#         mean = rates[:self.num_species]
#         drift = rates[self.num_species:self.num_species*(self.num_species+1)].reshape(self.num_species, self.num_species)
#         sqrt_diff = rates[self.num_species*(self.num_species+1):].reshape(self.num_species, self.num_species)
#         return(mean, drift, sqrt_diff)

#     def set_cov_map(self):
#         """
#         Construct a tensor that maps the reduced moments to a full covariance matrix
#         """
#         cov_map = torch.tril_indices(row=self.num_species, col=self.num_species, offset=0)
#         return(cov_map)


class MOU_Process_Linear(MultivariateDiffLinearControl):
    """
    Multivariate Ornstein Uhlenbeck process with linear control of the form
    u(x,t) = sqrt_dff * (u0(t) + u1(t)*x)
    """

    def __init__(self, initial, tspan, drift, diff, mean, diffusion='raw', gradient_mode='natural'):
        """
        Overload constructor to include moment closure choice
        """
        AutogradModel.__init__(self, initial, torch.zeros(initial.shape), tspan, gradient_mode=gradient_mode)
        self.num_species = int(-1.5+np.sqrt(2*self.num_states()+2.25))
        self.drift = drift
        self.mean = mean
        if diffusion == 'raw':
            self.diff = diff @ diff.T
        elif diffusion == 'squared':
            self.diff = diff
        self.sqrt_diff = torch.tensor(np.linalg.cholesky(self.diff))
        self.cov_map = self.set_cov_map()
        self.rates = self.get_rates()

    def moments(self, time, m, M, u0, u1, rates):
        # parameters
        mean, drift, sqrt_diff = self.get_parameters(rates)
        # mean contribution
        dm = -drift @ (m - mean) + sqrt_diff @ (u0 + u1 @ m)
        # covariance contribution
        tmp = -drift @ M + sqrt_diff @ u1 @ M 
        dM = tmp + tmp.T + sqrt_diff @ sqrt_diff.T
        return(dm, dM)

    def get_parameters(self, rates):
        """
        Convert the rate vector to a parameter vector
        """
        mean = rates[:self.num_species]
        drift = rates[self.num_species:self.num_species*(self.num_species+1)].reshape((self.num_species, self.num_species))
        sqrt_diff = rates[self.num_species*(self.num_species+1):].reshape((self.num_species, self.num_species))
        return(mean, drift, sqrt_diff)

    def get_rates(self):
        """
        Convert drift and mean to rates
        """
        rates = torch.cat([self.mean.flatten(), self.drift.flatten(), self.sqrt_diff.flatten()])
        return(rates)


# class MOU_Process_Linear_Raw(AutogradModel, MultivariateLinearControl):
#     """
#     Multivariate Ornstein Uhlenbeck process with linear control of the form
#     u(x,t) = u0(t) + u1(t)*x
#     """

#     def __init__(self, initial, tspan, drift, diff, mean, diffusion='raw'):
#         """
#         Overload constructor to include moment closure choice
#         """
#         AutogradModel.__init__(self, initial, torch.zeros(initial.shape), tspan)
#         self.num_species = int(-1.5+np.sqrt(2*self.num_states()+2.25))
#         self.drift = drift
#         self.mean = mean
#         if diffusion == 'raw':
#             self.diff = diff @ diff.T
#         elif diffusion == 'squared':
#             self.diff = diff
#         self.sqrt_diff = torch.tensor(np.linalg.cholesky(self.diff))
#         self.cov_map = self.set_cov_map()

#     def forward_torch(self, time, state, control):
#         # get first moment and covariance
#         m, M = self.get_moments(state)
#         u0, u1 = self.get_controls(control)
#         # derivative of the mean
#         dm = -self.drift @ (m - self.mean) + (u0 + u1 @ m)
#         # derivative of the covariance
#         dM = -self.drift @ M + u1 @ M + M.T @ u1.T + self.diff
#         # combine to single vector
#         dydt = torch.zeros(state.shape)
#         dydt[:self.num_species] = dm
#         dydt[self.num_species:] = dM[self.cov_map[0], self.cov_map[1]]
#         return(dydt)

#     def num_controls(self):
#         return(self.num_species*(self.num_species+1))

#     def get_moments(self, state):
#         """
#         Convert state to mean and covariance matrix
#         """
#         # extract first moment
#         m = state[:self.num_species]
#         # construct second moment
#         tmp = torch.zeros((self.num_species, self.num_species))
#         tmp[self.cov_map[0], self.cov_map[1]] = state[self.num_species:]
#         M = tmp+tmp.T-torch.diag(tmp.diag())
#         # return mean and covariance
#         return(m, M)
    
#     def get_controls(self, control):
#         """
#         Convert vector to u0 vector and u1 matrix
#         """
#         u0 = control[:self.num_species]
#         u1 = control[self.num_species:].reshape((self.num_species, self.num_species))
#         return(u0, u1)

#     def set_cov_map(self):
#         """
#         Construct a tensor that maps the reduced moments to a full covariance matrix
#         """
#         cov_map = torch.tril_indices(row=self.num_species, col=self.num_species, offset=0)
#         return(cov_map) 

#     # def stat_tensor(self, time, forward):
#     #     inv_diff = np.linalg.inv(self.diff)
#     #     diag_ind = [i for i in range(self.num_species)]
#     #     # E[D(x)]
#     #     D0 = np.ones(forward.shape[:3]+(self.num_species, self.num_species)) * inv_diff.reshape((1, 1, 1, self.num_species, self.num_species))
#     #     # E[ D(x) x]
#     #     D1 = inv_diff.reshape((1, 1, 1, self.num_species, self.num_species, 1)) * forward[:, :, :, :self.num_species].reshape(forward.shape[:3]+(1, 1, self.num_species))
#     #     # E[ D(x) x x]
#     #     M = np.zeros(forward.shape[:3]+(self.num_species, self.num_species))
#     #     M[:, :, :, self.cov_map[0], self.cov_map[1]] = forward[:, :, :, self.num_species:]
#     #     M = M + np.swapaxes(M, -1, -2)
#     #     M[:, :, :, diag_ind, diag_ind] *= 0.5
#     #     M += np.expand_dims(forward[:, :, :, :self.num_species], axis=-1) @ np.expand_dims(forward[:, :, :, :self.num_species], axis=-2)
#     #     D2 = inv_diff.reshape((1, 1, 1, self.num_species, self.num_species, 1, 1)) * M.reshape(M.shape[:3]+(1, 1)+M.shape[3:]) 
#     #     # return
#     #     return(D0, D1, D2)


class GBM(DiffIndependentControl):
    """
    Goemetric Brownian motion with independent control of the form
    u(x,t) = delta * x * u(t)
    """

    def forward_torch(self, time, state, control, rates):
        """
        forward equation for the ou process
        """
        dydt = torch.zeros(state.shape)
        dydt[0] = (rates[0]+rates[1]*control)*state[0]
        dydt[1] = 2*(rates[0]+rates[1]*control)*state[1]+rates[1]**2*(state[1]+state[0]**2)
        return(dydt)

    def stat_tensor_torch(self, forward, rates):
        tmp = forward.sum()
        diff = torch.tensor([1.0])+(tmp-tmp)
        return(diff.unsqueeze(0))


# # class GBM_Diff(AutogradModel, IndependentControl):
# #     """
# #     Goemetric Brownian motion with independent control of the form
# #     u(x,t) = delta^2 * x^2 * u(t)
# #     """

# #     def forward_torch(self, time, state, control):
# #         """
# #         forward equation for the ou process
# #         """
# #         # evaluate moment closure
# #         hom = self.closure(state)
# #         m2 = state[1]+state[0]**2
# #         # evaluate derivative
# #         dydt = torch.zeros(state.shape)
# #         dydt[0] = self.rates[0]*state[0] + self.rates[1]**2*control*m2
# #         dydt[1] = 2*self.rates[0]*state[1] + 2*self.rates[1]*control*(hom-m2*state[0]) + self.rates[1]**2*m2
# #         return(dydt)

# #     def closure(self, state):
# #         """
# #         Lognormal closure for E[X^3]
# #         """
# #         hom = (state[1]+state[0]**2)**3/state[0]**3
# #         return(hom)

# #     def stat_tensor(self, time, forward):
# #         diff = self.rates[1].numpy()**2*(forward[:, :, :, [1]]+forward[:, :, :, [0]]**2)
# #         return(diff)

# #     def num_controls(self):
# #         return(1)


# # class GBM_Raw(AutogradModel, IndependentControl):
# #     """
# #     Goemetric Brownian motion with independent control of the form
# #     u(x,t) = u(t)
# #     """

# #     def forward_torch(self, time, state, control):
# #         """
# #         forward equation for the ou process
# #         """
# #         dydt = torch.zeros(state.shape)
# #         dydt[0] = self.rates[0]*state[0] + control
# #         dydt[1] = 2*self.rates[0]*state[1] + self.rates[1]**2*(state[1]+state[0]**2)
# #         return(dydt)

# #     def closure(self, forward):
# #         """
# #         Lognormal closure for E[X^-2]
# #         """
# #         hom = (forward[:, :, :, [1]]+forward[:, :, :, [0]])**2/forward[:, :, :, [0]]**8
# #         return(hom)

# #     def stat_tensor(self, time, forward):
# #         hom = self.closure(forward)
# #         diff = 1.0/(hom*self.rates[1].numpy()**2)
# #         return(diff)

# #     def num_controls(self):
# #         return(1)


class GBM_Linear(DiffLinearControl, AutogradModel):
    """
    Goemetric Brownian motion with linear control of the form
    u(x,t) = delta * x * (u0(t) + u1(t)*x)
    """

    def forward_torch(self, time, state, control, rates):
        """
        forward equation for the ou process
        """
        # evaluate moment closure
        hom = self.closure(state)
        m2 = state[1]+state[0]**2
        eff_rates = torch.exp(rates)
        # calculate derivative
        dydt = torch.zeros(state.shape)
        dydt[0] = eff_rates[0]*state[0] + eff_rates[1]*control[0]*state[0] + eff_rates[1]*control[1]*m2
        dydt[1] = 2*eff_rates[0]*state[1] + 2*eff_rates[1]*control[0]*state[1] + 2*eff_rates[1]*(hom-m2*state[0])*control[1] + eff_rates[1]**2*m2
        return(dydt)

    def stat_tensor_torch(self, forward, rates):
        diff = torch.ones(2, 2)
        diff[0, 1] = forward[0]
        diff[1, 0] = forward[0]
        diff[1, 1] = forward[1]+forward[0]**2
        return(diff)

    def kl_contrib_torch(self, time, state, control, rates):
        stat_tensor = self.stat_tensor_torch(state, rates)
        kl_contrib = control @ (stat_tensor @ control)
        if rates.requires_grad:
            kl_contrib.requires_grad = True
        return(kl_contrib)

    def closure(self, state):
        """
        Lognormal closure for E[X^3]
        """
        hom = (state[1]+state[0]**2)**3/state[0]**3
        return(hom)


class GBM_Linear_Diff(LinearControl, AutogradModel):
    """
    Goemetric Brownian motion with diffusion rescaled linear control of the form
    u(x,t) = delta^2 x^2 *( u_0(t) + u1(t)*x)
    """

    def forward_torch(self, time, state, control, rates):
        """
        forward equation for the ou process
        """
        # evaluate moment closure
        hom = self.closure(state)
        m2 = state[1]+state[0]**2
        # calculate derivative
        dydt = torch.zeros(state.shape)
        dm = rates[0]*state[0] + rates[1]**2*control[0]*m2 + rates[1]**2*control[1]*hom[0]
        dydt[0] = dm
        dydt[1] = 2*rates[0]*m2 + rates[1]**2*m2 + 2*rates[1]**2*control[0]*hom[0] + 2*rates[1]**2*control[1]*hom[1] - 2*dm*state[0]
        return(dydt)

    def stat_tensor_torch(self, forward, rates):
        # preparations
        diff = torch.zeros(2, 2)
        m = forward[0]
        M = forward[1] + m**2
        # fill tensor (using log normal closure)
        diff[0, 0] = M
        diff[0, 1] = (M/m)**3
        diff[1, 0] = (M/m)**3
        diff[1, 1] = (((M/m)**6))/m**2
        return(rates[1]**2*diff)

    # def stat_tensor(self, time, forward):
    #     # preparations
    #     stat_tensor = np.zeros(forward.shape[:3]+(2, 2))
    #     m = forward[:, :, :, 0]
    #     M = forward[:, :, :, 1] + m**2
    #     # E[D(X)] = delta^2 E[ X^2] contribution
    #     stat_tensor[:, :, :, 0, 0] = M
    #     stat_tensor[:, :, :, 0, 1] = (M/m)**3
    #     stat_tensor[:, :, :, 1, 0] = (M/m)**3
    #     stat_tensor[:, :, :, 1, 1] = (((M/m)**6))/m**2
    #     return(self.rates[1].numpy()**2*stat_tensor)
 
    def closure(self, state):
        """
        Lognormal closure for E[X^3] and E[X^4]
        """
        hom = torch.zeros(2)
        hom[0] = (state[1]+state[0]**2)**3/state[0]**3
        hom[1] = (state[1]+state[0]**2)**6/state[0]**8
        return(hom)


class GBM_Multivariate(MultivariateDiffLinearControl):
    """
    Goemetric Brownian motion with linear control of the form
    u(x,t) = delta * x * (u0(t) + u1(t)*x)
    """

    def __init__(self, initial, rates, tspan):
        """
        Overload constructor to include moment closure choice
        """
        super().__init__(initial, rates, tspan)
        self.num_species = int(-1.5+np.sqrt(2*self.num_states()+2.25))
        self.cov_map = self.set_cov_map()
        self.alpha = self.set_alpha()

    def forward_torch(self, time, state, control, rates):
        """
        forward equation for the ou process
        """
        # get first moment and covariance
        m, M = self.get_moments(state)
        u0, u1 = self.get_controls(control)
        # get closure for third order
        x3 = self.lognorm_closure(m, M, self.alpha).reshape(self.num_species, self.num_species, self.num_species)
        # get parameters
        r, R = self.get_params(rates)
        # derivative of the mean
        dm = r*m + (R@u0)*m + ((R@u1)*M).sum(axis=1)
        # derivative of the covariance
        tmp = M*(r.unsqueeze(1) + (R@u0).unsqueeze(1)) + (x3*(R@u1).unsqueeze(-1)).sum(axis=1)
        dM = tmp + tmp.T + (R@R.T)*M - torch.ger(m, dm) - torch.ger(dm, m)
        # convert to vector
        dydt = torch.zeros(state.shape)
        dydt[:self.num_species] = dm
        dydt[self.num_species:] = dM[self.cov_map[0], self.cov_map[1]]
        return(dydt)

    def stat_tensor_torch(self, forward, rates):
        # get first moment and covariance
        m, M = self.get_moments(forward)
        #M = M + torch.ger(m, m)
        # u0 vs u0 contribution
        u0_u0 = torch.eye(self.num_species)
        # u1 vs u1 contribution
        u1_u1 = (M.reshape((1, self.num_species, 1, self.num_species))*torch.eye(self.num_species).reshape((self.num_species, 1, self.num_species, 1))).reshape((self.num_species**2, self.num_species**2))
        # u0 vs u1
        u0_u1 = (torch.eye(self.num_species).reshape((self.num_species, self.num_species, 1))*m.reshape((1, 1, self.num_species))).reshape((self.num_species, self.num_species**2))
        # concatenate and return
        tmp = rates.sum()
        stat_tensor = torch.cat([torch.cat([u0_u0, u0_u1], axis=1), torch.cat([u0_u1.T, u1_u1], axis=1)]) + tmp - tmp
        return(0.5*stat_tensor)

    def get_moments(self, state):
        """
        Convert state to mean and covariance matrix
        """
        # extract first moment
        m = state[:self.num_species]
        # construct second moment
        tmp = torch.zeros((self.num_species, self.num_species))
        tmp[self.cov_map[0], self.cov_map[1]] = state[self.num_species:]
        M = tmp+tmp.T-torch.diag(tmp.diag())+torch.ger(m, m)
        # return mean and covariance
        return(m, M)
    
    def get_controls(self, control):
        """
        Convert vector to u0 vector and u1 matrix
        """
        u0 = control[:self.num_species]
        u1 = control[self.num_species:].reshape((self.num_species, self.num_species))
        return(u0, u1)  

    def lognorm_closure(self, m, M, alpha):
        """
        General lognormal closure E[X^\alpha] (multi-index notation)
        """
        tmp1 = m**(2*alpha)/torch.diag(M)**(0.5*alpha)
        power = 0.5 * (alpha.unsqueeze(2) @ alpha.unsqueeze(1)).reshape((alpha.shape[0], -1))
        tmp2 = (M.flatten()/torch.ger(m, m).flatten())**power
        res = tmp1.prod(axis=1)*tmp2.prod(axis=1)
        return(res)

    def get_params(self, rates):
        # first components correspond to log growth rate
        r = torch.exp(rates[:self.num_species])
        # rest corresponds to vectorized R matrix
        R = torch.zeros((self.num_species, self.num_species))
        R[self.cov_map[0], self.cov_map[1]] = rates[self.num_species:]
        return(r, R)

    def set_cov_map(self):
        """
        Construct a tensor that maps the reduced moments to a full covariance matrix
        """
        cov_map = torch.tril_indices(row=self.num_species, col=self.num_species, offset=0)
        return(cov_map)

    def set_alpha(self):
        alpha = torch.zeros(self.num_species, self.num_species, self.num_species, self.num_species)
        for i in range(self.num_species):
            for j in range(self.num_species):
                for k in range(self.num_species):
                    alpha[i, j, k, i] += 1.0
                    alpha[i, j, k, j] += 1.0
                    alpha[i, j, k, k] += 1.0
        return(alpha.reshape((-1, self.num_species)))


class Bistable(DiffIndependentControl):
    """
    Bistable toy model motion with independent control
    """

    def forward_torch(self, time, state, control, rates):
        """
        forward equation for the ou process
        """
        # evaluate moment closure
        hom = self.closure(state)
        # calculate derivative
        dydt = torch.zeros(state.shape)
        dydt[0] = 4*(state[0]-hom[0])+rates[0]*control
        dydt[1] = 8*(state[1]-hom[1]+state[0]*hom[0])+rates[0]**2
        return(dydt)

    def stat_tensor_torch(self, forward, rates):
        tmp = forward.sum()
        diff = torch.tensor([1.0])+(tmp-tmp)
        return(diff.unsqueeze(0))

    def closure(self, state):
        """
        Gaussian closure for E[X^3] and E[X^4]
        """
        hom = torch.zeros(2)
        hom[0] = state[0]**3 + 3*state[0]*state[1]
        hom[1] = state[0]**4 + 6*state[0]**2*state[1] + 3*state[1]**2
        return(hom)


class Bistable_Linear(DiffLinearControl, AutogradModel):
    """
    Bistable toy model motion with independent control
    """

    def forward_torch(self, time, state, control, rates):
        """
        forward equation for the ou process
        """
        # evaluate moment closure
        hom = self.closure(state)
        # calculate derivative
        dydt = torch.zeros(state.shape)
        dydt[0] = (4.0+rates[0]*control[1])*state[0] - 4.0*hom[0] + rates[0]*control[0]
        dydt[1] = 2.0*(4.0+rates[0]*control[1])*state[1] + 8.0*(state[0]*hom[0]-hom[1]) + rates[0]**2
        return(dydt)

    def stat_tensor_torch(self, forward, rates):
        diff = torch.ones(2, 2)
        diff[0, 1] = forward[0]
        diff[1, 0] = forward[0]
        diff[1, 1] = forward[1]+forward[0]**2
        return(diff.unsqueeze(0))

    def closure(self, state):
        """
        Gaussian closure for E[X^3] and E[X^4]
        """
        hom = torch.zeros(2)
        hom[0] = state[0]**3 + 3*state[0]*state[1]
        hom[1] = state[0]**4 + 6*state[0]**2*state[1] + 3*state[1]**2
        return(hom)


class CIR(DiffIndependentControl):
    """
    CIR model motion with independent control of the form
    u(x,t) = delta * sqrt{x} * u(t)
    """

    def __init__(self, initial, rates, tspan, closure='lognormal'):
        """
        Overload constructor to include moment closure choice
        """
        AutogradModel.__init__(self, initial, rates, tspan)
        closure_options = {'lognormal': self.lognorm_closure, 'gamma': self.gamma_closure}
        self.closure = closure_options[closure]

    def forward_torch(self, time, state, control, rates):
        """
        forward equation for the ou process
        """
        # evaluate moment closure
        hom = self.closure(state)
        # calculate derivative
        dydt = torch.zeros(state.shape)
        dydt[0] = self.rates[0]*(self.rates[1]-state[0]) + self.rates[2]*hom[0]*control
        dydt[1] = -2.0*self.rates[0]*state[1] + 2.0*self.rates[2]*(hom[1]-state[0]*hom[0])*control + self.rates[2]**2*state[0]
        return(dydt)

    def stat_tensor_torch(self, forward, rates):
        tmp = forward.sum()
        diff = torch.tensor([1.0])+(tmp-tmp)
        return(diff.unsqueeze(0))

    def lognorm_closure(self, state):
        """
        Lognormal closure for E[X^3] and E[X^4]
        """
        hom = torch.zeros(2)
        hom[0] = state[0]**(3.0/4.0)/(state[1]+state[0]**2)**(1.0/8.0)
        hom[1] = state[0]**(3.0/4.0)*(state[1]+state[0]**2)**(3.0/8.0)
        return(hom)

    def gamma_closure(self, state):
        """
        Lognormal closure for E[X^3] and E[X^4]
        """
        hom = torch.zeros(2)
        alpha = state[0]**2/state[1]
        beta = state[0]/state[1]
        hom[0] = torch.lgamma(alpha+0.5)-torch.lgamma(alpha)-0.5*torch.log(beta)
        hom[1] = torch.lgamma(alpha+1.5)-torch.lgamma(alpha)-1.5*torch.log(beta)
        return(torch.exp(hom))


class CIR_Diff(IndependentControl):
    """
    CIR model motion with independent control of the form
    u(x,t) = delta^2 * x * u(t)
    """

    def forward_torch(self, time, state, control, rates):
        """
        forward equation for the ou process
        """
        # calculate derivative
        dydt = torch.zeros(state.shape)
        dydt[0] = rates[0]*(rates[1]-state[0]) + rates[2]**2*control*state[0]
        dydt[1] = -2.0*rates[0]*state[1] + 2.0*rates[2]**2*control*state[1] + rates[2]**2*state[0]
        return(dydt)

    def stat_tensor_torch(self, forward, rates):
        diff = rates[2]**2*forward[0]
        return(diff.unsqueeze(0))

    # def stat_tensor(self, time, forward, rates):
    #     diff = rates[2]**2*forward[:, :, :, [0]]
    #     return(diff)


class CIR_Raw(IndependentControl):
    """
    CIR model motion with independent control of the form
    u(x,t) = u(t)
    """

    def __init__(self, initial, rates, tspan, closure='lognormal'):
        """
        Overload constructor to include moment closure choice
        """
        AutogradModel.__init__(self, initial, rates, tspan)
        self.set_closure(closure)

    def forward_torch(self, time, state, control, rates):
        """
        forward equation for the ou process
        """
        # calculate derivative
        dydt = torch.zeros(state.shape)
        dydt[0] = rates[0]*(rates[1]-state[0]) + control
        dydt[1] = -2.0*rates[0]*state[1] + rates[2]**2*state[0]
        return(dydt)
        
    def stat_tensor_torch(self, forward, rates):
        hom = self.closure(forward)
        diff = 1.0/(hom*rates[2]**2)
        return(diff.unsqueeze(0))

    # def stat_tensor_(self, time, forward, rates):
    #     hom = self.closure_vec(forward)
    #     diff = 1.0/(hom*rates[2].numpy()**2)
    #     return(diff)

    def set_closure(self, option):
        if option == 'lognormal':
            self.closure = self.lognorm_closure
            self.closure_vec = self.lognorm_closure_vec
        elif option == 'gamma':
            self.closure = self.gamma_closure
            self.closure_vec = self.gamma_closure_vec    
        else:
            raise ValueError('Unrecognized option ' + option)    

    def lognorm_closure(self, forward):
        """
        Lognormal closure for E[X^3] and E[X^4]
        """
        hom = (forward[1]+forward[0]**2)/forward[0]**3
        return(hom)

    def lognorm_closure_vec(self, forward):
        """
        Lognormal closure for E[X^3] and E[X^4]
        """
        hom = (forward[:, :, :, [1]]+forward[:, :, :, [0]]**2)/forward[:, :, :, [0]]**3
        return(hom)

    def gamma_closure(self, forward):
        """
        Lognormal closure for E[X^3] and E[X^4]
        """
        alpha = forward[0]**2/forward[1]
        beta = forward[0]/forward[1]
        hom = torch.lgamma(alpha-1.0)-torch.lgamma(alpha)+1.0*torch.log(beta)
        return(torch.exp(hom))

    def gamma_closure_vec(self, forward):
        """
        Lognormal closure for E[X^3] and E[X^4]
        """
        alpha = forward[:, :, :, [0]]**2/forward[:, :, :, [1]]
        beta = forward[:, :, :, [0]]/forward[:, :, :, [1]]
        hom = special.loggamma(alpha-1.0)-special.loggamma(alpha)+1.0*np.log(beta)
        return(np.exp(hom))


class CIR_Linear(DiffLinearControl, AutogradModel):
    """
    CIR model motion with diffusion scaled lineear control, i.e. 
    u(x,t) = delta*sqrt(x)*( u0 + u1*x )
    """

    def __init__(self, initial, rates, tspan, closure='lognormal'):
        """
        Overload constructor to include moment closure choice
        """
        AutogradModel.__init__(self, initial, rates, tspan)
        closure_options = {'lognormal': self.lognorm_closure, 'gamma': self.gamma_closure}
        self.closure = closure_options[closure]

    def forward_torch(self, time, state, control, rates):
        """
        forward equation for the ou process
        """
        # evaluate moment closure
        hom = self.closure(state)
        # calculate derivative
        dydt = torch.zeros(state.shape)
        tmp = self.rates[0]*(self.rates[1]-state[0]) + self.rates[2]*hom[0]*control[0] + self.rates[2]*hom[1]*control[1]
        dydt[0] = tmp
        dydt[1] = 2.0*self.rates[0]*state[0]*self.rates[1] - 2.0*self.rates[0]*(state[1]+state[0]**2) + self.rates[2]**2*state[0] + 2.0*self.rates[2]*hom[1]*control[0] + 2.0*self.rates[2]*hom[2]*control[1] - 2.0*tmp*state[0] 
        #dydt[1] = -2.0*self.rates[0]*state[1] + self.rates[2]**2*state[0] + 2.0*self.rates[2]*(hom[1]-state[0]*hom[0])*control[0] + 2.0*self.rates[2]*(hom[2]-state[0]*hom[1])*control[1] 
        return(dydt)

    def stat_tensor_torch(self, forward, rates):
        diff = torch.ones(2, 2)
        diff[0, 1] = forward[0]
        diff[1, 0] = forward[0]
        diff[1, 1] = forward[1]+forward[0]**2
        return(diff)

    def lognorm_closure(self, state):
        """
        Lognormal closure for E[X^3] and E[X^4]
        """
        hom = torch.zeros(3)
        m2 = state[1]+state[0]**2
        hom[0] = state[0]**(3.0/4.0)/m2**(1.0/8.0)
        hom[1] = state[0]**(3.0/4.0)*m2**(3.0/8.0)
        hom[2] = m2**(15.0/8.0)/state[0]**(5.0/4.0)
        return(hom)

    def gamma_closure(self, state):
        """
        Lognormal closure for E[X^3] and E[X^4]
        """
        hom = torch.zeros(3)
        alpha = state[0]**2/state[1]
        beta = state[0]/state[1]
        hom[0] = torch.lgamma(alpha+0.5)-torch.lgamma(alpha)-0.5*torch.log(beta)
        hom[1] = torch.lgamma(alpha+1.5)-torch.lgamma(alpha)-1.5*torch.log(beta)
        hom[2] = torch.lgamma(alpha+2.5)-torch.lgamma(alpha)-2.5*torch.log(beta)
        return(torch.exp(hom))


class Toy2D(DiffIndependentControl):
    """
    Bistable toy model motion with independent control
    """

    def forward_torch(self, time, state, control, rates):
        """
        forward equation for the ou process
        """
        # evaluate moment closure
        hom = self.closure(state)
        # calculate derivative
        dydt = torch.zeros(state.shape)
        dm1 = state[0] - hom[0] - hom[1] - state[1] + control[0]
        dm2 = 2*state[1] - hom[2] - hom[3] + control[1]
        dydt[0] = dm1
        dydt[1] = dm2
        dydt[2] = 2*(state[2] + state[0]**2) - 2*hom[4] - 2*hom[5] - 2*(state[3] + state[0]*state[1]) + 1.0 + 2*control[0]*state[0] - 2*state[0]*dm1
        dydt[3] = 3*(state[3] + state[0]*state[1]) - 2*hom[7] - 2*hom[8] - (state[4] + state[1]**2) + control[1]*state[0] + control[0]*state[1] - state[0]*dm2 - state[1]*dm1
        dydt[4] = 4*(state[4] + state[1]**2) - 2*hom[5] - 2*hom[6] + 1.0 + 2*control[1]*state[1] - 2*state[1]*dm2
        return(dydt)

    def stat_tensor_torch(self, forward, rates):
        tmp = forward.sum()
        num_controls = self.num_controls()
        diff = torch.eye(num_controls, num_controls)+(tmp-tmp)
        return(0.5*diff.flatten())

    def closure(self, state):
        """
        Gaussian closure for E[X^3] and E[X^4]
        """
        hom = torch.zeros(9)
        # x1^3
        hom[0] = state[0]**3 + 3*state[2]*state[0]
        # x1 * x2^2
        hom[1] = 2*state[3]*state[1] + state[0]*(state[4] + state[2]**2)
        # x1^2 * x2
        hom[2] = 2*state[3]*state[1] + state[1]*(state[2] + state[1]**2)
        # x2^3
        hom[3] = state[1]**3 + 3*state[4]*state[1]
        # x1^4
        hom[4] = 3*state[2]**2 + 6*state[2]*state[0]**2 + state[0]**4
        # x1^2 * y1^2
        hom[5] = state[2]*state[4] + state[2]*state[1]**2 + state[3]**2 + state[3]**2 + 4*state[3]*state[0]*state[1] + state[4]*state[0]**2 + state[0]**2*state[1**2]
        # x2^4
        hom[6] = 3*state[4]**2 + 6*state[4]*state[1]**2 + state[1]**4
        # x1^3 *x2
        hom[7] = 3*state[2]*state[3] + 3*state[2]*state[0]*state[1] + 3*state[0]**2*state[3] + state[0]**3*state[1]
        # x1 x2^3 = 
        hom[8] = 3*state[4]*state[3] + 3*state[4]*state[0]*state[1] + 3*state[1]**2*state[3] + state[0]*state[1]**3
        return(hom)


class Heston_Model(MultivariateDiffLinearControl):
    """
    Heston stochastic volatility model for asset price development
    """

    def __init__(self, initial, tspan, drift, offset, diffusion):
        """
        Overload constructor to include moment closure choice
        """
        AutogradModel.__init__(self, initial, torch.zeros(initial.shape), tspan)
        self.num_species = int(-1.5+np.sqrt(2*self.num_states()+2.25))
        self.drift = drift
        self.offset = offset
        self.sqrt_diff = diffusion
        self.cov_map = self.set_cov_map()
        self.alpha1 = torch.tensor([[1.0, 0.5], [0.0, 0.5]])
        self.alpha2 = (self.alpha1.unsqueeze(1) + torch.eye(self.num_species).unsqueeze(0)).reshape((-1, self.num_species))
        self.alpha3 = self.alpha1.reshape((self.num_species, 1, 1, self.num_species)) + torch.eye(self.num_species).reshape((1, 1, self.num_species, self.num_species)) + torch.eye(self.num_species).reshape((1, self.num_species, 1, self.num_species))
        self.alpha4 = (self.alpha1.unsqueeze(1) + self.alpha1.unsqueeze(0)).reshape((-1, self.num_species))


    # def forward_torch(self, time, state, control, rates):
    #     # get first moment and covariance
    #     m, M = self.get_moments(state)
    #     M += torch.ger(m, m)
    #     u0, u1 = self.get_controls(control)
    #     # get closure relations 
    #     alpha1 = torch.tensor([[1.0, 0.5], [0.0, 0.5]])
    #     h1 = cl.lognorm_closure(m, M, alpha1)
    #     alpha2 = (alpha1.unsqueeze(1) + torch.eye(len(m)).unsqueeze(0)).reshape((-1, self.num_species))
    #     h2 = cl.lognorm_closure(m, M, alpha2).reshape(self.num_species, self.num_species)
    #     alpha3 =  alpha1.reshape((self.num_species, 1, 1, self.num_species)) + torch.eye(self.num_species).reshape((1, 1, self.num_species, self.num_species)) + torch.eye(self.num_species).reshape((1, self.num_species, 1, self.num_species))
    #     h3 = cl.lognorm_closure(m, M, alpha3.reshape(-1, self.num_species)).reshape(self.num_species, self.num_species, self.num_species)
    #     alpha4 = (alpha1.unsqueeze(1) + alpha1.unsqueeze(0)).reshape((-1, self.num_species))
    #     h4 = cl.lognorm_closure(m, M, alpha4).reshape(self.num_species, self.num_species)
    #     # derivative of the mean
    #     dm = self.drift @ (m - self.offset) + h1 * (self.sqrt_diff @ u0) + ((self.sqrt_diff @ u1) * h2).sum(axis=1)
    #     # derivative of the covariance
    #     tmp = self.drift @ (M - torch.ger(self.offset, m)) + (self.sqrt_diff @ u0).unsqueeze(-1) * h2 + ((self.sqrt_diff @ u1).unsqueeze(-1) * h3).sum(axis=1)
    #     dM = tmp + tmp.T + (self.sqrt_diff @ self.sqrt_diff.T) * h4 - torch.ger(dm, m) - torch.ger(m, dm)
    #     # combine to single vector
    #     dydt = torch.zeros(state.shape)
    #     dydt[:self.num_species] = dm
    #     dydt[self.num_species:] = dM[self.cov_map[0], self.cov_map[1]]
    #     return(dydt)

    def moments(self, time, m, M, u0, u1, rates):
        # raw second order moments
        M += torch.ger(m, m)
        # get closure relations 
        h1 = cl.lognorm_closure(m, M, self.alpha1)
        h2 = cl.lognorm_closure(m, M, self.alpha2).reshape(self.num_species, self.num_species)
        h3 = cl.lognorm_closure(m, M, self.alpha3.reshape(-1, self.num_species)).reshape(self.num_species, self.num_species, self.num_species)
        h4 = cl.lognorm_closure(m, M, self.alpha4).reshape(self.num_species, self.num_species)
        # derivative of the mean
        dm = self.drift @ (m - self.offset) + h1 * (self.sqrt_diff @ u0) + ((self.sqrt_diff @ u1) * h2).sum(axis=1)
        # derivative of the covariance
        tmp = self.drift @ (M - torch.ger(self.offset, m)) + (self.sqrt_diff @ u0).unsqueeze(-1) * h2 + ((self.sqrt_diff @ u1).unsqueeze(-1) * h3).sum(axis=1)
        dM = tmp + tmp.T + (self.sqrt_diff @ self.sqrt_diff.T) * h4 - torch.ger(dm, m) - torch.ger(m, dm)
        return(dm, dM)        
          

         