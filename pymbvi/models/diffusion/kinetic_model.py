# This files contains a class of variational models 

import numpy as np
import torch

#from pymbvi.models.diffusion.autograd_model import AutogradModel
from pymbvi.models.diffusion.autograd_model import FullAutogradModel
from pymbvi.models.diffusion.independent_control import IndependentControl
from pymbvi.models.diffusion.linear_control import MultivariateLinearControl
import pymbvi.util as ut
import pymbvi.closure as cl

# global options
torch.set_default_dtype(torch.float64)


class KineticModelInd(IndependentControl):
    """
    Chemical chinetic model in langevin approximation
    requires only second order reactions involving different molecules
    u(x,t) = diag( sqrt{h(x)} ) * u(t)
    """

    def __init__(self, initial, pre, post, rates, tspan, volume=1.0):
        """
        Overload constructor to include moment closure choice
        """
        IndependentControl.__init__(self, initial, rates, tspan)
        self.pre = torch.tensor(pre, dtype=torch.float64)
        self.post = torch.tensor(post, dtype=torch.float64)
        self.S = self.post-self.pre
        self.num_species = self.pre.shape[1]
        self.cov_map = self.set_cov_map()
        self.volume = volume

    def forward_torch(self, time, state, control, rates):
        """
        forward equation for the ou process
        """
        # extract first and second moments
        m, M = self.get_moments(state)
        M += torch.ger(m, m)
        # evaluate moment closure
        prop = rates * self.lognorm_closure(m, M, self.pre)
        #pre1 = self.pre.repeat([1, self.num_species]).reshape(-1, self.num_species) + torch.eye(self.num_species).repeat([self.pre.shape[0], 1])
        pre1 = (self.pre.unsqueeze(1) + torch.eye(len(m)).unsqueeze(0)).reshape((-1, self.num_species))
        prop_z = rates.unsqueeze(1) * self.lognorm_closure(m, M, pre1).reshape((-1, self.num_species))
        # calculate derivative
        dydt = torch.zeros(state.shape)
        tmp1 = self.S.T @ (prop + torch.diag(prop) @ self.S @ control)
        dydt[0:self.num_species] = tmp1
        tmp2 = self.S.T @ (prop_z + torch.diag(self.S @ control) @prop_z)
        tmp3 = tmp2 + tmp2.T + self.S.T @ torch.diag(prop) @ self.S / self.volume - torch.ger(tmp1, m) - torch.ger(m, tmp1)
        dydt[self.num_species:] = tmp3[self.cov_map[0], self.cov_map[1]]
        return(dydt)

    def stat_tensor_torch(self, forward, rates):
        m, M = self.get_moments(forward)
        M += torch.ger(m, m)
        prop = rates * self.lognorm_closure(m, M, self.pre)
        diff = self.S.T @ torch.diag(prop) @ self.S / self.volume
        return(diff)

    def lognorm_closure(self, m, M, alpha):
        """
        General lognormal closure E[X^\alpha] (multi-index notation)
        """
        tmp1 = m**(2*alpha)/torch.diag(M)**(0.5*alpha)
        power = 0.5 * (alpha.unsqueeze(2) @ alpha.unsqueeze(1)).reshape((alpha.shape[0], -1))
        tmp2 = (M.flatten()/torch.ger(m, m).flatten())**power
        res = tmp1.prod(axis=-1)*tmp2.prod(axis=-1)
        return(res)

    def lognorm_closure2(self, m, M, alpha):
        """
        General lognormal closure E[X^\alpha] (multi-index notation)
        """
        tmp1 = 2*alpha*torch.log(m.unsqueeze(0)) - 0.5*alpha*torch.log(torch.diag(M).unsqueeze(0))
        power = 0.5 * (alpha.unsqueeze(2) @ alpha.unsqueeze(1)).reshape((alpha.shape[0], -1))
        tmp2 = power * (torch.log(M) - torch.log(torch.ger(m, m))).flatten().unsqueeze(0)
        res = tmp1.sum(axis=1) + tmp2.sum(axis=1)
        return(torch.exp(res))


class KineticModelLinear(MultivariateLinearControl):
    """
    Chemical chinetic model in langevin approximation
    requires only second order reactions involving different molecules
    u(x,t) = V^T diag(h(x)) V * (u0(t) + u1(t) x)
    """

    def __init__(self, initial, pre, post, rates, tspan, volume=1.0, gradient_mode='natural'):
        """
        Overload constructor to include moment closure choice
        """
        MultivariateLinearControl.__init__(self, initial, rates, tspan, gradient_mode=gradient_mode)
        self.pre = torch.tensor(pre, dtype=torch.float64)
        self.post = torch.tensor(post, dtype=torch.float64)
        self.S = self.post-self.pre
        self.num_species = self.pre.shape[1]
        self.num_reactions = self.pre.shape[0]
        self.cov_map = self.set_cov_map()
        self.volume = volume

    def forward_torch(self, time, state, control, rates):
        """
        forward equation for the ou process
        """
        # extract first and second moments
        m, M = self.get_moments(state)
        M += torch.ger(m, m)
        u0, u1 = self.get_controls(control)
        rates_eff = torch.exp(rates)
        # evaluate moment closure
        prop = rates_eff * cl.lognorm_closure(m, M, self.pre)
        pre1 = (self.pre.unsqueeze(1) + torch.eye(len(m)).unsqueeze(0)).reshape((-1, self.num_species))
        prop_z = rates_eff.unsqueeze(1) * cl.lognorm_closure(m, M, pre1).reshape((-1, self.num_species))
        pre2 = self.pre.reshape((self.num_reactions, 1, 1, self.num_species)) + torch.eye(self.num_species).reshape((1, 1, self.num_species, self.num_species)) + torch.eye(self.num_species).reshape((1, self.num_species, 1, self.num_species))
        prop_z2 = rates_eff.reshape((self.num_reactions, 1, 1)) * cl.lognorm_closure(m, M, pre2.reshape((-1, self.num_species))).reshape((self.num_reactions, self.num_species, self.num_species))
        # calculate derivative
        dydt = torch.zeros(state.shape)
        tmp1 = self.S.T @ (prop + torch.diag(prop) @ self.S @ u0 + torch.sum((self.S @ u1) * prop_z, axis=1))
        dydt[0:self.num_species] = tmp1
        tmp2 = self.S.T @ ( (prop_z + torch.diag(self.S @ u0) @prop_z) + torch.sum((self.S @ u1).unsqueeze(-1) * prop_z2, axis=1) )
        tmp3 = tmp2 + tmp2.T + self.S.T @ torch.diag(prop) @ self.S / self.volume - torch.ger(tmp1, m) - torch.ger(m, tmp1)
        dydt[self.num_species:] = tmp3[self.cov_map[0], self.cov_map[1]]
        return(dydt)

    def stat_tensor_stats(self, forward, rates):
        # get required quantities
        m, M = self.get_moments(forward)
        M += torch.ger(m, m)
        rates_eff = torch.exp(rates)
        # compute expected propensities 
        prop = rates_eff * cl.lognorm_closure(m, M, self.pre)
        pre1 = (self.pre.unsqueeze(1) + torch.eye(len(m)).unsqueeze(0)).reshape((-1, self.num_species))
        prop_z = rates_eff.unsqueeze(1) * cl.lognorm_closure(m, M, pre1).reshape((-1, self.num_species))
        pre2 = self.pre.reshape((self.num_reactions, 1, 1, self.num_species)) + torch.eye(self.num_species).reshape((1, 1, self.num_species, self.num_species)) + torch.eye(self.num_species).reshape((1, self.num_species, 1, self.num_species))
        prop_z2 = rates_eff.reshape((self.num_reactions, 1, 1)) * cl.lognorm_closure(m, M, pre2.reshape((-1, self.num_species))).reshape((self.num_reactions, self.num_species, self.num_species))
        # u0 vs u0 contribution
        D0 = self.S.T @ torch.diag(prop) @ self.S
        # u1 vs u1 contribution
        D2 = (torch.tensordot(self.S.T.unsqueeze(axis=0) * self.S.T.unsqueeze(axis=1), prop_z2, dims=1))
        # u0 vs u1
        D1 = ((self.S.reshape((self.num_reactions, self.num_species, 1, 1)) * self.S.reshape((self.num_reactions, 1, self.num_species, 1))) * prop_z.reshape((self.num_reactions, 1, 1, self.num_species))).sum(axis=0)
        return(D0/self.volume, D1/self.volume, D2/self.volume)

    def stat_tensor_torch(self, forward, rates):
        # get required quantities
        D0, D1, D2 = self.stat_tensor_stats(forward, rates)
        u0_u0 = D0
        u0_u1 = D1.reshape((self.num_species, self.num_species**2))
        u1_u1 = D2.transpose(1, 2).reshape((self.num_species**2, self.num_species**2))
        # concatenate and return
        stat_tensor = torch.cat([torch.cat([u0_u0, u0_u1], axis=1), torch.cat([u0_u1.T, u1_u1], axis=1)])
        return(stat_tensor)

    # def stat_tensor(self, time, forward, rates):
    #     #print("test")
    #     # set up outpt
    #     dim = forward.shape[:3]
    #     D0 = torch.zeros(dim+(self.num_species, self.num_species))
    #     D1 = torch.zeros(dim+(self.num_species, self.num_species, self.num_species))
    #     D2 = torch.zeros(dim+(self.num_species, self.num_species, self.num_species, self.num_species))
    #     forward_torch = torch.from_numpy(forward)
    #     for i in range(dim[0]):
    #         for j in range(dim[1]):
    #             for k in range(dim[2]):
    #                 m, M = self.get_moments(forward_torch[i, j, k])
    #                 prop = self.rates * self.lognorm_closure(m, M, self.pre)
    #                 pre1 = (self.pre.unsqueeze(1) + torch.eye(len(m)).unsqueeze(0)).reshape((-1, self.num_species))
    #                 prop_z = self.rates.unsqueeze(1) * self.lognorm_closure(m, M, pre1).reshape((-1, self.num_species))
    #                 pre2 = self.pre.reshape((self.num_reactions, 1, 1, self.num_species)) + torch.eye(self.num_species).reshape((1, 1, self.num_species, self.num_species)) + torch.eye(self.num_species).reshape((1, self.num_species, 1, self.num_species))
    #                 prop_z2 = self.rates.reshape((self.num_reactions, 1, 1)) * self.lognorm_closure(m, M, pre2.reshape((-1, self.num_species))).reshape((self.num_reactions, self.num_species, self.num_species))
    #                 D0[i, j, k] = self.S.T @ torch.diag(prop) @ self.S
    #                 D1[i, j, k] = ((self.S.reshape((self.num_reactions, self.num_species, 1, 1)) * self.S.reshape((self.num_reactions, 1, self.num_species, 1))) * prop_z.reshape((self.num_reactions, 1, 1, self.num_species))).sum(axis=0)
    #                 D2[i, j, k] = torch.tensordot(self.S.T.unsqueeze(axis=0) * self.S.T.unsqueeze(axis=1), prop_z2, dims=1)
    #     return(D0.numpy(), D1.numpy(), D2.numpy())

    def stat_tensor(self, time, forward, rates):
        # set up outpt
        dim = forward.shape[:3]
        D0 = torch.zeros(dim+(self.num_species, self.num_species))
        D1 = torch.zeros(dim+(self.num_species, self.num_species, self.num_species))
        D2 = torch.zeros(dim+(self.num_species, self.num_species, self.num_species, self.num_species))
        forward_torch = torch.from_numpy(forward)
        rates_torch = torch.from_numpy(rates)
        for i in range(dim[0]):
            for j in range(dim[1]):
                for k in range(dim[2]):
                    D0_tmp, D1_tmp, D2_tmp = self.stat_tensor_stats(forward_torch[i, j, k], rates_torch)
                    D0[i, j, k] = D0_tmp
                    D1[i, j, k] = D1_tmp
                    D2[i, j, k] = D2_tmp
        return(D0.numpy(), D1.numpy(), D2.numpy())


class KineticModelLinearFA(FullAutogradModel):
    """
    Chemical chinetic model in langevin approximation
    requires only second order reactions involving different molecules
    u(x,t) = V^T diag(h(x)) V * (u0(t) + u1(t) x)
    """

    def __init__(self, initial, pre, post, rates, tspan, volume=1.0, gradient_mode='natural'):
        """
        Overload constructor to include moment closure choice
        """
        FullAutogradModel.__init__(self, initial, rates, tspan, gradient_mode=gradient_mode)
        self.pre = torch.tensor(pre, dtype=torch.float64)
        self.post = torch.tensor(post, dtype=torch.float64)
        self.S = self.post-self.pre
        self.num_species = self.pre.shape[1]
        self.num_reactions = self.pre.shape[0]
        self.cov_map = self.set_cov_map()
        self.volume = volume

    def forward_torch(self, time, state, control, rates):
        """
        forward equation for the ou process
        """
        # extract first and second moments
        m, M = self.get_moments(state)
        u0, u1 = self.get_controls(control)
        rates_eff = torch.exp(rates)
        # evaluate moment closure
        prop = rates_eff * cl.lognorm_closure(m, M, self.pre)
        pre1 = (self.pre.unsqueeze(1) + torch.eye(len(m)).unsqueeze(0)).reshape((-1, self.num_species))
        prop_z = rates_eff.unsqueeze(1) * cl.lognorm_closure(m, M, pre1).reshape((-1, self.num_species))
        pre2 = self.pre.reshape((self.num_reactions, 1, 1, self.num_species)) + torch.eye(self.num_species).reshape((1, 1, self.num_species, self.num_species)) + torch.eye(self.num_species).reshape((1, self.num_species, 1, self.num_species))
        prop_z2 = rates_eff.reshape((self.num_reactions, 1, 1)) * cl.lognorm_closure(m, M, pre2.reshape((-1, self.num_species))).reshape((self.num_reactions, self.num_species, self.num_species))
        # calculate derivative
        dydt = torch.zeros(state.shape)
        tmp1 = self.S.T @ (prop + torch.diag(prop) @ self.S @ u0 / self.volume + torch.sum((self.S @ u1 / self.volume) * prop_z, axis=1))
        dydt[0:self.num_species] = tmp1
        tmp2 = self.S.T @ ( (prop_z + torch.diag(self.S @ u0 / self.volume) @prop_z) + torch.sum((self.S @ u1 / self.volume).unsqueeze(-1) * prop_z2, axis=1) )
        tmp3 = tmp2 + tmp2.T + self.S.T @ torch.diag(prop) @ self.S/self.volume - torch.ger(tmp1, m) - torch.ger(m, tmp1)
        dydt[self.num_species:] = tmp3[self.cov_map[0], self.cov_map[1]]
        return(dydt)

    def kl_contrib_torch(self, time, state, control, rates):
        """
        forward equation for the ou process
        """
        # get controls 
        u0, u1 = self.get_controls(control)
        # get stats
        D0, D1, D2 = self.stat_tensor_stats(state, rates)
        # compute kl contribution
        kl0 = u0 @ D0 @ u0
        kl1 = (u0.reshape((self.num_species, 1, 1)) * u1.unsqueeze(0) * D1).sum()
        kl2 = (u1.reshape((self.num_species, 1, self.num_species, 1)) * u1.reshape(1, self.num_species, 1, self.num_species) * D2).sum()
        kl_contrib = kl0 + 2*kl1 + kl2
        return(0.5*kl_contrib)

    def stat_tensor_stats(self, forward, rates):
        # get required quantities
        m, M = self.get_moments(forward)
        rates_eff = torch.exp(rates)
        # compute propensity expectations
        prop = rates_eff * cl.lognorm_closure(m, M, self.pre)
        pre1 = (self.pre.unsqueeze(1) + torch.eye(len(m)).unsqueeze(0)).reshape((-1, self.num_species))
        prop_z = rates_eff.unsqueeze(1) * cl.lognorm_closure(m, M, pre1).reshape((-1, self.num_species))
        pre2 = self.pre.reshape((self.num_reactions, 1, 1, self.num_species)) + torch.eye(self.num_species).reshape((1, 1, self.num_species, self.num_species)) + torch.eye(self.num_species).reshape((1, self.num_species, 1, self.num_species))
        prop_z2 = rates_eff.reshape((self.num_reactions, 1, 1)) * cl.lognorm_closure(m, M, pre2.reshape((-1, self.num_species))).reshape((self.num_reactions, self.num_species, self.num_species))
        # u0 vs u0 contribution
        D0 = self.S.T @ torch.diag(prop) @ self.S
        # u1 vs u1 contribution
        D2 = (torch.tensordot(self.S.T.unsqueeze(axis=0) * self.S.T.unsqueeze(axis=1), prop_z2, dims=1))
        # u0 vs u1
        D1 = ((self.S.reshape((self.num_reactions, self.num_species, 1, 1)) * self.S.reshape((self.num_reactions, 1, self.num_species, 1))) * prop_z.reshape((self.num_reactions, 1, 1, self.num_species))).sum(axis=0)
        return(D0/self.volume, D1/self.volume, D2/self.volume)

    # def stat_tensor_torch(self, forward, rates):
    #     # get required quantities
    #     D0, D1, D2 = self.stat_tensor_stats(forward, rates)
    #     u0_u0 = D0
    #     u0_u1 = D1.reshape((self.num_species, self.num_species**2))
    #     u1_u1 = D2.transpose(0, 1).reshape((self.num_species**2, self.num_species**2))
    #     # concatenate and return
    #     stat_tensor = torch.cat([torch.cat([u0_u0, u0_u1], axis=1), torch.cat([u0_u1.T, u1_u1], axis=1)])
    #     return(0.5*stat_tensor)

    # def stat_tensor(self, time, forward, rates):
    #     #print("test")
    #     # set up outpt
    #     dim = forward.shape[:3]
    #     D0 = torch.zeros(dim+(self.num_species, self.num_species))
    #     D1 = torch.zeros(dim+(self.num_species, self.num_species, self.num_species))
    #     D2 = torch.zeros(dim+(self.num_species, self.num_species, self.num_species, self.num_species))
    #     forward_torch = torch.from_numpy(forward)
    #     for i in range(dim[0]):
    #         for j in range(dim[1]):
    #             for k in range(dim[2]):
    #                 m, M = self.get_moments(forward_torch[i, j, k])
    #                 prop = self.rates * self.lognorm_closure(m, M, self.pre)
    #                 pre1 = (self.pre.unsqueeze(1) + torch.eye(len(m)).unsqueeze(0)).reshape((-1, self.num_species))
    #                 prop_z = self.rates.unsqueeze(1) * self.lognorm_closure(m, M, pre1).reshape((-1, self.num_species))
    #                 pre2 = self.pre.reshape((self.num_reactions, 1, 1, self.num_species)) + torch.eye(self.num_species).reshape((1, 1, self.num_species, self.num_species)) + torch.eye(self.num_species).reshape((1, self.num_species, 1, self.num_species))
    #                 prop_z2 = self.rates.reshape((self.num_reactions, 1, 1)) * self.lognorm_closure(m, M, pre2.reshape((-1, self.num_species))).reshape((self.num_reactions, self.num_species, self.num_species))
    #                 D0[i, j, k] = self.S.T @ torch.diag(prop) @ self.S
    #                 D1[i, j, k] = ((self.S.reshape((self.num_reactions, self.num_species, 1, 1)) * self.S.reshape((self.num_reactions, 1, self.num_species, 1))) * prop_z.reshape((self.num_reactions, 1, 1, self.num_species))).sum(axis=0)
    #                 D2[i, j, k] = torch.tensordot(self.S.T.unsqueeze(axis=0) * self.S.T.unsqueeze(axis=1), prop_z2, dims=1)
    #     return(D0.numpy(), D1.numpy(), D2.numpy())

    # def stat_tensor(self, time, forward, rates):
    #     # set up outpt
    #     dim = forward.shape[:3]
    #     D0 = torch.zeros(dim+(self.num_species, self.num_species))
    #     D1 = torch.zeros(dim+(self.num_species, self.num_species, self.num_species))
    #     D2 = torch.zeros(dim+(self.num_species, self.num_species, self.num_species, self.num_species))
    #     forward_torch = torch.from_numpy(forward)
    #     rates_torch = torch.from_numpy(rates)
    #     for i in range(dim[0]):
    #         for j in range(dim[1]):
    #             for k in range(dim[2]):
    #                 D0_tmp, D1_tmp, D2_tmp = self.stat_tensor_stats(forward_torch[i, j, k], rates_torch)
    #                 D0[i, j, k] = D0_tmp
    #                 D1[i, j, k] = D1_tmp
    #                 D2[i, j, k] = D2_tmp
    #     return(D0.numpy(), D1.numpy(), D2.numpy())

    def set_cov_map(self):
        """
        Construct a tensor that maps the reduced moments to a full covariance matrix
        """
        cov_map = torch.tril_indices(row=self.num_species, col=self.num_species, offset=0)
        return(cov_map)

    def get_moments(self, state):
        """
        Convert state to mean and covariance matrix
        """
        # extract first moment
        m = state[:self.num_species]
        # construct second moment
        tmp = torch.zeros((self.num_species, self.num_species))
        tmp[self.cov_map[0], self.cov_map[1]] = state[self.num_species:]
        M = tmp+tmp.T-torch.diag(tmp.diag())+torch.ger(m, m)
        # return mean and covariance
        return(m, M)

    def get_controls(self, control):
        """
        Convert vector to u0 vector and u1 matrix
        """
        u0 = control[:self.num_species]
        u1 = control[self.num_species:].reshape((self.num_species, self.num_species))
        return(u0, u1)

    def num_controls(self):
        return(self.num_species*(self.num_species+1))