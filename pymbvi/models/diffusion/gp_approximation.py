# This files contains a class of variational models that use the torch autograd framework to compute required derivatives

import numpy as np
import torch
# from scipy.linalg import solve
from pymbvi.models.model import Model
from pymbvi.models.diffusion.autograd_model import FullAutogradModel
import pymbvi.util as ut

# global options
torch.set_default_dtype(torch.float64)


# class GpApproximation(Model):
#     """
#     This class is a replication of the Opper & Sanguinette framework for linear SDE approximations
#     """

#     # constructor and setup

#     def __init__(self, initial, rates, tspan):
#         """
#         Constructor method
#         """
#         # store inputs
#         self.initial = initial
#         self.rates = rates
#         self.tspan = tspan
#         # derived quantities
#         self.num_moments = len(initial)
#         self.num_reactions = len(rates)
#         self.num_species = int(-1.5+np.sqrt(2*self.num_states()+2.25))
#         self.cov_map = self.set_cov_map()

#     # interface functions required by abstract Model class

#     def num_states(self):
#         return(len(self.initial.numpy()))

#     def num_param(self):
#         return(len(self.rates.numpy()))

#     def forward(self, time, state, control, rates):
#         """ 
#         Compute r.h.s of the forward differential equations 
#         """
#         # get first moment and covariance
#         m, M = self.get_moments(state)
#         u0, u1 = self.get_controls(control)
#         diff = self.get_diffusion(rates)
#         # evaluate derivatie of the mean
#         dm = u0 + u1 @ m
#         # evaluate derivative of the covariance
#         dM = u1 @ M + M.T@u1.T + diff
#         # construct state vector derivative
#         dydt = np.zeros(state.shape)
#         dydt[:self.num_species] = dm
#         dydt[self.num_species:] = dM[self.cov_map[0], self.cov_map[1]]
#         return(dydt)

#     def backward(self, time, state, control, forward_time, forward, rates):
#         """
#         Adjoint equation
#         """

#     # def backward(self, time, state, control, forward_time, forward, rates):
#     #     # perform interpolation
#     #     forward = ut.interp_pwc(time, forward_time, forward)
#     #     # compute control part
#     #     control_eff = self.effective_control(control, rates)
#     #     # convert to torch stuff
#     #     forward_torch = torch.from_numpy(forward)
#     #     forward_torch.requires_grad = True
#     #     control_torch = torch.from_numpy(control)
#     #     rates_torch = torch.from_numpy(rates)
#     #     # compute contribution of the stat tensor
#     #     tmp = self.stat_tensor_torch(forward_torch, rates_torch)
#     #     tmp.backward(torch.from_numpy(control_eff))
#     #     dydt = np.array(forward_torch.grad)
#     #     forward_torch.grad.zero_()
#     #     # add contribution from the forward equation
#     #     tmp = self.forward_torch(time, forward_torch, control_torch, rates_torch)
#     #     tmp.backward(torch.from_numpy(state))
#     #     dydt -= np.array(forward_torch.grad)
#     #     return(dydt)

#     # def constraint_gradient(self, control, forward, backward, rates):
#     #     """
#     #     Evaluates the gradient of the constraint with respect to the controls
#     #     """
#     #     # preparations
#     #     dim = forward.shape
#     #     grad = np.zeros(dim[:3]+control.shape[-1:])
#     #     rates_torch = torch.from_numpy(rates)
#     #     # contributions lagrange multpliers
#     #     for i in range(dim[0]):
#     #         for j in range(dim[1]):
#     #             control_torch = torch.tensor(control[i, j], requires_grad=True)
#     #             for k in range(dim[2]):
#     #                 forward_torch = torch.from_numpy(forward[i, j, k])
#     #                 backward_torch = torch.from_numpy(backward[i, j, k])
#     #                 tmp = self.forward_torch(i, forward_torch, control_torch, rates_torch)
#     #                 tmp.backward(backward_torch)
#     #                 grad[i, j, k] = np.array(control_torch.grad)
#     #                 control_torch.grad.zero_()
#     #     return(grad)

#     # def stat_tensor(self, time, forward, rates):
#     #     # preparations
#     #     forward_torch = torch.from_numpy(forward)
#     #     rates_torch = torch.from_numpy(rates)
#     #     tmp = self.stat_tensor_torch(forward_torch[0, 0, 0], rates_torch)
#     #     dim = forward.shape[:3]+tmp.shape
#     #     diff = torch.zeros(dim)
#     #     # iterate 
#     #     for i in range(dim[0]):
#     #         for j in range(dim[1]):
#     #             for k in range(dim[2]):
#     #                 diff[i, j, k] = self.stat_tensor_torch(forward_torch[i, j, k], rates_torch)
#     #     return(diff.numpy())

#     def get_initial(self):
#         return(self.initial.numpy())

#     def get_moments(self, state):
#         """
#         Convert state to mean and covariance matrix
#         """
#         # extract first moment
#         m = state[:self.num_species]
#         # construct second moment
#         tmp = torch.zeros((self.num_species, self.num_species))
#         tmp[self.cov_map[0], self.cov_map[1]] = state[self.num_species:]
#         M = tmp+tmp.T-torch.diag(tmp.diag())
#         # return mean and covariance
#         return(m, M)
    
#     def get_controls(self, control):
#         """
#         Convert vector to u0 vector and u1 matrix
#         """
#         u0 = control[:self.num_species]
#         u1 = control[self.num_species:].reshape((self.num_species, self.num_species))
#         return(u0, u1)

#     def set_cov_map(self):
#         """
#         Construct a tensor that maps the reduced moments to a full covariance matrix
#         """
#         cov_map = torch.tril_indices(row=self.num_species, col=self.num_species, offset=0)
#         return(cov_map)    


class GpApproximationAutograd(FullAutogradModel):
    """
    Alternative implementation that exploits the autograd base model
    """

    def __init__(self, initial, rates, tspan, diffusion, gradient_mode='regular'):
        """
        Overload constructor to include moment closure choice
        """
        FullAutogradModel.__init__(self, initial, rates, tspan, gradient_mode=gradient_mode)
        self.num_species = int(-1.5+np.sqrt(2*self.num_states()+2.25))
        self.cov_map = self.set_cov_map()
        self.sqrt_diff = self.set_diffusion(diffusion)

    def forward_torch(self, time, state, control, rates):
        # get first moment and covariance
        m, M = self.get_moments(state)
        u0, u1 = self.get_controls(control)
        diff = self.get_diffusion(rates)
        # evaluate derivatie of the mean
        dm = u0 + u1 @ m
        # evaluate derivative of the covariance
        dM = u1 @ M + M.T@u1.T + diff
        # combine to single vector
        dydt = torch.zeros(state.shape)
        dydt[:self.num_species] = dm
        dydt[self.num_species:] = dM[self.cov_map[0], self.cov_map[1]]
        return(dydt)

    def kl_contrib_torch(self, time, state, control, rates):
        """
        contribution of moments and controls to the prior kl
        """
        # get first moment, covariance and control
        m, M = self.get_moments(state)
        u0, u1 = self.get_controls(control)
        diff = self.get_diffusion(rates)
        diff_inv = torch.inverse(diff)
        # get drift statistics    
        a, a_x, a_a = self.get_drift_stats(m, M, rates)
        # u0 contribution
        u0_u0 = u0.T @ diff_inv @ u0
        # u0 u1 contribution
        u0_u1 = 2 * u0.T @ diff_inv @ u1 @ m
        # pure u1 contribution
        u1_u1 = ((u1.T @ diff_inv @ u1) * (M + torch.ger(m, m))).sum()
        # a(x) u0 contribution
        a_u0 = -2 * u0.T @ diff_inv @ a
        # contribution of x a(x) and u1
        a_x_u1 = -2 * ((diff_inv @ u1) * a_x).sum()
        # contribution of a(x) squared
        a_D_a = (diff_inv * a_a).sum()
        # compute full output
        kl_contrib = u0_u0 + u0_u1 + u1_u1 + a_u0 + a_x_u1 + a_D_a
        if kl_contrib < 0.0:
            print(f'Negative KL: {kl_contrib}')
            print(f'u0_u0: {u0_u0}')
            print(f'u0_u1: {u0_u1}')
            print(f'u1_u1: {u1_u1}')
            print(f'a_u0: {a_u0}')
            print(f'a_x_u1: {a_x_u1}')
            print(f'a_D_a: {a_D_a}')
            print(u0)
            print(u1)
            print(m)
            print(M)
            quit()
            # if a_D_a < 0.0:
            #     print(m)
            #     print(M)
            #     print(a_a)
            #     quit()
        return(0.5*kl_contrib)

#     def kl_prior_torch(self, time, forward, control, rates):
# # get first moment, covariance and control
#         m, M = self.get_moments(forward)
#         u0, u1 = self.get_controls(control)
#         diff = self.get_diffusion(rates)
#         diff_inv = torch.inverse(diff)
#         # u0 contribution
#         u0_u0 = u0.T @ diff_inv @ u0
#         # u0 u1 contribution
#         u0_u1 = 2 * u0.T @ diff_inv @ u1 @ m
#         # pure u1 contribution
#         u1_u1 = ((u1.T @ diff_inv @ u1) * (M + torch.ger(m, m))).sum()
#         # compute full output
#         kl_contrib = u0_u0 + u0_u1 + u1_u1
#         return(0.5*kl_contrib)


#     def fisher_loc(self, time, control, forward, rates):
#         """
#         Required for natural gradient mode
#         """
#         def fun(x):
#             return(self.kl_prior_torch(time, forward, x, rates))
#         fisher = ut.autograd_hessian(fun, control).numpy()
#         return(fisher)

    def fisher_loc(self, time, control, forward, rates):
        """
        Required for natural gradient mode
        """
        # get required quantities
        D0, D1, D2 = self.stat_tensor_stats(forward, rates)
        u0_u0 = D0
        u0_u1 = D1.reshape((self.num_species, self.num_species**2))
        u1_u1 = D2.transpose(1, 2).reshape((self.num_species**2, self.num_species**2))
        # concatenate and return
        fisher = torch.cat([torch.cat([u0_u0, u0_u1], axis=1), torch.cat([u0_u1.T, u1_u1], axis=1)])
        return(fisher)

    def stat_tensor_stats(self, forward, rates):
        # get required quantities
        m, M = self.get_moments(forward)
        M2 = M + torch.ger(m, m)
        diff = self.get_diffusion(rates)
        diff_inv = torch.inverse(diff)
        # u0_u0 contribution
        D0 = diff_inv
        # u0_u1 contribution
        D1 = diff_inv.unsqueeze(2) * m.reshape((1, 1) + m.shape)
        # u1_u1 contribution
        D2 = diff_inv.reshape(diff_inv.shape + (1, 1)) * M2.reshape((1, 1) + M.shape)
        return(D0, D1, D2)

    def num_controls(self):
        return(self.num_species*(self.num_species+1))

    def get_moments(self, state):
        """
        Convert state to mean and covariance matrix
        """
        # extract first moment
        m = state[:self.num_species]
        # construct second moment
        tmp = torch.zeros((self.num_species, self.num_species))
        tmp[self.cov_map[0], self.cov_map[1]] = state[self.num_species:]
        M = tmp+tmp.T-torch.diag(tmp.diag())
        # return mean and covariance
        return(m, M)
    
    def get_controls(self, control):
        """
        Convert vector to u0 vector and u1 matrix
        """
        u0 = control[:self.num_species]
        u1 = control[self.num_species:].reshape((self.num_species, self.num_species))
        return(u0, u1)

    def set_cov_map(self):
        """
        Construct a tensor that maps the reduced moments to a full covariance matrix
        """
        cov_map = torch.tril_indices(row=self.num_species, col=self.num_species, offset=0)
        return(cov_map)      

    def set_diffusion(self, diffusion):
        """
        set diffusion from input
        """
        if hasattr(diffusion, 'shape'):
            sqrt_diff = diffusion
        else:
            sqrt_diff = diffusion*torch.eye(self.num_species)
        return(sqrt_diff)

    def get_diffusion(self, rates):
        """
        Extract diffusion matrix from the parameters
        Basic version returns stored diffusion, overwrite if diffusion tensor is to be learned
        """
        return(self.sqrt_diff @ self.sqrt_diff.T)

    def get_drift_stats(self, m, M, rates):
        """
        Compute required expectations of the drift
        Output:
            E[a(X_t)]: expected drift
            E[a(X_t)X_t^T]: drift with state cross moment
            E[a(X_t)a(X_t)^T]: drift square moment
        """
        raise NotImplementedError


class GPMC(GpApproximationAutograd):
    """
    GP approximation with sample based evaluation of drift statistics
    """

    def __init__(self, initial, rates, tspan, diffusion, gradient_mode='regular', num_samples=100, adjust_samples=False):
        super().__init__(initial, rates, tspan, diffusion, gradient_mode=gradient_mode)
        self.num_samples = num_samples
        self.samples = self.set_samples(adjust_samples)

    def set_samples(self, adjust_samples):
        samples = torch.randn((self.num_samples, self.num_species))
        if adjust_samples:
            # compute mean and cov
            mean = samples.mean(axis=0)            
            tmp = samples-mean.unsqueeze(0)
            cov = torch.mean(tmp.unsqueeze(2) @ tmp.unsqueeze(1), axis=0)
            # standardize mean and cov
            R = torch.cholesky(cov)
            samples = torch.solve(tmp.unsqueeze(-1), R.unsqueeze(0))[0].squeeze()
        return(samples)

    def get_drift_stats(self, m, M, rates):
        """
        Compute required expectations of the drift
        Output:
            E[a(X_t)]: expected drift
            E[a(X_t)X_t^T]: drift with state cross moment
            E[a(X_t)a(X_t)^T]: drift square moment
        """
        # preparations
        try:
            R = torch.cholesky(M)
        except RuntimeError:
            R = torch.cholesky(M+1e-5*torch.eye(self.num_species))
        samples = m + self.samples @ R.T
        # compute drift
        drift = self.drift(samples, rates)
        a = torch.mean(drift, axis=0)
        a_x = torch.mean(drift.unsqueeze(2) @ samples.unsqueeze(1), axis=0)
        a_a = torch.mean(drift.unsqueeze(2) @ drift.unsqueeze(1), axis=0)
        return(a, a_x, a_a)

    def drift(self, state, rates):
        """
        SDE drift function. Should be able to handle vectorized input
        """
        raise NotImplementedError

    # def kl_contrib_torch(self, time, state, control, rates):
    #     """
    #     contribution of moments and controls to the prior kl
    #     """
    #     # preparations
    #     m, M = self.get_moments(state)
    #     R = torch.cholesky(M+1e-3*torch.eye(self.num_species))
    #     samples = m + self.samples @ R.T
    #     diff = self.get_diffusion(rates)
    #     diff_inv = torch.inverse(diff)
    #     u0, u1 = self.get_controls(control)
    #     # compute drift
    #     drift = self.drift(samples, rates)
    #     # compute kl contribution
    #     tmp = u0.unsqueeze(0) + (u1 @ samples.T).T - drift
    #     kl_contrib = torch.mean(tmp.unsqueeze(1) @ (diff_inv.unsqueeze(0) @ tmp.unsqueeze(2)), axis=0)
    #     return(0.5*kl_contrib.sum())


class GPUC(GpApproximationAutograd):
    """
    GP approximation with sample based evaluation of drift statistics
    """

    def __init__(self, initial, rates, tspan, diffusion, gradient_mode='regular', alpha=1e-2, kappa=0.0, beta=2.0):
        super().__init__(initial, rates, tspan, diffusion, gradient_mode=gradient_mode)
        self.alpha = alpha
        self.kappa = kappa
        self.beta = beta
        self.reg = 1e-5
        self.lam = alpha**2*(self.num_species+kappa) - self.num_species
        self.weights = self.set_weights()
        self.samples = self.set_samples()

    def set_samples(self):
        """
        Generate the sigma points
        """
        tmp = torch.eye(self.num_species)
        samples = torch.zeros((2*self.num_species+1, self.num_species))
        for i in range(self.num_species):
            samples[i+1] = torch.sqrt(torch.tensor(self.num_species+self.lam))*tmp[i]
            samples[i+1+self.num_species] = -torch.sqrt(torch.tensor(self.num_species+self.lam))*tmp[i]
        return(samples)

    def set_weights(self):
        """
        Compute the weights
        """
        # mean weights
        weights = torch.zeros((2, 2*self.num_species+1))
        weights[0, 0] = self.lam/(self.num_species+self.lam)
        weights[0, 1:] = 0.5/(self.num_species+self.lam)
        # covariance weights
        weights[1, 0] = self.lam/(self.num_species+self.lam) + (1-self.alpha**2+self.beta)
        weights[1, 1:] = 0.5/(self.num_species+self.lam)
        return(weights)

    # def get_drift_stats(self, m, M, rates):
    #     """
    #     Compute required expectations of the drift
    #     Output:
    #         E[a(X_t)]: expected drift
    #         E[a(X_t)X_t^T]: drift with state cross moment
    #         E[a(X_t)a(X_t)^T]: drift square moment
    #     """
    #     # preparations
    #     R = torch.cholesky(M+self.reg*torch.eye(self.num_species))
    #     samples = m + self.samples @ R.T
    #     # compute drift
    #     drift = self.drift(samples, rates)
    #     a = torch.sum(drift * self.weigts[0].unsqueeze(0), axis=0)
    #     a_x = torch.sum((drift.unsqueeze(2) @ samples.unsqueeze(1)) * self.weights, axis=0)
    #     a_a = torch.mean(drift.unsqueeze(2) @ drift.unsqueeze(1), axis=0)
    #     return(a, a_x, a_a)

    def kl_contrib_torch(self, time, state, control, rates):
        """
        contribution of moments and controls to the prior kl
        """
        # preparations
        m, M = self.get_moments(state)
        diff = self.get_diffusion(rates)
        diff_inv = torch.inverse(diff)
        u0, u1 = self.get_controls(control)
        # create marginal samples from variational process
        try:
            R = torch.cholesky(M)
        except RuntimeError:
            R = torch.cholesky(M+self.reg*torch.eye(self.num_species))
        gp_samples = m + self.samples @ R.T
        # compute transformed samples
        drift = self.drift(gp_samples, rates)
        obj_samples = u0.unsqueeze(0) + gp_samples @ u1.T - drift
        # compute kl contribution
        mean = torch.sum(obj_samples * self.weights[0].unsqueeze(1), axis=0)
        tmp = obj_samples - mean.unsqueeze(0)
        cov = torch.sum(tmp.unsqueeze(2) @ tmp.unsqueeze(1) * self.weights[1].unsqueeze(-1).unsqueeze(-1), axis=0)
        kl_contrib = 0.5*((cov + torch.ger(mean, mean)) * diff_inv).sum()
        return(kl_contrib)

    def drift(self, state, rates):
        """
        SDE drift function. Should be able to handle vectorized input
        """
        raise NotImplementedError