import numpy as np
import torch

from pymbvi.models.model import Model
from pymbvi.models.diffusion.autograd_model import AutogradModel
import pymbvi.util as ut
from scipy.linalg import solve
#from pymbvi.uitl import 


class IndependentControl(AutogradModel):
    """
    General class for diffusion with a (possibly rescaled) control
    control is of the form c(x,t)*u(t)
    stat_tensor correspons to E[ c(x,t)^T D^{-1} (x, t) c(x, t)] expressed in terms of the moment functions
    """

    def kl_prior(self, time, control, forward, rates):
        """
        For the independent control, the kl divergence only depends on the control
       
        """
        # get diffusion tensor
        num_species = int(-1.5+np.sqrt(2*self.num_states()+2.25))
        dim = forward.shape[:2]+(num_species, num_species)
        stat_tensor = self.stat_tensor(time, forward, rates)
        #  compute kl fun
        kl = 0.5*(np.expand_dims(control, axis=-2)@stat_tensor@np.expand_dims(control, axis=-1)).squeeze()
        return(kl)

    # def rcontrol_gradient(self, time, control, forward, backward, rates):
    #     # get diffusion tensor
    #     num_species = int(-0.5+np.sqrt(self.num_states()+0.25))
    #     dim = forward.shape[:2]+(num_species, num_species)
    #     stat_tensor = self.stat_tensor(time, forward, rates)
    #     stat_tensor = ut.integrate_subsamples(time, stat_tensor).reshape(dim)
    #     # contribution from the constraint
    #     dim_forward = forward.shape[0:3]+(-1,)
    #     constraint_grad = self.control_gradient(forward.reshape(-1, forward.shape[-1]), backward.reshape(-1, backward.shape[-1]))
    #     constraint_grad = ut.integrate_subsamples(time, constraint_grad, dim_forward)
    #     # contribution from the prior kl divergence
    #     grad = stat_tensor@control - constraint_grad
    #     return(grad)

    def control_gradient(self, time, control, forward, backward, rates):
        # get diffusion tensor
        num_species = int(-1.5+np.sqrt(2*self.num_states()+2.25))
        dim = forward.shape[:2]+(num_species, num_species)
        stat_tensor = self.stat_tensor(time, forward, rates)
        # contribution from the constraint
        dim_forward = forward.shape[0:3]+(-1,)
        constraint_grad = self.constraint_gradient(time, control, forward, backward, rates)
        constraint_grad = self.fisher_transform(stat_tensor, constraint_grad)
        # contribution from the prior kl divergence
        grad = control - constraint_grad
        return(grad)

    # def fisher_transform(self, diff, constraint_grad):
    #     # set up output
    #     dim = constraint_grad.shape
    #     transformed_grad = np.zeros(dim)
    #     # iterate over time
    #     for i in range(dim[0]):
    #         for j in range(dim[1]):
    #             G = diff[i, j]
    #             tmp = constraint_grad[i, j]
    #             transformed_grad[i, j] = solve(G, tmp, sym_pos=True)
    #     return(transformed_grad)

    def num_controls(self):
        return(self.num_species)

    def set_cov_map(self):
        """
        Construct a tensor that maps the reduced moments to a full covariance matrix
        """
        cov_map = torch.tril_indices(row=self.num_species, col=self.num_species, offset=0)
        return(cov_map)

    def get_moments(self, state):
        """
        Convert state to mean and covariance matrix
        """
        # extract first moment
        m = state[:self.num_species]
        # construct second moment
        tmp = torch.zeros((self.num_species, self.num_species))
        tmp[self.cov_map[0], self.cov_map[1]] = state[self.num_species:]
        M = tmp+tmp.T-torch.diag(tmp.diag())
        # return mean and covariance
        return(m, M)
    

class DiffIndependentControl(AutogradModel):
    """
    Diffusion model class for the state independent control 
    This assumes a rescaling with the noise term i.e. 
    u(x,t) = b(x,t)*u(t) and
    stat_tensor is of the form eye(n)
    """

    def kl_prior(self, time, control, forward, rates):
        """
        For the independent control, the kl divergence only depends on the control
        """
        delta = time[:, :, [-1]]-time[:, :, [0]]
        kl = 0.5*control**2*delta
        return(kl)

    def control_gradient(self, time, control, forward, backward, rates):
        """
        Gradient of the control
        Note that we have implemented a natural gradient (Fisher matrix is diagional with delta t)
        """
        # get relevant dimensions
        dim = forward.shape[0:3]+(-1,)
        # contribution from the constraint
        constraint_grad = self.constraint_gradient(time, control, forward, backward, rates)
        # contribution from the prior kl divergence
        kl_grad = control.copy()
        # construct final gradien
        delta = time[:, :, [-1]]-time[:, :, [0]]
        grad = kl_grad-constraint_grad/delta
        return(grad)

    def stat_tensor_torch(self, forward, rates):
        tmp = forward.sum()
        diff = torch.tensor([1.0])+(tmp-tmp)
        return(diff.unsqueeze(0))

    def num_controls(self):
        num_species = int(-1.5+np.sqrt(2*self.num_states()+2.25))
        return(num_species)
    
    