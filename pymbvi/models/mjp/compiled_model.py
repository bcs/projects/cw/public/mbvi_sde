# A template class where the forward, backward, natural_moments and constraint_grad functions are provided in form of compiled functions
# specific implementations are realized by constructing the required subclasses

import numpy as np
import torch
from pymbvi.models.model import Model
import pymbvi.util as ut

# global options
torch.set_default_dtype(torch.float64)


class CompiledModel(Model):
    """
    Base class for models implemented using torch autograd
    The derived models require a custom implmentation of the set_... functions
    """

    # constructor and setup

    def __init__(self, initial, rates, tspan):
        """
        Constructor method
        """
        # store inputs
        self.initial = initial
        self.rates = rates
        self.tspan = tspan
        # derived quantities
        self.num_moments = initial
        self.num_reactions = rates

    # interface functions required by abstract Model class

    def num_states(self):
        return(len(self.initial))

    def num_controls(self):
        return(len(self.rates))

    def num_param(self):
        return(len(self.rates))

    def get_initial(self):
        return(self.initial)

    # wrapper functions to obtain vectorized versions of the compiled functions

    def constraint_gradient(self, control, forward, backward, rates):
        """
        Evaluates the gradient of the constraint with respect to the controls
        """
        # preparations
        dim = forward.shape
        grad = np.zeros(dim[:3]+control.shape[-1:])
        # contributions lagrange multpliers
        for i in range(dim[0]):
            for j in range(dim[1]):
                for k in range(dim[2]):
                    grad[i, j, k] = self.constraint_gradient_sub(control[i, j], forward[i, j , k], backward[i, j, k], rates)
        return(grad)

    # def natural_moments(self, forward, rates):  
    #     """
    #     Compute natural moments for all forward vectors
    #     """
    #     # preparations
    #     dim = forward.shape
    #     prop = np.zeros(dim[:3]+(self.num_controls()))
    #     for i in range(dim[0]):
    #         for j in range(dim[1]):
    #             for k in range(dim[2]):
    #                 prop[i, j, k] = self.natural_moments_sub(forward[i, j, k], rates)
    #     return(prop)

    def natural_moments(self, forward, rates):
        """
        Compute natural moments for all forward vectors
        """
        # preparations
        dim = forward.shape
        prop = np.zeros((dim[0], self.num_controls()))
        for i in range(dim[0]):
            prop[i] = self.natural_moments_sub(forward[i], rates)
        return(prop)    


    def constraint_rates_gradient(self, control, forward, backward, rates):
        """
        Evaluates the gradient of the constraint with respect to the controls
        """
        dim = forward.shape
        grad = np.zeros(dim[:3]+control.shape[-1:])
        # contributions lagrange multpliers
        for i in range(dim[0]):
            for j in range(dim[1]):
                for k in range(dim[2]):
                    grad[i, j, k] = self.constraint_rates_gradient_sub(control[i, j], forward[i, j , k], backward[i, j, k], rates)
        return(grad)
    
    def constraint_full_gradient(self, control, forward, backward, rates):
        """
        Evaluates the gradient of the constraint with respect to the controls and rates
        """
        grad_control = self.constraint_gradient(control, forward, backward, rates)
        grad_rates = self.constraint_rates_gradient(control, forward, backward, rates)
        return(grad_control, grad_rates)

    # functions required by sub class

    def forward(self, time, forward, control, rates):
        raise(NotImplementedError)
    
    def backward(self, time, backward, control, forward_time, forward, rates):
        raise(NotImplementedError)

    def constraint_gradient_sub(self, control, forward, backward, rates):
        raise(NotImplementedError)

    def constraint_rates_gradient_sub(self, control, forward, backward, rates):
        raise(NotImplementedError)

    def natural_moments_sub(self, forward, rates):
        raise(NotImplementedError)
