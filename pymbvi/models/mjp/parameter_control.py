import numpy as np
import torch
from pymbvi.models.model import Model
from pymbvi import util as ut
from scipy.linalg import solve
from scipy.integrate import solve_ivp
from scipy.interpolate import interp1d

#from pymbvi.uitl import 
from pymbvi.models.mjp.autograd_model import AutogradModel


class ParameterControl(AutogradModel):
    """
    Class for MJPs where each transition group corresponds to one parameter. 
    In addition, parameters are replaced by controls instead of multiplied. Thus, the variational model is independent of the prior parameters. 
    """

    # def kl_prior(self, time, control, forward, rates):
    #     # get propensities
    #     dim = forward.shape[:3]+control.shape[-1:]
    #     propensities = self.natural_moments(forward.reshape((-1, forward.shape[-1])), rates)
    #     propensities = ut.integrate_subsamples(time, propensities, dim)
    #     # compute kl div
    #     eff_control = np.exp(rates)-np.exp(control)+np.exp(control)*(control-rates)
    #     kl = propensities * eff_control
    #     return(kl)

    def effective_control(self, control, rates):
        """
        Effective control required for kl prior and backward function
        """
        # transform control
        eff_control = torch.exp(rates)+torch.exp(control)*(control-rates-1.0)
        return(eff_control)

    # def rcontrol_gradient(self, time, control, forward, backward, rates):
    #     # get propensities
    #     dim = forward.shape[:3]+control.shape[-1:]
    #     propensities = self.natural_moments(forward.reshape((-1, forward.shape[-1])), rates)
    #     propensities = ut.integrate_subsamples(time, propensities, dim)
    #     # contribution from the constraint
    #     dim_forward = forward.shape[0:3]+(-1,)
    #     constraint_grad = self.constraint_gradient(control, forward, backward, rates)
    #     constraint_grad = ut.integrate_subsamples(time, constraint_grad)
    #     # contribution from prior kl
    #     control_grad = propensities*(control-rates)*np.exp(control)-constraint_grad
    #     return(control_grad)

    def control_gradient(self, time, control, forward, backward, rates):
        # get propensities
        propensities = self.natural_moments(time, forward, rates)
        # contribution from the constraint
        constraint_grad = self.constraint_gradient(time, control, forward, backward, rates)
        # contribution from the prior kl divergence
        control_grad = (control-rates)-constraint_grad/(propensities*np.exp(control))
        # contribution from the prior kl divergence
        if self.gradient_mode == 'natural':
            control_grad = (control-rates)-constraint_grad/(propensities*np.exp(control))
        elif self.gradient_mode == 'regular':
            control_grad = (control-rates)*propensities*np.exp(control)-constraint_grad
        return(control_grad)
    
    def rates_gradient(self, time, control, forward, backward, rates):
        """
        Joint natural gradient with respect to controls and rates
        This is a simplified version that only uses the diagonal elements of the fisher information tensor
        """
        # get propensities
        propensities = self.natural_moments(time, forward, rates)
        eff_prop = propensities * (np.exp(rates)-np.exp(control))
        eff_prop = eff_prop.reshape((-1, len(rates))).sum(axis=0)
        # fisher information
        if self.gradient_mode == 'joint_natural':
            fisher_g = self.compute_fisher_g_rates(time, forward, rates)
            rates_grad = eff_prop/fisher_g
        else:
            rates_grad = eff_prop
        return(rates_grad)

    def rates_gradient_old(self, time, control, forward, backward, rates):
        """
        Joint natural gradient with respect to controls and rates
        This is a simplified version that only uses the diagonal elements of the fisher information tensor
        """
        # get propensities
        propensities = self.natural_moments(forward.reshape((-1, forward.shape[-1])), rates)
        eff_prop = propensities.reshape(forward.shape[:-1]+(-1,))*np.expand_dims(np.exp(rates)-np.exp(control), axis=2)
        eff_prop = ut.integrate(time, eff_prop)
        # fisher information
        fisher_g = self.compute_fisher_g_rates(time, forward, rates)
        # natural gradient
        rates_grad = eff_prop/fisher_g
        return(rates_grad)

    def joint_gradient(self, time, control, forward, backward, rates, options):
        # compute control gradient
        control_grad = self.control_gradient(time, control, forward, backward, rates)
        # compute rates gradient 
        rates_grad = self.rates_gradient(time, control, forward, backward, rates)
        return(control_grad, rates_grad)

    # def compute_fisher_g_control(self, time, control, forward, backward, rates):
    #     """ 
    #     compute fisher g for the control
    #     """
    #     # get propensities
    #     dim = forward.shape[:3]+control.shape[-1:]
    #     propensities = self.natural_moments(forward.reshape((-1, forward.shape[-1])), rates)
    #     propensities = ut.integrate_subsamples(time, propensities, dim)
    #     return(propensities*np.exp(control))

    def compute_fisher_g_rates(self, time, forward, rates):
        """
        fisher g for rates 
        """
        # solve forward equation with current rates
        dim = forward.shape
        initial = forward[0, 0, 0]
        def odefun(time, state):
            return(self.forward(time, state, rates, rates))
        tspan = np.array([time[0, 0, 0], time[-1, -1, -1]])
        sol = solve_ivp(odefun, tspan, initial, t_eval=np.unique(time))
        forward_tmp = interp1d(sol['t'], sol['y'].T, axis=0)(time.flatten()).reshape(dim)
        # compute natural moments
        propensities = self.natural_moments(time, forward_tmp, rates)
        eff_prop = propensities.reshape((-1, len(rates))).sum()
        # compute fisher g
        fisher_g = eff_prop*np.exp(rates)
        return(fisher_g)

    # def fisher_transform(self, control_grad, rates_grad, control_g, rates_g, k=1e-6):

    #     fisher_g = self.compute_fisher_g(control_g, rates_g)
    #     # transform grad
    #     grad = np.concatenate([control_grad.flatten(), rates_grad])
    #     # apply inverse fisher information
    #     natural_grad = solve(fisher_g.T@fisher_g+k*np.eye(fisher_g.shape[0]), fisher_g.T@grad, sym_pos=True)
    #     #natural_grad = ut.lstsq_reg(fisher_g, grad, k=1e-6)
    #     #natural_grad = np.linalg.lstsq(fisher_g, grad)[0]
    #     # split again and return
    #     control_grad = natural_grad[0:control_grad.size].reshape(control_grad.shape)
    #     rates_grad = natural_grad[control_grad.size:]
    #     return(control_grad, rates_grad)

    def num_controls(self):
        return(len(self.rates))