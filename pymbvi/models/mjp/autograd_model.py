# This files contains a class of variational models that use the torch autograd framework to compute required derivatives

import numpy as np
import torch

from pymbvi.models.model import Model
import  pymbvi.models.autograd_model as am
import pymbvi.util as ut

# global options
torch.set_default_dtype(torch.float64)


class AutogradModel(am.AutogradModel):
    """
    Base class for models implemented using torch autograd
    The derived models require a custom implmentation of the set_... functions
    """

    # def fisher_loc(self, time, control, forward, rates):
    #     """
    #     Required for natural gradient mode
    #     """
    #     return(self.stat_tensor_torch(forward, rates))

    def kl_contrib_torch(self, time, state, control, rates):
        """
        compute kl contribution in terms of stat tensor
        """
        natural_moments = self.natural_moments_torch(state, rates)
        eff_control = self.effective_control(control, rates)
        kl_contrib = natural_moments @ eff_control
        return(kl_contrib)

    def constraint_gradient(self, time, control, forward, backward, rates):
        """
        gradient contribution of dynamic constraint only
        """
        # preparations
        dim = forward.shape
        control_grad = np.zeros(dim[0:3]+control.shape[-1:])
        rates_torch = torch.from_numpy(rates)
        # iterate over stored values
        with torch.set_grad_enabled(True):
            for i in range(dim[0]):
                for j in range(dim[1]):
                    control_torch = torch.tensor(control[i, j], requires_grad=True)
                    for k in range(dim[2]):
                        # forward contribution
                        forward_torch = torch.from_numpy(forward[i, j, k])
                        backward_torch = torch.from_numpy(backward[i, j, k])
                        tmp = self.forward_torch(time, forward_torch, control_torch, rates_torch)
                        tmp.backward(backward_torch)
                        control_grad[i, j, k] = np.array(control_torch.grad)
                        control_torch.grad.zero_()
        # integrate over subsamples
        control_grad = ut.integrate_subsamples(time, control_grad)
        return(control_grad)

    def constraint_joint_gradient(self, time, control, forward, backward, rates):
       # preparations
        dim = forward.shape
        control_grad = np.zeros(dim[0:3]+control.shape[-1:])
        rates_torch = torch.tensor(rates, requires_grad=True)
        rates_grad = np.zeros(dim[0:3]+rates.shape)
        # iterate over stored values
        with torch.set_grad_enabled(True):
            for i in range(dim[0]):
                for j in range(dim[1]):
                    control_torch = torch.tensor(control[i, j], requires_grad=True)
                    for k in range(dim[2]):
                        # forward contribution
                        forward_torch = torch.from_numpy(forward[i, j, k])
                        backward_torch = torch.from_numpy(backward[i, j, k])
                        tmp = self.forward_torch(time, forward_torch, control_torch, rates_torch)
                        tmp.backward(backward_torch)
                        control_grad[i, j, k] = np.array(control_torch.grad)
                        rates_grad[i, j, k] = np.array(rates_torch.grad)
                        control_torch.grad.zero_()
                        rates_torch.grad.zero_()
        # integrate over subsamples
        control_grad = ut.integrate_subsamples(time, control_grad)
        rates_grad = ut.integrate(time, rates_grad)
        return(control_grad, rates_grad)

    def natural_moments(self, time, forward, rates):
        """
        Map forward moment function to the natural moments
        Forward is expected to be of shape (num_steps, control_dim)
        """
        dim = forward.shape[:3] + (self.num_controls(),)
        forward_torch = torch.from_numpy(forward)
        rates_torch = torch.from_numpy(rates)
        natural_moments = np.zeros(dim)
        for i in range(dim[0]):
            for j in range(dim[1]):
                for k in range(dim[2]):
                    natural_moments[i, j, k] = self.natural_moments_torch(forward_torch[i, j, k], rates_torch).numpy()
        natural_moments = ut.integrate_subsamples(time, natural_moments)
        return(natural_moments)

    def natural_moments_torch(self, forward, rates):
        """
        Expected sumary statistics
        """
        raise NotImplementedError

    def effective_control(self, control, rates):
        """
        Convert controls to the form required for the kl contribution
        """
        raise NotImplementedError


class AutogradModelOld(Model):
    """
    Base class for models implemented using torch autograd
    The derived models require a custom implmentation of the set_... functions
    """

    # constructor and setup

    def __init__(self, initial, rates, tspan):
        """
        Constructor method
        """
        # store inputs
        self.initial = AutogradModel.init_tensor(initial)
        self.rates = AutogradModel.init_tensor(rates)
        self.tspan = AutogradModel.init_tensor(tspan)
        # derived quantities
        self.num_moments = len(initial)
        self.num_reactions = len(rates)

    @staticmethod
    def init_tensor(array):
        """
        initialize depending on input
        """
        if type(array) is np.ndarray:
            new_array = torch.tensor(array)
        elif type(array) is list:
            new_array = torch.tensor(array)
        elif type(array) is torch.Tensor:
            new_array = array.clone().detach()
        else:
            raise ValueError('Invalid input type for input {}'.format(array))
        return(new_array)

    # interface functions required by abstract Model class

    def num_states(self):
        return(len(self.initial.numpy()))

    def num_controls(self):
        return(len(self.rates.numpy()))

    def num_param(self):
        return(len(self.rates.numpy()))       

    def forward(self, time, state, control, rates):
        """ 
        Compute r.h.s of the forward differential equations 
        """
        # evaluate the derivative
        dydt = self.forward_torch(time, torch.from_numpy(state), torch.from_numpy(control), torch.from_numpy(rates))
        return(dydt)

    def backward(self, time, state, control, forward_time, forward, rates):
        # perform interpolation
        exp_control = np.exp(control)
        forward = ut.interp_pwc(time, forward_time, forward)
        # compute control part
        control_eff = self.effective_control(control, rates)
        # convert to torch stuff
        forward_torch = torch.from_numpy(forward)
        forward_torch.requires_grad = True
        control_torch = torch.from_numpy(control)
        rates_torch = torch.from_numpy(rates)
        # compoute contribution of the natural moments
        tmp = self.natural_moments_torch(forward_torch, rates_torch)
        tmp.backward(torch.from_numpy(control_eff))
        dydt = np.array(forward_torch.grad)
        forward_torch.grad.zero_()
        # add contribution from the forward equation
        tmp = self.forward_torch(time, forward_torch, control_torch, rates_torch)
        tmp.backward(torch.from_numpy(state))
        dydt -= np.array(forward_torch.grad)
        return(dydt)

    def constraint_gradient(self, control, forward, backward, rates):
        """
        Evaluates the gradient of the constraint with respect to the controls
        """
        # preparations
        dim = forward.shape
        grad = np.zeros(dim[:3]+control.shape[-1:])
        rates_torch = torch.from_numpy(rates)
        # contributions lagrange multpliers
        for i in range(dim[0]):
            for j in range(dim[1]):
                control_torch = torch.tensor(control[i, j], requires_grad=True)
                for k in range(dim[2]):
                    forward_torch = torch.from_numpy(forward[i, j, k])
                    backward_torch = torch.from_numpy(backward[i, j, k])
                    tmp = self.forward_torch(i, forward_torch, control_torch, rates_torch)
                    tmp.backward(backward_torch)
                    grad[i, j, k] = np.array(control_torch.grad)
                    control_torch.grad.zero_()
        return(grad)

    def constraint_rates_gradient(self, control, forward, backward, rates):
        """
        Evaluates the gradient of the constraint with respect to the controls
        """
        # preparations
        dim = forward.shape
        grad = np.zeros(dim[:3]+control.shape[-1:])
        rates_torch = torch.tensor(rates, requires_grad=True)
        # contributions lagrange multpliers
        for i in range(dim[0]):
            for j in range(dim[1]):
                control_torch = torch.tensor(control[i, j])
                for k in range(dim[2]):
                    forward_torch = torch.from_numpy(forward[i, j, k])
                    backward_torch = torch.from_numpy(backward[i, j, k])
                    tmp = self.forward_torch(i, forward_torch, control_torch, rates_torch)
                    tmp.backward(backward_torch)
                    grad[i, j, k] = np.array(rates_torch.grad)
                    rates_torch.grad.zero_()
        return(grad)

    def constraint_full_gradient(self, control, forward, backward, rates):
        """
        Evaluates the gradient of the constraint with respect to the controls
        """
        # preparations
        dim = forward.shape
        grad_control = np.zeros(dim[:3]+control.shape[-1:])
        grad_rates = np.zeros(dim[:3]+control.shape[-1:])
        rates_torch = torch.tensor(rates, requires_grad=True)
        # contributions lagrange multpliers
        for i in range(dim[0]):
            for j in range(dim[1]):
                control_torch = torch.tensor(control[i, j], requires_grad=True)
                for k in range(dim[2]):
                    forward_torch = torch.from_numpy(forward[i, j, k])
                    backward_torch = torch.from_numpy(backward[i, j, k])
                    tmp = self.forward_torch(i, forward_torch, control_torch, rates_torch)
                    tmp.backward(backward_torch)
                    grad_control[i, j, k] = np.array(control_torch.grad)
                    grad_rates[i, j, k] = np.array(rates_torch.grad)
                    rates_torch.grad.zero_()
                    control_torch.grad.zero_()
        return(grad_control, grad_rates)

    def natural_moments(self, forward, rates):
        """
        Map forward moment function to the natural moments
        Forward is expected to be of shape (num_steps, control_dim)
        """
        prop = self.natural_moments_torch(torch.from_numpy(forward).T, torch.from_numpy(rates))
        return(prop.numpy())

    def get_initial(self):
        return(self.initial.numpy())

    # helper functions to be implmented by derived classes

    def forward_torch(self, time, state, control, rates):
        """
        Identical to forward but expecting torch tensor input
        """
        raise NotImplementedError

    def natural_moments_torch(self, forward, rates):
        """
        Same as natural)moments but in torch version
        """
        raise NotImplementedError


class LinearAutogradModel(AutogradModel):
    """
    Linear model sublcass of the autograd model
    """

    def __init__(self, initial, rates, tspan):
        """
        Constructor method
        """
        # base class constructor
        super().__init__(initial, rates, tspan)
        # stuff for ode functions
        self.forward_mat = self.set_forward_mat()
        self.forward_vec = self.set_forward_vec()
        self.natural_moment_mat = self.set_natural_moment_mat()
        self.natural_moment_vec = self.set_natural_moment_vec()

    def forward_torch(self, time, state, control):
        """
        Identical to forward but expecting torch tensor input
        """
        # evaluate the derivative
        dydt = torch.matmul(torch.matmul(self.forward_mat, control), state) + torch.matmul(self.forward_vec, control)
        return(dydt)

    def forward_batch(self, time, state, control):
        """
        Takes a hole batch of state and control and propagates it forward in time
        """
        # transform the control to forward matrix
        forward_mat = torch.matmul(self.forward_mat, control.T)
        # apply to state
        dydt = torch.matmul(forward_mat.permute(2, 0, 1), state.unsqueeze(-1)).squeeze()
        # add state independent contribution
        dydt += torch.matmul(control, self.forward_vec.T)
        return(dydt)

    def natural_moments_torch(self, forward):
        """
        Same as natural)moments but in torch version
        """
        prop = torch.matmul(self.natural_moment_mat, forward).T+self.natural_moment_vec
        return(prop)

    # helper functions to set the required matrices, to be implemented by derived models

    def set_forward_mat(self):
        """
        matrix that transforms controls to state
        """
        raise NotImplementedError

    def set_forward_vec(self):
        """
        ofset depending on control
        """
        raise NotImplementedError

    def set_natural_moment_mat(self):
        raise NotImplementedError

    def set_natural_moment_vec(self):
        raise NotImplementedError




# class BernoulliTasep(AutogradModel):
#     """
#     Implementaiton of tasep model with Bernoulli closure and rate parametrization
#     """

#     def __init__(self, initial, rates, tspan):
#         """
#         Constructor method
#         """
#         # base class constructor
#         super().__init__(initial, rates, tspan)
#         # stuff for ode functions
#         self.moment_selection = self.set_moment_selection()

#     def forward_torch(self, time, state, control):
#         """
#         Identical to forward but expecting torch tensor input
#         """
#         dydt = torch.zeros(self.num_moments)
#         dydt[0] = control[0]*(1.0-state[0]) - control[1]*state[0]*(1-state[1])
#         dydt[1:-1] = control[1]*(state[0:-2]*(1-state[1:-1]) - state[1:-1]*(1-state[2:]))
#         dydt[-1] = control[1]*state[-2]*(1-state[-1]) - control[2]*state[-1]
#         return(dydt)

#     def natural_moments_torch(self, forward):
#         """
#         Same as natural)moments but in torch version
#         """
#         # preparations
#         if forward.dim() == 2:
#             pad_one = torch.ones(1, forward.shape[1])
#             pad_zero = torch.zeros(1, forward.shape[1])
#         else:
#             pad_one = torch.ones(1)
#             pad_zero = torch.zeros(1)
#         tmp1 = torch.cat([pad_one, forward])
#         tmp2 = 1.0-torch.cat([forward, pad_zero])
#         # compute raw phi
#         phi = tmp1*tmp2
#         # sumarize reactions with same parameter
#         phi_reduced = torch.matmul(phi.T, self.moment_selection)
#         return(phi_reduced)

#     def set_moment_selection(self):
#         """
#         Construct a matrix for producing phi reduces
#         """
#         moment_selection = torch.zeros(self.num_moments+1, 3)
#         moment_selection[0, 0] = 1.0
#         moment_selection[-1, -1] = 1.0
#         moment_selection[1:-1, 1] = 1.0
#         return(moment_selection)


class BernoulliTasep2(AutogradModel):
    """
    Implementaiton of tasep model with Bernoulli closure and rate parametrization
    allows one control for each reaction and is thus more flexible
    """

    def forward_torch(self, time, state, control):
        """
        Identical to forward but expecting torch tensor input
        """
        dydt = torch.zeros(self.num_moments)
        dydt[0] = control[0]*(1.0-state[0]) - control[1]*state[0]*(1-state[1])
        dydt[1:-1] = control[1:-2]*state[0:-2]*(1-state[1:-1]) - control[2:-1]*state[1:-1]*(1-state[2:])
        dydt[-1] = control[-2]*state[-2]*(1-state[-1]) - control[-1]*state[-1]
        return(dydt)

    def natural_moments_torch(self, forward):
        """
        Same as natural)moments but in torch version
        """
        # preparations
        if forward.dim() == 2:
            pad_one = torch.ones(1, forward.shape[1])
            pad_zero = torch.zeros(1, forward.shape[1])
        else:
            pad_one = torch.ones(1)
            pad_zero = torch.zeros(1)
        tmp1 = torch.cat([pad_one, forward])
        tmp2 = 1.0-torch.cat([forward, pad_zero])
        # compute raw phi
        phi = tmp1*tmp2
        return(phi)

