"""
Transition model class that can be used for exact forward backward integration within the ForwardBackward class
"""
import numpy as np


class DenseCTMC:

    def __init__(self, rate_matrix):
        self.dim = rate_matrix.shape[0]
        self.states_vec = np.array([i for i in range(self.dim)])
        self.rate_matrix = rate_matrix
        self.exit_rate = np.sum(rate_matrix, axis=0)

    def forward(self, time, state, rates):
        dydt = self.rate_matrix @ state - self.exit_rate * state
        return(dydt)

    def backward(self, time, state, rates):
        dydt = self.rate_matrix.T @ state - self.exit_rate * state
        return(-dydt)

    def num_states(self):
        return(self.dim)

    def get_states(self):
        return(self.states_vec)

    @property
    def rates(self):
        return(self.rate_matrix.flatten())


class SparseCTMC:

    def __init__(self, generator, states_dict=None):
        self.dim = generator.shape[0]
        self.generator = generator
        self.states_vec = self.set_states_vec(states_dict)

    def forward(self, time, state, rates):
        dydt = self.generator @ state
        return(dydt)

    def backward(self, time, state, rates):
        dydt = self.generator.T @ state 
        return(-dydt)

    def num_states(self):
        return(self.dim)

    def get_states(self):
        return(self.states_vec)

    @property
    def rates(self):
        return(self.generator.data.copy())

    def set_states_vec(self, state_dict):
        if state_dict is None:
            states_vec = np.array([i for i in range(self.dim)])
        else:
            state_dim = state_dict[0].size
            states_vec = np.zeros((self.dim, state_dim))
            for i in range(self.dim):
                states_vec[i] = state_dict[i]
        return(states_vec)