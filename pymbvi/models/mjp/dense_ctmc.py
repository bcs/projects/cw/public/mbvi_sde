from pymbvi.models.model import Model 
from pymbvi.models.mjp.autograd_model import AutogradModel
import pymbvi.util as ut
import numpy as np
import torch

#class DenseCTMCAutograd(AutogradModel):

class DenseCTMC(Model):
    """
    Class for exact variational smoothing in CTMCs
    """

    def __init__(self, rate_matrix, initial):
        self.rate_matrix = rate_matrix
        self.dim = rate_matrix.shape[0]
        self.initial = initial

    def forward(self, time, state, control, rates):
        # compute controlled matrix
        control = np.exp(np.expand_dims(control, 1) - np.expand_dims(control, 0))
        rate_matrix = self.rate_matrix * control
        exit_rate = np.sum(rate_matrix, axis=0)
        # compute derivative
        dydt = rate_matrix @ state - exit_rate * state
        return(dydt)

    def constraint_gradient(self, time, state, control, rates, backward):
        # compute controlled matrix
        control = np.exp(np.expand_dims(control, 1) - np.expand_dims(control, 0))
        rate_matrix = self.rate_matrix * control
        # compute derivative
        # dcontrol = (rate_matrix.T @ state + rate_matrix.sum(axis=0) * state) * backward
        # dcontrol += -rate_matrix @ (state * backward) - (rate_matrix.T @ backward) * state
        dcontrol = (rate_matrix @ state) * backward
        dcontrol += rate_matrix.sum(axis=0) * state * backward
        dcontrol -= (rate_matrix.T @ backward) * state
        dcontrol -= rate_matrix @ (backward * state)
        return(dcontrol)

    def backward(self, time, state, control, forward_time, forward, rates):
        # perform interpolation
        forward = ut.interp_pwc(time, forward_time, forward)
        # compute controlled matrix
        control = np.exp(np.expand_dims(control, 1) - np.expand_dims(control, 0))
        rate_matrix = self.rate_matrix * control
        exit_rate = np.sum(rate_matrix, axis=0)
        # compute adjoint contribution
        dydt = exit_rate * state - rate_matrix.T @ state
        # compute kl contribution
        dydt += np.sum(self.rate_matrix - rate_matrix + rate_matrix*np.log(control), axis=0)
        return(dydt)

    def control_gradient(self, time, control, forward, backward, rates):
        # preparations 
        control_eff = np.exp(np.expand_dims(control, -1) - np.expand_dims(control, -2))
        rate_matrix = self.rate_matrix.reshape((1,1)+self.rate_matrix.shape) * control_eff #* (np.expand_dims(control, -1) - np.expand_dims(control, -2))
        rate_matrix_eff = rate_matrix * (np.expand_dims(control, -1) - np.expand_dims(control, -2))
        rate_matrix = np.expand_dims(rate_matrix, 2)
        rate_matrix_eff = np.expand_dims(rate_matrix_eff, 2)
        #forward_eff = ut.integrate_subsamples(time, forward)
        # kl contribution to the control
        grad = (rate_matrix_eff @ np.expand_dims(forward, -1)).squeeze() - forward*np.sum(rate_matrix_eff, axis=-2)
        # constraint contribution
        grad -= (rate_matrix @ np.expand_dims(forward, -1)).squeeze() * backward
        grad -= rate_matrix.sum(axis=-2) * forward * backward
        grad += (np.swapaxes(rate_matrix, -1, -2) @ np.expand_dims(backward, -1)).squeeze() * forward
        grad += (rate_matrix @ np.expand_dims(backward * forward, -1)).squeeze()
        # integrate subsampling
        grad = ut.integrate_subsamples(time, grad)
        return(grad)

    def rates_gradient(self, time, control, forward, backward, rates):
        return(None)

    def get_initial(self):
        return(self.initial)

    def kl_contrib(self, time, control, forward, rates):
        # preparations
        control = np.exp(np.expand_dims(control, 1) - np.expand_dims(control, 0))
        rate_matrix = self.rate_matrix * control
        # compute contribution to the divergence
        kl_contrib = forward @ np.sum(self.rate_matrix - rate_matrix + rate_matrix * np.log(control), axis=0)
        return(kl_contrib)

    def kl_prior(self, time, control, forward, rates):
        # preparations
        control_eff = np.exp(np.expand_dims(control, -1) - np.expand_dims(control, -2))
        rate_matrix = self.rate_matrix.reshape((1, 1)+self.rate_matrix.shape) * control_eff
        forward_eff = ut.integrate_subsamples(time, forward)
        # compute contributions
        kl = forward_eff * np.sum(self.rate_matrix.reshape((1, 1)+self.rate_matrix.shape) - rate_matrix + rate_matrix * np.log(control_eff), axis=-2)
        return(kl)

    def num_controls(self):
        return(self.dim)

    def num_param(self):
        return(self.dim*(self.dim-1))

    def num_states(self):
        return(self.dim)

    @property
    def rates(self):
        return(self.rate_matrix.flatten())
