import numpy as np
from pymbvi.models.model import Model
import pymbvi.util as ut

"""
The idea of this class is to provide a standard interface for kinetic models with custom closure.

Let S(X) and R(X) be vectors of summary statistics of a multivariate Markov process X. Set
    phi(t) = E[S(X_t)] .
From the standard moment equations, we get
    d/dt phi(t) = A phi(t) + B E[R(X_t)] .
Using moment closure, we arrive at the form
    d/dt phi(t) = A phi(t) + B psi(phi(t))
To implement the variational inference, we only require matrices A, B, and a closure function psi. 
We alsor require a matrix that computes the natural moments from the selected moments
This allows us to obtain a more efficient implementation, since we can 
"""


class ClosureModel(Model):

    def __init__(self, initial, A, B, C, psi, rates, tspan):
        """
        Constructor method
        Input:
            A: (num_species, num_species, )
        """
        # store inputs
        self.initial = initial.copy()
        self.rates = rates.copy()
        self.tspan = tspan.copy()
        # derived quantities
        self.num_moments = len(initial)
        self.num_reactions = len(rates)
        # stuff for ode functions
        self.A = A
        self.B = B
        self.closure = closure

    # interface functions required by abstract Model class

    def num_states(self):
        return(len(self.initial))

    def num_controls(self):
        return(len(self.rates))

    def forward(self, time, state, control, rates):
        """
        Identical to forward but expecting torch tensor input
        """
        # evaluate the derivative
        eff_control = np.exp(control+rates)
        closure = self.closure(state)
        dydt = (self.A@eff_control)@state + (self.B@eff_control)@closure
        return(dydt)

    def backward(self, time, state, control, forward_time, forward, rates):
        # perform interpolation
        forward = ut.interp_pwc(time, forward_time, forward)
        # compute control part
        alpha_eff = self.kl_equation(control, rates)
        # compute Jacobian
        eff_control = np.exp(control+rates)
        closure = self.closure_backward(forward, state)
        dydt = self.C.T@(1 - np.exp(control) + control*np.exp(control)) - (self.A@eff_control).T@state - (self.B@eff_control).T@closure
        return(dydt)

    def control_gradient_old(self, forward, backward):
        """
        Evaluates the control gradient  on a number of support points
        """
        # number of time_steps
        num_steps = forward.shape[0]
        # initialize
        grad = np.zeros((num_steps, 6))
        # contributions lagrange multpliers
        for i in range(num_steps):
            fj = np.tensordot(self.forward_mat, forward[i, :], ([1], [0]))+self.forward_vec
            grad[i, :] = fj.T@backward[i, :]
        return(grad)

    def natural_moments(self, forward):
        """
        Same as natural)moments but in torch version
        """
        prop = (self.natural_moment_mat@(forward.T)).T + self.natural_moment_vec
        return(prop)

    def kl_equation(self, control, rates):
        #print('Kl equation stuff: {0}'.format(tmp))
        control_eff = rates[0, :]-control+control*np.log(control/rates[1, :])
        return(control_eff)

    # functions that have to be provided by derived classes

    def set_forward_mat(self):
        """
        matrix that transforms controls to state
        """
        raise NotImplementedError

    def set_forward_vec(self):
        """
        ofset depending on control
        """
        raise NotImplementedError

    def set_natural_moment_mat(self):
        raise NotImplementedError

    def set_natural_moment_vec(self):
        raise NotImplementedError

    def kl_prior(self, state, get_gradient=False):
        if get_gradient:
            return(0.0, np.zeros(state.size))
        else:
            return(0.0)

    def get_initial(self):
        return(self.initial.numpy())


