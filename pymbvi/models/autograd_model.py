# This files contains a class of variational models that use the torch autograd framework to compute required derivatives

import numpy as np
import torch
from scipy.linalg import solve
from pymbvi.models.model import Model
import pymbvi.util as ut

# global options
torch.set_default_dtype(torch.float64)


class AutogradModel(Model):
    """
    This model class also uses autograd to compute the prior kl divergence contribution and required gradients.
    It requires a forward_torch and a kl_contrib_torch function and does not come with any restrictions on the control class
    """

    # constructor and setup

    def __init__(self, initial, rates, tspan, gradient_mode='natural'):
        """
        Constructor method
        """
        # store inputs
        self.initial = AutogradModel.init_tensor(initial)
        self.rates = AutogradModel.init_tensor(rates)
        self.tspan = AutogradModel.init_tensor(tspan)
        # derived quantities
        self.gradient_mode = gradient_mode
        self.fisher_reg = 0.0

    @staticmethod
    def init_tensor(array):
        """
        initialize depending on input
        """
        if type(array) is np.ndarray:
            new_array = torch.tensor(array)
        elif type(array) is list:
            new_array = torch.tensor(array)
        elif type(array) is torch.Tensor:
            new_array = array.clone().detach()
        else:
            raise ValueError('Invalid input type for input {}'.format(array))
        return(new_array)

    def forward(self, time, state, control, rates):
        """ 
        Compute r.h.s of the forward differential equations 
        """
        # evaluate the derivative
        state_torch = torch.from_numpy(state)
        control_torch = torch.from_numpy(control)
        rates_torch = torch.from_numpy(rates)
        dydt = self.forward_torch(time, state_torch, control_torch, rates_torch)
        return(dydt.numpy())

    def backward(self, time, state, control, forward_time, forward, rates):
        # perform interpolation
        forward = ut.interp_pwc(time, forward_time, forward)
        with torch.set_grad_enabled(True):
            # convert to torch stuff
            forward_torch = torch.from_numpy(forward)
            forward_torch.requires_grad = True
            control_torch = torch.from_numpy(control)
            rates_torch = torch.from_numpy(rates)
            # compute contribution of the stat tensor
            tmp = self.kl_contrib_torch(time, forward_torch, control_torch, rates_torch)
            tmp.backward()
            dydt = np.array(forward_torch.grad)
            forward_torch.grad.zero_()
            # add contribution from the forward equation
            tmp = self.forward_torch(time, forward_torch, control_torch, rates_torch)
            tmp.backward(torch.from_numpy(state))
            dydt -= np.array(forward_torch.grad)
        return(dydt)

    def kl_prior(self, time, control, forward, rates):
        """
        For the independent control, the kl divergence only depends on the control
        """
        # preparations
        dim = forward.shape
        kl = np.zeros(dim[0:3]+(1,))
        rates_torch = torch.from_numpy(rates)
        # iterate over stored values
        for i in range(dim[0]):
            for j in range(dim[1]):
                control_torch = torch.from_numpy(control[i, j])
                for k in range(dim[2]):
                    forward_torch = torch.from_numpy(forward[i, j, k])
                    kl[i, j, k] = self.kl_contrib_torch(time[i, j, k], forward_torch, control_torch, rates_torch).numpy()
        # integrate ver subsamples
        kl = ut.integrate_subsamples(time, kl)
        return(kl)

    def control_gradient(self, time, control, forward, backward, rates):
        # preparations
        dim = forward.shape
        control_grad = np.zeros(dim[0:3]+control.shape[-1:])
        rates_torch = torch.from_numpy(rates)
        # iterate over stored values
        with torch.set_grad_enabled(True):
            for i in range(dim[0]):
                for j in range(dim[1]):
                    control_torch = torch.tensor(control[i, j], requires_grad=True)
                    for k in range(dim[2]):
                        # prior kl contribution
                        forward_torch = torch.from_numpy(forward[i, j, k])
                        kl = self.kl_contrib_torch(time, forward_torch, control_torch, rates_torch)
                        kl.backward()
                        control_grad[i, j, k] += np.array(control_torch.grad)
                        control_torch.grad.zero_()
                        # forward contribution
                        backward_torch = torch.from_numpy(backward[i, j, k])
                        tmp = self.forward_torch(time, forward_torch, control_torch, rates_torch)
                        tmp.backward(backward_torch)
                        control_grad[i, j, k] -= np.array(control_torch.grad)
                        control_torch.grad.zero_()
        # integrate over subsamples
        control_grad = ut.integrate_subsamples(time, control_grad)
        if self.gradient_mode == 'natural':
            # compute fisher information
            fisher_g = self.compute_fisher(time, control, forward, rates)
            # tmp = self.stat_tensor(time, forward, rates)
            # tmp = ut.integrate_subsamples(time, tmp)
            # print(np.linalg.norm(fisher_g-tmp))
            # fisher transform
            control_grad = self.fisher_transform(fisher_g, control_grad)
        return(control_grad)

    def rates_gradient(self, time, control, forward, backward, rates):
        # preparations
        dim = forward.shape
        rates_grad = np.zeros(dim[0:3]+rates.shape)
        rates_torch = torch.tensor(rates, requires_grad=True)
        # iterate over stored values
        with torch.set_grad_enabled(True):
            for i in range(dim[0]):
                for j in range(dim[1]):
                    control_torch = torch.tensor(control[i, j])
                    for k in range(dim[2]):
                        # prior kl contribution
                        forward_torch = torch.from_numpy(forward[i, j, k])
                        kl = self.kl_contrib_torch(time, forward_torch, control_torch, rates_torch)
                        kl.backward()
                        if rates_torch.grad is not None:
                            rates_grad[i, j, k] += np.array(rates_torch.grad)
                            rates_torch.grad.zero_()
                        # forward contribution
                        backward_torch = torch.from_numpy(backward[i, j, k])
                        tmp = self.forward_torch(time, forward_torch, control_torch, rates_torch)
                        tmp.backward(backward_torch)
                        rates_grad[i, j, k] -= np.array(rates_torch.grad)
                        rates_torch.grad.zero_()
        # integrate over subsamples
        rates_grad = ut.integrate(time, rates_grad)
        # if self.gradient_mode == 'natural':
        #     # compute fisher information
        #     fisher_g = self.compute_fisher_g(time, ontrcol, forward, rates)
        #     # fisher transform
        #     control_grad = self.fisher_transform(fisher_g, control_grad)
        return(rates_grad)

    def joint_gradient(self, time, control, forward, backward, rates, options):
       # preparations
        dim = forward.shape
        control_grad = np.zeros(dim[0:3]+control.shape[-1:])
        rates_torch = torch.tensor(rates, requires_grad=True)
        rates_grad = np.zeros(dim[0:3]+rates.shape)
        # iterate over stored values
        with torch.set_grad_enabled(True):
            for i in range(dim[0]):
                for j in range(dim[1]):
                    control_torch = torch.tensor(control[i, j], requires_grad=True)
                    for k in range(dim[2]):
                        # prior kl contribution
                        forward_torch = torch.from_numpy(forward[i, j, k])
                        kl = self.kl_contrib_torch(time, forward_torch, control_torch, rates_torch)
                        kl.backward()
                        control_grad[i, j, k] += np.array(control_torch.grad)
                        control_torch.grad.zero_()
                        if rates_torch.grad is not None:
                            rates_grad[i, j, k] += np.array(rates_torch.grad)
                            rates_torch.grad.zero_()
                        # forward contribution
                        backward_torch = torch.from_numpy(backward[i, j, k])
                        tmp = self.forward_torch(time, forward_torch, control_torch, rates_torch)
                        tmp.backward(backward_torch)
                        control_grad[i, j, k] -= np.array(control_torch.grad)
                        rates_grad[i, j, k] -= np.array(rates_torch.grad)
                        control_torch.grad.zero_()
                        rates_torch.grad.zero_()
        # integrate over subsamples
        control_grad = ut.integrate_subsamples(time, control_grad)
        rates_grad = ut.integrate(time, rates_grad)
        if self.gradient_mode == 'natural':
            # compute fisher information
            fisher_g = self.compute_fisher(time, control, forward, rates)
            # tmp = self.stat_tensor(time, forward, rates)
            # tmp = ut.integrate_subsamples(time, tmp)
            # print(np.linalg.norm(fisher_g-tmp))
            # fisher transform
            control_grad = self.fisher_transform(fisher_g, control_grad)
        return(control_grad, rates_grad)

    def compute_fisher(self, time, control, forward, rates):
        # preparations
        dim = forward.shape
        fisher_g = np.zeros(dim[0:3]+(control.shape[-1], control.shape[-1]))
        rates_torch = torch.from_numpy(rates)
        # iterate
        for i in range(dim[0]):
            for j in range(dim[1]):
                control_torch = torch.tensor(control[i, j])
                for k in range(dim[2]):
                    # set up output function
                    forward_torch = torch.from_numpy(forward[i, j, k])
                    fisher_g[i, j, k] = self.fisher_loc(time, control_torch, forward_torch, rates_torch)
        fisher_g = ut.integrate_subsamples(time, fisher_g)
        return(fisher_g)

    def fisher_transform(self, fisher_g, control_grad):
        # set up output
        dim = control_grad.shape
        transformed_grad = np.zeros(dim)
        # iterate over time
        for i in range(dim[0]):
            for j in range(dim[1]):
                G = fisher_g[i, j]
                tmp = control_grad[i, j]
                try:
                    transformed_grad[i, j] = solve(G+self.fisher_reg*np.eye(G.shape[0]), tmp, sym_pos=True)
                except np.linalg.LinAlgError:
                    print('Warnnig: Fisher not spd')
                    continue
        return(transformed_grad)

    def get_initial(self):
        return(self.initial)

    def num_states(self):
        return(self.initial.shape[0])

    def num_param(self):
        return(self.rates.shape[0])

    # helper functions to be implmented by derived classes

    def forward_torch(self, time, state, control, rates):
        """
        Identical to forward but expecting torch tensor input
        """
        raise NotImplementedError

    def kl_contrib_torch(self, time, state, control, rates):
        """
        Identical to forward but expecting torch tensor input
        """
        raise NotImplementedError

    def fisher_loc(self, time, control_torch, forward_torch, rates_torch):
        """
        Required for natural gradient mode
        """
        raise NotImplementedError

    def num_controls(self):
        raise NotImplementedError
