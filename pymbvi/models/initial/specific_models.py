# This file collects several obsrevation model classes relevant for kinetic models

from pymbvi.models.initial.initial_model import InitialModel
import numpy as np
import torch


class BasicInitial(InitialModel):
    """
    simple initial model
    """
    
    def sample(self, *args):
        pass

    def llh(self, *args):
        pass
    
    def kl(self, *args):
        pass

    def gradient(self, *args):
        pass


class ProductBernoulli(InitialModel):
    """
    Product Bernoulli Prior model
    """

    def __init__(self, prob, gradient_mode='natural', parametrization='regular'):
        """
        Constructor for the product Bernoulli prior
        prob: vector of probabilities
        """
        self.dim = len(prob)
        self.prob = prob
        self.gradient_mode = gradient_mode
        self.parametrization = parametrization

    def sample(self):
        rand = np.random.rand(self.dim)
        sample = 1.0*(rand < prob)
        return(sample)

    def llh(self, state):
        llh = state*np.log(self.prob) + (1-state)*np.log(1-self.prob)
        return(sample.sum())
    
    def kl(self, m):
        """
        kl divergence of two bernoullis
        """
        if self.parametrization == 'regular':
            kl = m*np.log(m/self.prob) + (1-m)*np.log((1-m)/(1-self.prob))
        elif self.parametrization == 'sigmoid':
            m_eff = 1/(1+np.exp(-m))
            kl = m_eff*np.log(m_eff/self.prob) + (1-m_eff)*np.log((1-m_eff)/(1-self.prob))
        else:
            raise ValueError('Invalid parametrization ' + self.parametrization)
        return(kl.sum())

    def gradient(self, m, eta):
        """ 
        natural gradient under bernoulli assumptions
        """
        if self.parametrization == 'regular':
            grad = np.log(m/self.prob) - np.log((1-m)/(1-self.prob))
            if self.gradient_mode == 'natural':
                grad = m*(1-m)*(grad-eta)
            elif self.gradient_mode == 'regular':
                grad = (grad-eta)
            else:
                raise ValueError('Unrecognized option ' + self.gradient_mode + ' for attribute gradient_mode')
        elif self.parametrization == 'sigmoid':
            m_eff = 1/(1+np.exp(-m))
            grad = np.log(m_eff/self.prob) - np.log((1-m_eff)/(1-self.prob))
            if self.gradient_mode == 'natural':
                grad = grad - eta
            elif self.gradient_mode == 'regular':
                grad = (grad-eta)*(1/(1+np.exp(-m)))*(1/(1+np.exp(m)))
            else:
                raise ValueError('Unrecognized option ' + self.gradient_mode + ' for attribute gradient_mode')
        return(grad)

    def get_moments(self, param=None):
        if param is None:
            return(self.prob.copy())
        if self.parametrization == 'regular':
            m = param.copy()
        elif self.parametrization == 'sigmoid':
            m = 1/(1+np.exp(-param))
        else:
            raise ValueError('Invalid parametrization ' + self.parametrization)
        return(m)

