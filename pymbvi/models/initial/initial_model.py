from abc import ABC, abstractmethod


class InitialModel(ABC):
    """
    ABstract initial model class. Methods are required by the variational engine.
    """

    @abstractmethod
    def sample(self, *args):
        pass

    @abstractmethod
    def llh(self, *args):
        pass
    
    @abstractmethod
    def kl(self, *args):
        pass

    @abstractmethod
    def gradient(self, *args):
        pass

    @abstractmethod
    def get_moments(self, *args):
        pass