from pymbvi.models.observation.obs_model import ObservationModel
import numpy as np
import torch


class CTMCObs(ObservationModel):
    """
    This observation model class is for CTMCs with finite state space
    """

    def __init__(self, dim, obs_param):
        self.dim = dim
        self.llh_vec = np.zeros(dim)
        self.obs_param = obs_param

    def sample(self, state, time=None):
        """
        Sampling function, to be implemented by derived class
        """
        raise NotImplementedError    

    def llh(self, state, obs):
        """
        observation likelihood function, to be implemented by derived class
        """
        raise NotImplementedError

    def update_llh_vec(self, obs):
        for i in range(self.dim):
            self.llh_vec[i] = self.llh(i, obs)
        return

    def get_residual(self, moments, observation, sample_time, obs_param):
        # compute llh vetor
        self.update_llh_vec(observation)
        # take average
        res = -moments @ self.llh_vec
        return(res)

    def get_terminal(self, moments, observation, sample_time, obs_param):
        # compute llh vector 
        self.update_llh_vec(observation)
        return(self.llh_vec.copy())


class CTMCGaussObs(CTMCObs):

    def sample(self, state, time=None):
        """
        Sample from Gaussian
        """
        return(state+self.obs_param*np.random.randn())

    def llh(self, state, obs):
        """
        Compute Gaussian llh
        """
        llh = -0.5*np.log(2*np.pi)-np.log(self.obs_param)-0.5*(obs-state)**2/self.obs_param**2
        return(llh)

