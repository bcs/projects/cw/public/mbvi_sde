# This file collects several observation model classes relevant 

from pymbvi.models.observation.obs_model import ObservationModel
import numpy as np
import math
import torch


class MeanfieldObsModel(ObservationModel):

    def __init__(self, obs_param, dim):
        self.obs_param = obs_param
        self.dim = dim
        return

    def get_residual(self, state, observation, sample_time, obs_param):
        res = -self.llh(torch.from_numpy(state), torch.from_numpy(observation), torch.from_numpy(obs_param))
        return(res.numpy())

    def get_terminal(self, state, observation, sample_time, obs_param):
        state_torch = torch.from_numpy(state)
        state_torch.requires_grad = True
        tmp = self.llh(state_torch, torch.from_numpy(observation), torch.from_numpy(obs_param))
        tmp.backward()
        return(state_torch.grad.detach().numpy())

    def get_parameters(self):
        return(self.obs_param.copy())

    def sample(self, state, obs_param=None):
        raise NotImplementedError

    def llh(self, state, observation, obs_param=None):
        raise NotImplementedError


class MultiGaussObs(MeanfieldObsModel):
    """
    Gaussian observation model where each component is observed independently
    Input:
        sigma: (dim,) 
    """

    def __init__(self, obs_param):
        super().__init__(obs_param, len(obs_param))

    def sample(self, state, obs_param=None):
        output = state+self.obs_param*np.random.randn(self.dim)
        return(output)

    def llh(self, state, observation, obs_param=None):
        llh = -0.5*self.dim*torch.log(torch.tensor(2*math.pi))-torch.log(obs_param).sum()-0.5*torch.sum((observation-state)**2/obs_param**2)
        return(llh)