# This file collects several obsrevation model classes relevant for kinetic models

from pymbvi.models.observation.obs_model import ObservationModel
import math
import numpy as np
import torch

class GaussObsDiag(ObservationModel):
    """
    This model implements observations of a single species with Gaussian noise
    """
    
    def __init__(self, decoder, dim, num_samples=100):
        self.decoder = decoder
        self.dim = dim
        self.cov_map = self.set_cov_map()
        self.samples = torch.randn((num_samples, dim))
        """
        Inputs:
            decoder: neural network mapping the latent state to the mean and variance of a Gaussian 
            dim: dimension of the latent space
        """   

    def sample(self, state, time=None):
        state_torch = torch.from_numpy(state)
        sample = self.sample_torch(state_torch).numpy()
        return(sample)

    def sample_torch(self, state):
        mu, sigma = self.decoder(state)
        sample = mu + torch.randn(self.dim)*sigma
        return(sample)

    def llh(self, state, obs):
        llh = self.llh_torch(torch.from_numpy(state), torch.from_numpy(obs)).numpy()
        return(llh)

    def llh_torch(self, state, obs):
        mu, sigma = self.decoder(state)
        llh = -0.5*torch.log(2*math.pi)*len(mu) - np.log(self.sigma).sum() - 0.5*(((obs-mu)/sigma)**2).sum()
        return(llh)

    def get_residual(self, moments, observation, sample_time, obs_param):
        residual = self.get_residual_torch(torch.from_numpy(moments), torch.from_numpy(observations), sample_time, obs_param).numpy()
        return(residual)

    def get_residual_torch(self, moments, observation, sample_time, obs_param):
        # get moments
        m, M = self.get_moments(state)
        # create samples
        R = torch.cholesky(M)
        samples = m + self.samples @ R.T
        # get expected likelihood
        llh = self.llh_torch(self, samples, observation)
        residual = -llh.sum()
        return(residual)

    def get_terminal(self, moments, observation, sample_time, obs_param):
        terminal = self.get_terminal_torch(torch.from_numpy(moments), torch.from_numpy(observations), sample_time, obs_param)
        return(terminal)

    def get_terminal_torch(self, moments, observation, sample_time, obs_param):
        # compute terminal via autograd gradient
        moments.requires_grad = True
        residual = self.get_residual_torch(moments, observation, sample_time, obs_param)
        residual.backward()
        return(-moments.grad.detach())
        

    def get_moments(self, state):
        """
        Convert state to mean and covariance matrix
        """
        # extract first moment
        m = state[:self.num_species]
        # construct second moment
        tmp = torch.zeros((self.num_species, self.num_species))
        tmp[self.cov_map[0], self.cov_map[1]] = state[self.num_species:]
        M = tmp+tmp.T-torch.diag(tmp.diag())
        # return mean and covariance
        return(m, M)

    def set_cov_map(self):
        """
        Construct a tensor that maps the reduced moments to a full covariance matrix
        """
        cov_map = torch.tril_indices(row=self.num_species, col=self.num_species, offset=0)
        return(cov_map)  

