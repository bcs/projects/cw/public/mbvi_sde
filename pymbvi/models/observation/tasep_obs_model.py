# implementation of a specific form of the observation model used for the tasep process as model for transcripiton

from pymbvi.models.observation.obs_model import ObservationModel
import numpy as np
from scipy.linalg import solve


class Gauss(ObservationModel):
    """
    This model implements observations of the tasep model. Observations are obtained from
    a weighted sum of polymerases. 
    """
    
    def __init__(self, obs_param, alpha, var_reg=1.0):
        """
        The model requires two inputs
            obs_param:  np.array([b0, b1, lambda, gamma, sigma]) where
                b0: non-bleachable background
                b1: bleachable background
                ldambda: rate of exponential bleaching model
                gamma: scaling factor between active stemloop and observed intensity
                sigma: noise level of the mulitplicative noise
        """
        self.b0, self.b1, self.lamb, self.gamma, self.sigma = obs_param.reshape(-1, 1)
        self.alpha = alpha
        self.var_reg = var_reg

    def sample(self, state, time):
        # compute raw intensity
        I = self.intensity(state, time)
        # apply multiplicative noise
        sample = I+self.sigma*np.random.standard_normal(I.shape)
        return(sample)

    def llh(self, state, time, obs):
        # compute intensity
        I = self.intensity(state, time)
        # compute log likelihood
        llh = -0.5*np.sum((obs-I)**2)/self.sigma**2
        llh += (-0.5*np.log(2*np.pi)-np.log(self.sigma))*len(I)
        return(llh)

    def llh_vec(self, state, time, obs):
        # compute intensity
        I = self.intensity(state, time)
        # compute log likelihood
        llh = -0.5*(obs-I)**2/self.sigma**2
        llh += -0.5*np.log(2*np.pi)-np.log(self.sigma)
        return(llh)

    def get_residual(self, moments, observation, sample_time, obs_param):
        """
        Compute contribution of the obseravtions to the objective functions
        """
        # get required summary statistics
        exp_intensity = self.intensity(moments, sample_time)
        var_intensity = self.var_intensity(moments, sample_time)
        # contribution of the normalizer
        residual = 0.5*np.log(2*np.pi)+np.log(self.sigma)
        # central moment contribution
        residual += 0.5*self.var_reg*var_intensity/self.sigma**2
        # observation contribution
        residual += 0.5*(observation-exp_intensity)**2/self.sigma**2
        return(residual)

    def get_terminal(self, moments, observation, sample_time, obs_param):
        """
        Compute the reset condition depending on moments and observations
        """
        # get required summary statistics
        exp_intensity = self.intensity(moments, sample_time)
        grad_exp_intensity = self.grad_exp_intensity(moments, sample_time)
        grad_var_intensity = self.grad_var_intensity(moments, sample_time)
        # get updates
        terminal = (observation-exp_intensity)/self.sigma**2*grad_exp_intensity
        terminal += -0.5*self.var_reg*grad_var_intensity/self.sigma**2
        return(terminal)

    # helper functions to calculate some intermediate values

    def intensity(self, state, time, obs_param=None):
        """
        compute intensity (without stochstic noise part)
            state is either an array of shape (L, ) or (num_steps, L)
            time shold be of shape (1, ) ore (num_steps, ) according to the form of state
        """
        if obs_param is None:
            b0, b1, lamb, gamma, sigma_raw = self.b0, self.b1, self.lamb, self.gamma, self.sigma
        else:
            b0, b1, lamb, gamma, sigma_raw = obs_param
        # get number of stemloops
        N = state @ self.alpha
        # add background and scaling
        I = b0 + np.exp(-lamb*time)*(b1+gamma*N)
        return(I)

    def grad_exp_intensity(self, moments, time):
        """
        Calculate expected intensity under product bernoulli approximation
        """
        # compute gradient
        grad = np.exp(-self.lamb*time)+self.gamma*self.alpha
        return(grad)

    def var_intensity(self, moments, time):
        """ 
        Calculate variance of intensity under product bernoulli approximation
        """
        prefactor = np.exp(-2*self.lamb*time)*self.gamma**2
        N_sq = np.sum(self.alpha**2*moments*(1-moments))
        var = prefactor*N_sq
        return(var)

    def grad_var_intensity(self, moments, time):
        prefactor = np.exp(-2*self.lamb*time)*self.gamma**2
        grad = prefactor*self.alpha**2*(1-2*moments)
        return(grad)

    def num_param(self):
        """
        Return number of optimizeable parameters assuming (b0, b1 and gamma) are variable
        """
        return(5)

    def get_parameters(self):
        """
        Return array of parameters
        """
        obs_param = np.array([self.b0, self.b1, self.lamb, self.gamma, self.sigma])
        return(obs_param.flatten())


class LognormGauss(ObservationModel):
    """
    This model implements observations of the tasep model. Observations are obtained from
    a weighted sum of polymerases. 
    """
    
    def __init__(self, obs_param, alpha, var_reg=1.0):
        """
        The model requires two inputs
            obs_param:  np.array([b0, b1, lambda, gamma, sigma]) where
                b0: non-bleachable background
                b1: bleachable background
                ldambda: rate of exponential bleaching model
                gamma: scaling factor between active stemloop and observed intensity
                sigma: noise level of the mulitplicative noise
        """
        self.b0, self.b1, self.lamb, self.gamma, self.sigma = obs_param.reshape(-1, 1)
        self.alpha = alpha
        self.var_reg = var_reg
        self.sigma_prior = np.array([0.05, 0.05, 0.2])

    def sample(self, state, time):
        # compute raw intensity
        I = self.intensity(state, time)
        # apply multiplicative noise
        sample = I*np.exp(self.sigma*np.random.standard_normal(I.shape))
        return(sample)

    def llh(self, state, time, obs):
        # compute intensity
        I = self.intensity(state, time)
        # compute log likelihood
        llh = -0.5*np.sum((np.log(obs)-np.log(I)))**2/self.sigma**2
        llh += -np.sum(np.log(obs))
        llh += (-0.5*np.log(2*np.pi)-np.log(self.sigma))*len(I)
        return(llh)

    def llh_vec(self, state, time, obs):
        # compute intensity
        I = self.intensity(state, time)
        # compute log likelihood
        llh = -0.5*(np.log(obs)-np.log(I))**2/self.sigma**2
        llh += -np.log(obs)
        llh += -0.5*np.log(2*np.pi)-np.log(self.sigma)
        return(llh)

    def llh_vec_appr(self, state, time, obs):
        # compute intensity
        sigma = self.sigma*obs
        I = self.intensity(state, time)
        # compute log likelihood
        llh = -0.5*(obs-I)**2/sigma**2
        llh += -0.5*np.log(2*np.pi)-np.log(sigma)
        return(llh)

    def get_residual(self, moments, observation, sample_time, obs_param):
        """
        Compute contribution of the obseravtions to the objective functions
        """
        # aproximate evaluation by scaling noise with observation
        sigma = obs_param[4]*observation
        # get required summary statistics
        exp_intensity = self.intensity(moments, sample_time, obs_param)
        var_intensity = self.var_intensity(moments, sample_time, obs_param)
        # contribution of the normalizer
        residual = 0.5*np.log(2*np.pi)+np.log(sigma)
        # central moment contribution
        residual += 0.5*self.var_reg*var_intensity/sigma**2
        # observation contribution
        residual += 0.5*(observation-exp_intensity)**2/sigma**2
        return(residual)

    def get_terminal(self, moments, observation, sample_time, obs_param):
        """
        Compute the reset condition depending on moments and observations
        """
        # aproximate evaluation by scaling noise with observation
        sigma = obs_param[4]*observation
        # get required summary statistics
        exp_intensity = self.intensity(moments, sample_time, obs_param)
        grad_exp_intensity = self.grad_exp_intensity(moments, sample_time, obs_param)
        grad_var_intensity = self.grad_var_intensity(moments, sample_time, obs_param)
        # get updates
        terminal = (observation-exp_intensity)/sigma**2*grad_exp_intensity
        terminal += -0.5*self.var_reg*grad_var_intensity/sigma**2
        return(terminal)

    def gradientr(self, moments, observation, sample_time, obs_param):
        """
        Derivative of residual w.r.p to the observation parameters
        This function is only required for doing inference on the observation parameters
        """
        # aproximate evaluation by scaling noise with observation
        sigma = obs_param[4]*observation
        # get mean and variance contribution
        exp_intensity = self.intensity(moments, sample_time, obs_param)
        grad_exp_intensity = self.grad_exp_intensity_param(moments, sample_time, obs_param)
        grad_var_intensity = self.grad_var_intensity_param(moments, sample_time, obs_param)
        # construct gradient
        grad = (exp_intensity-observation)/sigma**2*grad_exp_intensity
        grad += +0.5*self.var_reg*grad_var_intensity/sigma**2
        return(grad)

    def gradientn(self, moments, observation, sample_time, obs_param, k=1e-6):
        """
        Derivative of residual w.r.p to the observation parameters
        This function is only required for doing inference on the observation parameters
        """
        # aproximate evaluation by scaling noise with observation
        sigma = obs_param[4]*observation
        # get mean and variance contribution
        exp_intensity = self.intensity(moments, sample_time, obs_param)
        grad_exp_intensity = self.grad_exp_intensity_param(moments, sample_time, obs_param)
        grad_var_intensity = self.grad_var_intensity_param(moments, sample_time, obs_param)
        # construct gradient
        grad = -(observation-exp_intensity)/sigma**2*grad_exp_intensity
        grad += +0.5*self.var_reg*grad_var_intensity/sigma**2
        # consruct local fisher information 
        fisher_g = grad_exp_intensity[[0, 1, 3]]/(sigma*exp_intensity)
        fisher_g = np.outer(fisher_g, fisher_g)
        natural_grad = solve(fisher_g.T@fisher_g+k*np.eye(fisher_g.shape[0]), fisher_g.T@grad[[0, 1, 3]], sym_pos=True)
        grad[[0, 1, 3]]

    def gradient_loc(self, moments, observation, sample_time, obs_param, k=1e-6):
        """
        Derivative of residual w.r.p to the observation parameters
        This function is only required for doing inference on the observation parameters
        """
        # aproximate evaluation by scaling noise with observation
        sigma = obs_param[4]*observation
        # get mean and variance contribution
        exp_intensity = self.intensity(moments, sample_time, obs_param)
        grad_exp_intensity = self.grad_exp_intensity_param(moments, sample_time, obs_param)
        grad_var_intensity = self.grad_var_intensity_param(moments, sample_time, obs_param)
        # construct gradient
        grad = -(observation-exp_intensity)/sigma**2*grad_exp_intensity
        grad += +0.5*self.var_reg*grad_var_intensity/sigma**2
        # consruct local fisher information 
        fisher_g = grad_exp_intensity[[0, 1, 3]]/(sigma)
        fisher_g = np.outer(fisher_g, fisher_g)
        return(grad[[0, 1, 3]], fisher_g)

    def gradient(self, forward, obs_data, obs_times, obs_param):
        """
        Full gradient of all time steps
        """
        grad = np.zeros((len(obs_times), 3))
        fisher_g = np.zeros((len(obs_times), 3, 3))
        for i in range(len(obs_times)):
            grad_i, fisher_i = self.gradient_loc(forward[i, -1, -1, :], obs_data[i], obs_times[i], obs_param)
            grad[i] = grad_i
            fisher_g[i] = fisher_i
        grad = grad.sum(axis=0)+self.grad_prior(obs_param)
        fisher_g = fisher_g.sum(axis=0)
        k = 1e-6
        # compute natural gradient
        sol = solve(fisher_g.T@fisher_g+k*np.eye(fisher_g.shape[0]), fisher_g.T@grad, sym_pos=True)
        sol = grad
        gradient = np.zeros(5)
        gradient[[0, 1, 3]] = sol
        return(gradient)

    # helper functions to calculate some intermediate values

    def intensity(self, state, time, obs_param=None):
        """
        compute intensity (without stochstic noise part)
            state is either an array of shape (L, ) or (num_steps, L)
            time shold be of shape (1, ) ore (num_steps, ) according to the form of state
        """
        if obs_param is None:
            b0, b1, lamb, gamma, sigma_raw = self.b0, self.b1, self.lamb, self.gamma, self.sigma
        else:
            b0, b1, lamb, gamma, sigma_raw = obs_param
        # get number of stemloops
        N = state @ self.alpha
        # add background and scaling
        I = b0 + np.exp(-lamb*time)*(b1+gamma*N)
        return(I)

    def grad_exp_intensity(self, moments, time, obs_param):
        """
        Gradient of excpected intensity w.r.t. moments
        """
        # preparations
        b0, b1, lamb, gamma, sigma_raw = obs_param
        N = moments @ self.alpha
        # compute gradient
        grad = np.exp(-lamb*time)*gamma*self.alpha
        return(grad)

    def grad_exp_intensity_param(self, moments, time, obs_param):
        """
        Gradient of expected intensity w.r.t. parameters
        """
        # preparations
        b0, b1, lamb, gamma, sigma_raw = obs_param
        N = moments @ self.alpha
        # compute gradient
        grad = np.zeros(self.num_param())
        grad[0] = 1.0
        grad[1] = np.exp(-lamb*time)
        grad[3] = np.exp(-lamb*time)*N
        return(grad)

    def var_intensity(self, moments, time, obs_param):
        """ 
        Calculate variance of intensity under product bernoulli approximation
        """
        # preparations
        b0, b1, lamb, gamma, sigma_raw = obs_param
        prefactor = np.exp(-2*lamb*time)*gamma**2
        N_sq = np.sum(self.alpha**2*moments*(1-moments))
        # compute variance
        var = prefactor*N_sq
        return(var)

    def grad_var_intensity(self, moments, time, obs_param):
        """
        Gradient of the intenity w.r.t. the moments
        """
        # preparations
        b0, b1, lamb, gamma, sigma_raw = obs_param
        prefactor = np.exp(-2*lamb*time)*gamma**2
        # compute gradient
        grad = prefactor*self.alpha**2*(1-2*moments)
        return(grad)

    def grad_var_intensity_param(self, moments, time, obs_param):
        """
        Gradient of the intenity w.r.t. the parameters
        """
        # preparations
        b0, b1, lamb, gamma, sigma_raw = obs_param
        N = moments @ self.alpha
        # compute gradient
        grad = np.zeros(self.num_param())
        grad[3] = 2*gamma*np.exp(-2*lamb*time)*np.sum(self.alpha**2*moments*(1-moments))
        return(grad)

    def num_param(self):
        """
        Return number of optimizeable parameters assuming (b0, b1 and gamma) are variable
        """
        return(5)

    def get_parameters(self):
        """
        Return array of parameters
        """
        obs_param = np.array([self.b0, self.b1, self.lamb, self.gamma, self.sigma])
        return(obs_param.flatten())

    def log_prior(self, obs_param):
        """
        Contribute prior contribution 
        """
        mu = np.array([self.b0, self.b1, self.gamma]).flatten()
        res = 0.5*(np.log(obs_param[[0, 1, 3]])-np.log(mu))**2/self.sigma_prior**2
        return(res.sum())

    def grad_prior(self, obs_param):
        mu = np.array([self.b0, self.b1, self.gamma]).flatten()
        grad = (np.log(obs_param[[0, 1, 3]])-np.log(mu))/(self.sigma_prior**2*obs_param[[0, 1, 3]])
        return(grad)