from abc import ABC, abstractmethod


class ObservationModel(ABC):
    """
    ABstract observationmodel class. Methods are required by the variational engine. 
    """

    @abstractmethod
    def sample(self, *args):
        pass

    def llh(self, *args):
        pass
    
    @abstractmethod
    def get_residual(self, *args):
        pass

    @abstractmethod
    def get_terminal(self, *args):
        pass

    def llh_vec(self, *args):
        return(self.llh(*args))

    def log_prior(self, obs_param):
        """
        log of the parameter prior distribution
        default is uniform prior and thus a zero return
        """
        return(0.0)

    def get_parameters(self):
        """
        Return parameter vector. Only relevant if optimizaton w.r.t. observation
        paramters is required
        """
        return(0.0)