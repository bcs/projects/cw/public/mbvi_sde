import numpy as np

# optimizers for the variational inference

def gradient_descent(fun, initial, iter=1, h=1e-2, projector=None, scheduler=None, message=True, delta=None):
    """
    fun is a function that ouptus a gradient and a value
    """
    # initialization
    val, grad = fun(initial)
    arg = initial
    elbo_hist = np.zeros(iter)
    for i in range(iter):
        if message:
            print('Elbo in iteration {0} is {1}.'.format(i, val))
        elbo_hist[i] = val
        # update argument
        arg_test = arg-h*grad
        if projector is not None:
            projector(arg_test, arg)
        # evaluate objective function for new argument
        val, grad = fun(arg_test)
        arg = arg_test
        if delta is not None and (h < delta):
            break
    return(arg, val, grad, elbo_hist)


def robust_gradient_descent(fun, initial, iter=1, h=1e-2, alpha=1.2, beta=0.5, projector=None, scheduler=None, adaptive=True, message=True, delta=None):
    """
    fun is a function that ouptus a gradient and a value
    """
    # initialization
    val, grad = fun(initial)
    arg = initial
    elbo_hist = np.zeros(iter)
    for i in range(iter):
        if message:
            print('Elbo in iteration {0} is {1}.'.format(i, val))
        elbo_hist[i] = val
        # update argument
        arg_test = arg-h*grad
        if projector is not None:
            projector(arg_test, arg)
        # evaluate objective function for new argument
        val_test, grad_test = fun(arg_test)
        if adaptive:
            if val_test < val:
                val = val_test
                grad = grad_test
                arg = arg_test
                h = alpha*h
            else:
                h = beta*h
            if scheduler is not None:
                val, grad = scheduler(i, fun, val, grad)
        else:
            arg, val, grad = arg_test, val_test, grad_test
            if scheduler is not None:
                scheduler(i, fun, arg, val, grad)  
        if delta is not None and (h < delta):
            break
    return(arg, val, grad, elbo_hist, h)


def multi_arg_gradient_descent(fun, initial, iter=1, h=1e-2, alpha=1.2, beta=0.5, projector=None, scheduler=None, adaptive=True, message=True, delta=None):
    """
    This is a version in which a joint gradient descent for several variables is performed
    initial is the list of arguments
    """
    # per cmoponent h
    num_comp = len(initial)
    if isinstance(h, float):
        h = h*np.ones(num_comp)
    # initialization
    val, grad = fun(initial)
    arg = initial
    elbo_hist = np.zeros(iter)
    for i in range(iter):
        if message:
            print('Elbo in iteration {0} is {1}.'.format(i, val))
        elbo_hist[i] = val
        # update argument
        arg_test = [arg[i]-h[i]*grad[i] for i in range(num_comp)]
        if projector is not None:
            projector(arg_test)
        # evaluate objective function for new argument
        if adaptive:
            val_test, grad_test = fun(arg_test)
            if val_test < val:
                val = val_test
                grad = grad_test
                arg = arg_test
                h = alpha*h
            else:
                h = beta*h
            if scheduler is not None:
                val, grad = scheduler(i, fun, arg, val, grad)
        else:
            arg = arg_test
            val, grad = fun(arg)
            if scheduler is not None:
                scheduler(i, fun, arg, val, grad)
        check = np.linalg.norm(h, 1)
        if delta is not None and (check < delta):
            break
    return(arg, val, grad, elbo_hist, h)


def alternating_gradient_descent(fun, initial, iter0=1, iter1=1, h=1e-3, projector=None, scheduler=None, message=True):
    # per cmoponent h
    num_comp = len(initial)
    if isinstance(h, float):
        h = h*np.ones(num_comp)
    # per component iterations
    if isinstance(iter1, int):
        iter1 = [iter1 for i in range(num_comp)]
    # outer iteration
    arg = initial.copy()
    val, grad = fun(arg)
    elbo_hist = []
    for i in range(iter0):
        # inner iterations
        for j in range(num_comp):
            # gradient steps
            for k in range(iter1[j]):
                if message:
                    print('Elbo in iteration {0}, {1}, {2} is {3}.'.format(i, j, k, val))  
                # update argument and evalute objective function
                arg_test = [arg[l].copy() for l in range(num_comp)]
                arg_test[j] -= h[j]*grad[j]
                val_test, grad_test = fun(arg_test)
                # update if new objective value is smaller
                if val_test < val:
                    val = val_test
                    grad = grad_test
                    arg = arg_test
                    h[j] = 1.2*h[j]
                else:
                    h[j] = 0.5*h[j] 
                elbo_hist.append(val)
    return(arg, val, grad, np.array(elbo_hist), h)


# mcmc optimizer


def la(vi_engine, initial, projector=None, iter=1, h=1e-2, alpha=1.2, beta=0.5):
    """
    fun is a function that ouptus a gradient and a value
    """
    # aliases
    fun = vi_engine.objective_function
    time = vi_engine.time
    control = vi_engine.control
    forward = vi_engine.forward
    backward = vi_engine.backward
    rates = vi_engine.rates
    # initialization
    val, grad = fun(initial)
    arg = initial.copy()
    fisher_g = vi_engine.transition_model.compute_fisher_g_control(time, control, forward, backward, rates)
    elbo_hist = np.zeros(iter)
    for i in range(iter):
        print('Elbo in iteration {0} is {1}.'.format(i, val))
        # draw proposal
        arg = arg - 0.5*h*grad + np.sqrt(h)*np.random.standard_normal(arg.shape)/np.sqrt(fisher_g)
        # evaluate at new argument
        val, grad = fun(arg)
        fisher_g = vi_engine.transition_model.compute_fisher_g_control(time, control, forward, backward, rates)
        elbo_hist[i] = val
    return(arg, val, grad, elbo_hist)


def mala(vi_engine, initial, projector=None, iter=1, h=1e-2, alpha=1.2, beta=0.5):
    """
    fun is a function that ouptus a gradient and a value
    """
    # aliases
    fun = vi_engine.objective_function
    time = vi_engine.time
    control = vi_engine.control
    forward = vi_engine.forward
    backward = vi_engine.backward
    rates = vi_engine.rates
    # initialization
    val, grad = fun(initial)
    arg = initial.copy()
    fisher_g = vi_engine.transition_model.compute_fisher_g_control(time, control, forward, backward, rates)
    elbo_hist = np.zeros(iter)
    accept = 0
    for i in range(iter):
        print('Elbo in iteration {0} is {1}.'.format(i, val))
        # draw proposal
        arg_new = arg - 0.5*h*grad + np.sqrt(h)*np.random.standard_normal(arg.shape)/np.sqrt(fisher_g)
        if projector is not None:
            projector(arg_new, arg)
        # evaluate at new argument
        val_new, grad_new = fun(arg_new)
        print(val_new)
        fisher_g_new = vi_engine.transition_model.compute_fisher_g_control(time, control, forward, backward, rates)
        # compute acceptance ratio
        prop_llh_new = 0.5*np.log(fisher_g).sum()-0.5*((arg_new-arg+0.5*h*grad)**2*fisher_g/h).sum()
        prop_llh_reverse = 0.5*np.log(fisher_g_new).sum()-0.5*((arg-arg_new+0.5*h*grad_new)**2*fisher_g_new/h).sum()
        log_accept = val - val_new + prop_llh_reverse - prop_llh_new
        # perform acceptance step
        u = np.random.rand()
        if u < np.exp(log_accept):
            val = val_new
            arg = arg_new
            grad = grad_new
            fisher_g = fisher_g_new
            accept += 1
        elbo_hist[i] = val
        print(accept/(i+1))
    return(arg, val, grad, elbo_hist)



# helper functions

def bound_projector(arg):
    """
    Cut of values above or below boundary
    """
    # set bounds
    bounds = np.array([1e-6, 1e6])
    # cut off larger values
    arg[arg > bounds[1]] = bounds[1]
    # cut off lower values
    arg[arg < bounds[0]] = bounds[0]