# test the sde variational engine with ou process example
import numpy as np
import torch
import matplotlib.pyplot as plt
import matplotlib.patches as ptch
from scipy.integrate import solve_ivp
from scipy.stats import special_ortho_group
from pathlib import Path
from src import plot_options

# load data
load_path = Path(__file__).parent.joinpath('gbm_multivariate_vi_720.npz')
data = np.load(load_path)

# extract data
t_sim = data['t_sim']
states_sim = data['states_sim']
t_obs = data['t_obs']
observations = data['observations']
t_prior = data['t_prior']
states_prior = data['states_prior']
t_post = data['t_post']
states_post = data['states_post']
initial_rates = data['initial_rates']
rates_post = data['rates_post']
cov_prior = data['cov_prior']
cov_post = data['cov_post']

# load data from file
load_path =  Path(__file__).parent.joinpath('gbm_multivariate_produce_data.npz')
data = np.load(load_path)

# extract data
gamma_true = data['gamma']
R_true = data['R']
D_true = R_true @ R_true.T
C_true = D_true/np.outer(np.sqrt(np.diag(D_true)), np.sqrt(np.diag(D_true)))
print(R_true)
# preparations
num_species = states_sim.shape[1]
num_moments = int(0.5*(num_species**2-num_species) + 2*num_species)
tril_ind = np.tril_indices(num_species)

# create R
r_initial = np.exp(initial_rates[:num_species])
R_initial = np.zeros((num_species, num_species))
R_initial[tril_ind[0], tril_ind[1]] = initial_rates[num_species:]
D_initial = R_initial @ R_initial.T
r_post = np.exp(rates_post[:num_species])
R_post = np.zeros((num_species, num_species))
R_post[tril_ind[0], tril_ind[1]] = rates_post[num_species:]
D_post = R_post @ R_post.T
C_post = D_post/np.outer(np.sqrt(np.diag(D_post)), np.sqrt(np.diag(D_post)))

print(gamma_true)
print(r_post)
print(max(t_sim))
print(len(t_obs))

# plot
plot_options.set_options(height=2)

fig, axes = plt.subplots(nrows=1, ncols=2)
#plt.subplot(1, 2, 1)
im1 = axes[0].imshow(C_true, cmap='hot', vmin=-0.6, vmax=0.6)
axes[0].text(1.0, -1.0, 'True', fontsize=10)
#plt.subplot(1, 2, 2)
im2 = axes[1].imshow(C_post, cmap='hot', vmin=-0.6, vmax=0.6)
axes[1].text(0.3, -1.0, 'Inferred', fontsize=10)
gca = plt.gca()
gca.set_yticklabels('')
#plt.colorbar(fig, orientation='horizontal')

fig.subplots_adjust(left=0.1, bottom=0.1, right=0.7, top=0.95)
cbar_ax = fig.add_axes([0.75, 0.35, 0.1, 0.34])
fig.colorbar(im2, cax=cbar_ax, orientation='vertical')

save_path = Path(__file__).parent.joinpath('gbm_mv_corr.pdf')
plt.savefig(save_path)

plt.show()

# create a box
anchor = [200, 0.96]
width = 100.0
height = 0.2
rect = ptch.Rectangle(anchor, width, height, fill=False)

plot_options.set_options(height=1.8)

# plot
plt.subplot(2, 2, 1)
plt.plot(t_sim, states_sim[:, 0], '--r')
plt.plot(t_prior, states_prior[:, 0], '-g')
plt.fill_between(t_prior, states_prior[:, 0] + np.sqrt(cov_prior[:, 0, 0]), states_prior[:, 0] - np.sqrt(cov_prior[:, 0, 0]), facecolor='g', alpha=0.5)
plt.plot(t_post, states_post[:, 0], '-b')
#plt.fill_between(t_post, states_post[:, 0] + np.sqrt(cov_post[:, 0, 0]), states_post[:, 0] - np.sqrt(cov_post[:, 0, 0]), facecolor='b', alpha=0.5)
plt.plot(t_obs, observations[:, 0], '--k')
plt.ylabel('$X_1$')
plt.ylim([0.5, 1.8])
gca = plt.gca()
gca.set_xticklabels('')
gca.yaxis.set_label_coords(-0.05, 1.0)

plt.subplot(2, 2, 2)
plt.plot(t_sim, states_sim[:, 1], '--r')
plt.plot(t_prior, states_prior[:, 1], '-g')
plt.fill_between(t_prior, states_prior[:, 1] + np.sqrt(cov_prior[:, 1, 1]), states_prior[:, 1] - np.sqrt(cov_prior[:, 1, 1]), facecolor='g', alpha=0.5)
plt.plot(t_post, states_post[:, 1], '-b')
#plt.fill_between(t_post, states_post[:, 1] + np.sqrt(cov_post[:, 1, 1]), states_post[:, 1] - np.sqrt(cov_post[:, 1, 1]), facecolor='b', alpha=0.5)
plt.plot(t_obs, observations[:, 1], '--k')
plt.ylim([0.5, 1.8])
plt.ylabel('$X_2$')
gca = plt.gca()
gca.add_patch(rect)
gca.set_xticklabels('')
gca.set_yticklabels('')
gca.yaxis.set_label_coords(-0.05, 1.0)

plt.subplot(2, 2, 3)
plt.plot(t_sim, states_sim[:, 2], '--r')
plt.plot(t_prior, states_prior[:, 2], '-g')
plt.fill_between(t_prior, states_prior[:, 2] + np.sqrt(cov_prior[:, 2, 2]), states_prior[:, 2] - np.sqrt(cov_prior[:, 2, 2]), facecolor='g', alpha=0.5)
plt.plot(t_post, states_post[:, 2], '-b')
#plt.fill_between(t_post, states_post[:, 2] + np.sqrt(cov_post[:, 2, 2]), states_post[:, 2] - np.sqrt(cov_post[:, 2, 2]), facecolor='b', alpha=0.5)
plt.plot(t_obs, observations[:, 2], '--k')
plt.ylim([0.5, 1.8])
plt.xlabel('Time in days')
plt.ylabel('$X_3$')
gca = plt.gca()
gca.yaxis.set_label_coords(-0.05, 1.0)

plt.subplot(2, 2, 4)
plt.plot(t_sim, states_sim[:, 3], '--r')
plt.plot(t_prior, states_prior[:, 3], '-g')
plt.fill_between(t_prior, states_prior[:, 3] + np.sqrt(cov_prior[:, 3, 3]), states_prior[:, 3] - np.sqrt(cov_prior[:, 3, 3]), facecolor='g', alpha=0.5)
plt.plot(t_post, states_post[:, 3], '-b')
#plt.fill_between(t_post, states_post[:, 3] + np.sqrt(cov_post[:, 3, 3]), states_post[:, 3] - np.sqrt(cov_post[:, 3, 3]), facecolor='b', alpha=0.5)
plt.plot(t_obs, observations[:, 3], '--k')
plt.ylim([0.5, 1.8])
plt.xlabel('Time in days')
plt.ylabel('$X_4$')
gca = plt.gca()
gca.set_yticklabels('')
gca.yaxis.set_label_coords(-0.05, 1.0)

#plt.subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.9, wspace=0.3, hspace=0.2)

save_path = Path(__file__).parent.joinpath('gbm_mv_smoothed.pdf')
plt.savefig(save_path)


plt.show()

plot_options.set_options(height=1.2)

plt.plot(t_sim, states_sim[:, 1], '--r', label='True state')
#plt.plot(t_prior, states_prior[:, 1], '-g')
#plt.fill_between(t_prior, states_prior[:, 1] + np.sqrt(cov_prior[:, 1, 1]), states_prior[:, 1] - np.sqrt(cov_prior[:, 1, 1]), facecolor='g', alpha=0.5)
plt.plot(t_post, states_post[:, 1], '-b', label='Posterior')
plt.fill_between(t_post, states_post[:, 1] + np.sqrt(cov_post[:, 1, 1]), states_post[:, 1] - np.sqrt(cov_post[:, 1, 1]), facecolor='b', alpha=0.5)
plt.plot(t_obs, observations[:, 1], 'xk', label='Observations')
plt.xlim([anchor[0], anchor[0]+width])
plt.ylim([anchor[1], anchor[1]+height])
#plt.ylabel('$X_2$')
plt.xlabel('Time in days')
gca = plt.gca()
#gca.yaxis.set_label_coords(-0.1, 1.1)
#plt.legend()

save_path = Path(__file__).parent.joinpath('gbm_mv_smoothed_zoomed.pdf')
plt.savefig(save_path)

plt.show()

plot_options.set_options(width=1.5, height=0.5)

plt.plot(t_sim, states_sim[:, 1], '--r', label='True state')
plt.plot(t_prior, states_prior[:, 1], '-g', label='Prior')
#plt.plot(t_post, states_post[:, 1], '-b', label='Posterior')
#plt.plot(t_obs, observations[:, 1], 'xk', label='Observations')
plt.xlim([0.0, 0.01])
plt.ylim([0.0, 0.01])
gca = plt.gca()
gca.axis('off')
plt.legend(frameon=False)

save_path = Path(__file__).parent.joinpath('gbm_mv_smoothed_legend_part1.pdf')
plt.savefig(save_path)

plt.show()

plot_options.set_options(width=1.5, height=0.5)

#plt.plot(t_sim, states_sim[:, 1], '--r', label='True state')
#plt.plot(t_prior, states_prior[:, 1], '-g', label='Prior')
plt.plot(t_post, states_post[:, 1], '-b', label='Posterior')
plt.plot(t_obs, observations[:, 1], 'xk', label='Observations')
plt.xlim([0.0, 0.01])
plt.ylim([0.0, 0.01])
gca = plt.gca()
gca.axis('off')
plt.legend(frameon=False)

save_path = Path(__file__).parent.joinpath('gbm_mv_smoothed_legend_part2.pdf')
plt.savefig(save_path)

plt.show()