# test the sde variational engine with ou process example
import os
os.environ["OMP_NUM_THREADS"] = "1" 
os.environ["OPENBLAS_NUM_THREADS"] = "1" 
os.environ["MKL_NUM_THREADS"] = "1" 
os.environ["VECLIB_MAXIMUM_THREADS"] = "1" 
os.environ["NUMEXPR_NUM_THREADS"] = "1" 
import numpy as np
import torch
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
from scipy.stats import special_ortho_group
from pathlib import Path
from pymbvi.variational_engine import VariationalEngine
from pymbvi.models.observation.kinetic_obs_model import MultiGaussObs
import pymbvi.models.diffusion.specific_models as vi_model
from pymbvi.models.diffusion.autograd_model import FullAutogradModel
import pymbvi.optimize as opt
from pyssa.sde import Simulator
from pyssa.models.diffusion import Diffusion
import pyssa.ssa as ssa
from src.models.gbm import GBM_Multivariate

# file_name = 'gbm_multivariate_produce_data.npz'
load_path = Path(__file__).parent.joinpath('gbm_multivariate_produce_data.npz')
data = np.load(load_path)

# extract data
ind = 3
num_samples = data['num_samples']
gamma_true = data['gamma']
R_true = data['R']
sigma_obs = data['sigma_obs']
timestep = data['timestep']
initial = data['initial']
tspan = data[f'tspan_{ind}']
delta_t = data['delta_t']
t_sim = data[f't_sim_{ind}']
states_sim = data[f'states_sim_{ind}']
t_obs = data[f't_obs_{ind}']
observations = data[f'observations_{ind}']

# preparations
num_species = len(initial)
num_moments = int(0.5*(num_species**2-num_species) + 2*num_species)

# create wrong rates
gamma = np.log(1.1)/360 * np.ones(num_species)
R = np.eye(num_species)*1e-2

# convert to rates for model
tril_ind = np.tril_indices(num_species)
rates = np.concatenate([np.log(gamma), R[tril_ind[0], tril_ind[1]]])

# set up gene expression model
moment_initial = np.zeros(num_moments)
moment_initial[:num_species] = initial
#model = vi_model.GBM_Multivariate(moment_initial, rates, tspan)
model = GBM_Multivariate(moment_initial, rates, tspan)

# set up an observation model
obs_model = MultiGaussObs(sigma_obs)

# compute prior
def odefun(time, state):
    return(model.forward(time, state, np.zeros(model.num_controls()), rates))
sol = solve_ivp(odefun, tspan, moment_initial, t_eval=t_sim)
t_prior = sol['t']
states_prior = sol['y'].transpose()
cov_prior = np.zeros((len(t_prior), num_species, num_species))
for i in range(len(t_prior)):
    cov_prior[i, tril_ind[0], tril_ind[1]] = states_prior[i, num_species:]
    cov_prior[i] = cov_prior[i] + cov_prior[i].T - np.diag(np.diag(cov_prior[i]))


# set up variational engine for inference 
options = {'smoothing': True, 'inference': True}
vi_engine = VariationalEngine(moment_initial, model, obs_model, t_obs, observations, subsample=20, tspan=tspan, options=options)
vi_engine.initialize()
h_par = 1e-8
h_cnt = 1e-2
h = [h_cnt, h_par]

# optimize
initial_arg = [vi_engine.control.copy(), vi_engine.rates.copy()]
sol = opt.alternating_gradient_descent(vi_engine.objective_function, initial_arg, iter0=50, iter1=5, h=h)

# # test optimization
# initial_rates = vi_inf.rates.copy()
# optimal_rates = opt.robust_gradient_descent(vi_inf.objective_function, initial_rates, iter=10, h=1e-3)[0]
# vi_inf.objective_function(optimal_rates)


# # run optimization
# initial_control = vi_engine.control.copy()
# optimal_control = robust_gradient_descent(vi_engine.objective_function, initial_control, iter=0, h=1e-3)[0]

# exract data for plotting
t_post = vi_engine.get_time()
states_post = vi_engine.get_forward()
cov_post = np.zeros((len(t_post), num_species, num_species))
for j in range(len(t_post)):
    cov_post[j, tril_ind[0], tril_ind[1]] = states_post[j, num_species:]
    cov_post[j] = cov_post[j] + cov_post[j].T - np.diag(np.diag(cov_post[j]))

# store stuff
save_path = Path(__file__).with_suffix('.npz')
np.savez(save_path, t_sim=t_sim, states_sim=states_sim, t_obs=t_obs, observations=observations, t_prior=t_prior, states_prior=states_prior, cov_prior=cov_prior, t_post=t_post, states_post=states_post, control_post=vi_engine.control, cov_post=cov_post, rates_post=vi_engine.rates, initial_rates=rates)

# # plot
# plt.subplot(2, 2, 1)
# plt.plot(t_sim, states_sim[:, 0], '--r')
# plt.plot(t_prior, states_prior[:, 0], '-g')
# plt.fill_between(t_prior, states_prior[:, 0] + np.sqrt(cov_prior[:, 0, 0]), states_prior[:, 0] - np.sqrt(cov_prior[:, 0, 0]), facecolor='g', alpha=0.5)
# plt.plot(t_post, states_post[:, 0], '-b')
# plt.fill_between(t_post, states_post[:, 0] + np.sqrt(cov_post[:, 0, 0]), states_post[:, 0] - np.sqrt(cov_post[:, 0, 0]), facecolor='b', alpha=0.5)
# plt.plot(t_obs, observations[:, 0], 'xk')

# plt.subplot(2, 2, 2)
# plt.plot(t_sim, states_sim[:, 1], '--r')
# plt.plot(t_prior, states_prior[:, 1], '-g')
# plt.fill_between(t_prior, states_prior[:, 1] + np.sqrt(cov_prior[:, 1, 1]), states_prior[:, 1] - np.sqrt(cov_prior[:, 1, 1]), facecolor='g', alpha=0.5)
# plt.plot(t_post, states_post[:, 1], '-b')
# plt.fill_between(t_post, states_post[:, 1] + np.sqrt(cov_post[:, 1, 1]), states_post[:, 1] - np.sqrt(cov_post[:, 1, 1]), facecolor='b', alpha=0.5)
# plt.plot(t_obs, observations[:, 1], 'xk')

# plt.subplot(2, 2, 3)
# plt.plot(t_sim, states_sim[:, 2], '--r')
# plt.plot(t_prior, states_prior[:, 2], '-g')
# plt.fill_between(t_prior, states_prior[:, 2] + np.sqrt(cov_prior[:, 2, 2]), states_prior[:, 2] - np.sqrt(cov_prior[:, 2, 2]), facecolor='g', alpha=0.5)
# plt.plot(t_post, states_post[:, 2], '-b')
# plt.fill_between(t_post, states_post[:, 2] + np.sqrt(cov_post[:, 2, 2]), states_post[:, 2] - np.sqrt(cov_post[:, 2, 2]), facecolor='b', alpha=0.5)
# plt.plot(t_obs, observations[:, 2], 'xk')

# plt.subplot(2, 2, 4)
# plt.plot(t_sim, states_sim[:, 3], '--r')
# plt.plot(t_prior, states_prior[:, 3], '-g')
# plt.fill_between(t_prior, states_prior[:, 3] + np.sqrt(cov_prior[:, 3, 3]), states_prior[:, 3] - np.sqrt(cov_prior[:, 3, 3]), facecolor='g', alpha=0.5)
# plt.plot(t_post, states_post[:, 3], '-b')
# plt.fill_between(t_post, states_post[:, 3] + np.sqrt(cov_post[:, 3, 3]), states_post[:, 3] - np.sqrt(cov_post[:, 3, 3]), facecolor='b', alpha=0.5)
# plt.plot(t_obs, observations[:, 3], 'xk')


# #plt.xlim([0, 0.1])
# #plt.plot(t_obs, observations[0, :, 0], 'xk')
# #plt.plot(t_obs, observations[0, :, 1], 'xk')
# plt.show()

# plt.plot(t_sim, states_sim[:, 0], ':r', label='True State')
# plt.plot(t_obs, observations[:, 0], 'xk', label='Observations')
# plt.plot(t_prior, states_prior[:, 0], '-g', label='Prior')
# plt.plot(t_post, states_post[:, 0], '-b', label='Variational Posterior')
# plt.fill_between(t_prior, states_prior[:, 0] + np.sqrt(states_prior[:, 2]), states_prior[:, 0] - np.sqrt(states_prior[:, 2]), facecolor='g', alpha=0.5)
# plt.fill_between(t_post, states_post[:, 0] + np.sqrt(states_post[:, 2]), states_post[:, 0] - np.sqrt(states_post[:, 2]), facecolor='b', alpha=0.5)
# plt.ylim(0, 600)
# plt.ylabel('$X_1$')
# plt.legend(bbox_to_anchor=(0.2, 1.0), loc='lower left')
