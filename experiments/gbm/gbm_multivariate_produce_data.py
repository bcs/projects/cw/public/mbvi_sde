# test the sde variational engine with ou process example
import sys
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
from scipy.stats import special_ortho_group
from pathlib import Path
from pymbvi.variational_engine import VariationalEngine
from pymbvi.models.observation.kinetic_obs_model import MultiGaussObs
import pymbvi.models.diffusion.specific_models as vi_model
from pymbvi.optimize import robust_gradient_descent
from pyssa.sde import Simulator
import pyssa.ssa as ssa
from src.models.gbm import GBM

# fix seed
np.random.seed(2002041754)

# construct a correlation structure
dim = 4
Q = special_ortho_group.rvs(dim)
eig = np.array([0.01, 0.02, 0.01, 0.01])**2
D = Q.T @ np.diag(eig) @ Q
R = np.linalg.cholesky(D)
std = np.outer(np.sqrt(np.diag(D)), np.sqrt(np.diag(D)))

# initialize model
gamma = np.array([1.0, 2.64, 1.5, 3.2])*1e-4
model = GBM(gamma, R)

# set up an observation model
sigma_obs = np.array([0.01, 0.01, 0.01, 0.01])
obs_model = MultiGaussObs(sigma_obs)

# construct sde engine
timestep = 1e-2
simulator = Simulator(model, timestep)

# set target times
t_stop = np.array([90, 180, 360, 720])
N = len(t_stop)
output = {}

for i in range(len(t_stop)):

    # perform simulation
    initial = np.ones([dim])
    tspan = np.array([0.0, t_stop[i]])
    trajectory = simulator.simulate(initial, tspan)
    t_sim = trajectory['times']
    states_sim = trajectory['states']

    # produce observations 
    delta_t = 7
    t_obs = np.arange(delta_t, tspan[1], delta_t)
    print(len(t_obs))
    observations = ssa.discretize_trajectory(trajectory, t_obs, obs_model=obs_model)

    # store
    output['tspan_{}'.format(i)] = tspan
    output['t_sim_{}'.format(i)] = t_sim
    output['states_sim_{}'.format(i)] = states_sim
    output['t_obs_{}'.format(i)] = t_obs
    output['observations_{}'.format(i)] = observations
    print(observations.shape)

    if i == 3:
        # plot
        plt.plot(t_sim, states_sim[:, 0], '-b')
        plt.plot(t_sim, states_sim[:, 1], '-g')
        plt.plot(t_sim, states_sim[:, 2], '-m')
        plt.plot(t_sim, states_sim[:, 3], '-y')
        plt.plot(t_obs, observations[:, 0], 'xk')
        plt.plot(t_obs, observations[:, 1], 'xk')
        plt.plot(t_obs, observations[:, 2], 'xk')
        plt.plot(t_obs, observations[:, 3], 'xk')
        plt.show()

# store stuff
save_path = Path(__file__).with_suffix('.npz')
np.savez(save_path, gamma=gamma, R=R, sigma_obs=sigma_obs, timestep=timestep, initial=initial, delta_t=delta_t, num_samples=N, **output)