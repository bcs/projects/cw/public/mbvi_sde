import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path
from src import plot_options

# load trajectory
load_path = Path(__file__).parent.joinpath('produce_data.npz')
data = np.load(load_path)
gamma = data['gamma']
mu = data['mu']
sigma = data['sigma']
sigma_obs = data['sigma_obs']
timestep = data['timestep']
initial = data['initial']
tspan = data['tspan']
delta_t = data['delta_t']
t_sim = data['t_sim']
states_sim = data['states_sim']
t_obs = data['t_obs']
observations = data['observations']

# load smoothing data
load_path = Path(__file__).parent.joinpath('smoothing.npz')
data = np.load(load_path)
t_prior = data['t_prior']
states_prior = data['states_prior']
t_post = data['t_post']
states_post = data['states_post']
control_post = data['control_post']

plot_options.set_options(width=11/2.54, height=5/2.54)

# plot
fig = plt.figure(figsize=(11.0/2.54, 5.0/2.54), dpi=300)
plt.plot(t_sim, states_sim[:, 0], '--k', label='Sample path', alpha=0.5)
plt.plot(t_obs, observations[:, 0], 'xk', markersize=8, label='Measurements')
plt.plot(t_post, states_post[:, 0], '-b', label='Posterior Mean')
plt.fill_between(t_post, states_post[:, 0] + np.sqrt(states_post[:, 1]), states_post[:, 0] - np.sqrt(states_post[:, 1]), facecolor='b', alpha=0.5, label='Posterior STD')
plt.xlim(tspan[0], tspan[1])
plt.legend()

save_path = Path(__file__).parent.joinpath('ou_demo.png')
plt.savefig(save_path, dpi=300)

plt.show()