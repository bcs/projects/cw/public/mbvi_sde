# test the sde variational engine with a multivariate ornstein uhlenbeck process example
import numpy as np
import torch
from pathlib import Path
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
from pymbvi.models.observation.kinetic_obs_model import MultiGaussObs
import pyssa.sde as sde
from pyssa.models.diffusion import Diffusion
from src.models.ou_pocess import OU_Process

# fix seed
np.random.seed(2002050927)

# initialize model
gamma = np.array([0.3])
sigma = np.array([[0.2]])
mu = np.array([1.0])
model = OU_Process(gamma, sigma, mu)

# set up an observation model
sigma_obs = np.array([0.1])
obs_model = MultiGaussObs(sigma_obs)

# construct sde engine
timestep = 1e-2
simulator = sde.Simulator(model, timestep)

# perform simulation
initial = np.array([0.0])
tspan = np.array([0.0, 20.0])
trajectory = simulator.simulate(initial, tspan)

# produce observations 
delta_t = 5.0
t_obs = np.arange(delta_t, tspan[1], delta_t)
observations = sde.discretize_trajectory(trajectory, t_obs, obs_model=obs_model)

# exract data for plotting
t_sim = trajectory['times']
states_sim = trajectory['states']#

# store stuff
save_path = Path(__file__).with_suffix('.npz')
np.savez(save_path, gamma=gamma, mu=mu, sigma=sigma, sigma_obs=sigma_obs, timestep=timestep, initial=initial, tspan=tspan, delta_t=delta_t, t_sim=t_sim, states_sim=states_sim, t_obs=t_obs, observations=observations)

# plot
plt.plot(t_sim, states_sim[:, 0], '-b')
#plt.plot(t_sim, states_sim[:, 1], '-g')
plt.plot(t_obs, observations[:, 0], 'xk')
#plt.plot(t_obs, observations[:, 1], 'xk')
plt.show()