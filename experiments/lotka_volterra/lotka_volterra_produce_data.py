# test the sde variational engine with ou process example

import sys
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
from pathlib import Path
from pymbvi.variational_engine import VariationalEngine
from pymbvi.models.observation.kinetic_obs_model import MultiGaussObs
import pymbvi.models.diffusion.specific_models as vi_model
from pymbvi.optimize import robust_gradient_descent
import pyssa.sde as sde
from pyssa.models.cle_model import CLEModel, CLEModelExtended, CLEModelLV
#import pyssa.ssa as ssa

# fix seed
np.random.seed(19101543)

# set up the model
pre = [[1, 0],
    [1, 1],
    [0, 1]]

post = [[2, 0],
    [0, 2],
    [0, 0]]
rates = np.array([0.5, 0.0025, 0.3]) 
model = CLEModelLV(np.array(pre), np.array(post), np.array(rates))

# reflective boundary condtions ats zero
def reflect(state):
    new_state = np.abs(state)
    return(new_state)

# set up an observation model
sigma_obs = np.array([1.0, 1.0])
obs_model = MultiGaussObs(sigma_obs)

# construct sde engine
timestep = 1e-2
simulator = sde.Simulator(model, timestep, reflect)   

# perform simulation
initial = np.array([71.0, 79.0])
tspan = np.array([0.0, 50])
trajectory = simulator.simulate(initial, tspan)

# produce observations 
delta_t = 10.0
t_obs = np.arange(delta_t, tspan[1], delta_t)
observations = sde.discretize_trajectory(trajectory, t_obs, obs_model=obs_model)

# extract data
t_sim = trajectory['times']
states_sim = trajectory['states']

# store stuff
save_path = Path(__file__).with_suffix('.npz')
np.savez(save_path, timestep=timestep, initial=initial, tspan=tspan, delta_t=delta_t, pre=np.array(pre), post=np.array(post), rates=rates, sigma_obs=sigma_obs, t_sim=t_sim, states_sim=states_sim, t_obs=t_obs, observations=observations)

plt.subplot(2, 1, 1)
plt.plot(t_sim, states_sim[:, 0], ':r', label='True State')
plt.plot(t_obs, observations[:, 0], 'xk', label='Observations')
plt.ylim(0, 600)
plt.ylabel('$X_1$')
gca = plt.gca()
gca.tick_params(labelbottom=False) 

plt.subplot(2, 1, 2)
plt.plot(t_sim, states_sim[:, 1], ':r', label='True State')
plt.plot(t_obs, observations[:, 1], 'xk', label='Observations')
plt.ylim(0, 600)
plt.ylabel('$X_2$')
plt.xlabel('Time in $s$')

plt.show()