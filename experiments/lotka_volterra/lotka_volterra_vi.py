# test the sde variational engine with ou process example

import sys
import numpy as np
import torch
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
from pathlib import Path
from pymbvi.variational_engine import VariationalEngine
from pymbvi.models.diffusion.kinetic_model import KineticModelLinear, KineticModelLinearFA
from pymbvi.models.observation.kinetic_obs_model import MultiGaussObs
import pymbvi.models.diffusion.specific_models as vi_model
from pymbvi.optimize import robust_gradient_descent


# load data from file
load_path = Path(__file__).parent.joinpath('lotka_volterra_produce_data.npz')
data = np.load(load_path)

# extract data
pre = data['pre']
post = data['post']
rates = data['rates']
sigma_obs = data['sigma_obs']
timestep = data['timestep']
initial = data['initial']
tspan = data['tspan']
delta_t = data['delta_t']
t_sim = data['t_sim']
states_sim = data['states_sim']
t_obs = data['t_obs']
observations = data['observations']

# set up variational model
moment_initial = np.zeros(5)
moment_initial[:2] = initial
model = KineticModelLinearFA(moment_initial, pre, post, np.log(rates), tspan)

# set up an observation model
obs_model = MultiGaussObs(sigma_obs)

# compute prior
def odefun(time, state):
    return(model.forward(time, state, np.zeros(model.num_controls()), np.log(rates)))
sol = solve_ivp(odefun, tspan, moment_initial, t_eval=t_sim)
t_prior = sol['t']
states_prior = sol['y'].transpose()

# set up variational engine
options = {'sovler_method': 'Radau'}
vi_engine = VariationalEngine(moment_initial, model, obs_model, t_obs, observations, subsample=20, tspan=tspan, options=options)
vi_engine.initialize()

# run optimization
initial_control = vi_engine.control.copy()
optimal_control = robust_gradient_descent(vi_engine.objective_function, initial_control, iter=100, h=1e-5)[0]

# exract data for plotting
t_post = vi_engine.get_time()
states_post = vi_engine.get_forward()

# store stuff
save_path = Path(__file__).with_suffix('.npz')
np.savez(save_path, t_sim=t_sim, states_sim=states_sim, t_obs=t_obs, observations=observations, t_prior=t_prior, states_prior=states_prior, t_post=t_post, states_post=states_post, control_post=vi_engine.control)
