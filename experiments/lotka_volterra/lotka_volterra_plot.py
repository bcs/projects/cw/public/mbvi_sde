# plot for the bivariate ou process
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
from src import plot_options

# load data from file
load_path = Path(__file__).parent.joinpath('lotka_volterra_vi.npz')
data = np.load(load_path)

# extract data
t_sim = data['t_sim']
states_sim = data['states_sim']
t_obs = data['t_obs']
observations = data['observations']
t_prior = data['t_prior']
states_prior = data['states_prior']
t_post = data['t_post']
states_post = data['states_post']


# # plot
# plt.subplot(2, 1, 1)
# #plt.text(-5.0, 630, '$a)$', fontsize=16)
# plt.plot(t_sim, states_sim[:, 0], ':g')
# plt.plot(t_sim, states_sim[:, 1], ':b')
# plt.plot(t_obs, observations[:, 0], 'xk')
# plt.plot(t_obs, observations[:, 1], 'xk')
# plt.plot(t_prior, states_prior[:, 0], '-g')
# plt.plot(t_prior, states_prior[:, 1], '-b')
# plt.fill_between(t_prior, states_prior[:, 0] + np.sqrt(states_prior[:, 2]), states_prior[:, 0] - np.sqrt(states_prior[:, 2]), facecolor='green', alpha=0.5)
# plt.fill_between(t_prior, states_prior[:, 1] + np.sqrt(states_prior[:, 4]), states_prior[:, 1] - np.sqrt(states_prior[:, 4]), facecolor='blue', alpha=0.5)
# plt.ylim(0, 600)
# plt.ylabel('State')

# plt.subplot(2, 1, 2)
# #plt.text(-5.0, 630, '$b)$', fontsize=16)
# plt.plot(t_sim, states_sim[:, 0], ':g')
# plt.plot(t_sim, states_sim[:, 1], ':b')
# plt.plot(t_obs, observations[:, 0], 'xk')
# plt.plot(t_obs, observations[:, 1], 'xk')
# plt.plot(t_post, states_post[:, 0], '-g', label='Prey')
# plt.plot(t_post, states_post[:, 1], '-b', label='Pred')
# plt.fill_between(t_post, states_post[:, 0] + np.sqrt(states_post[:, 2]), states_post[:, 0] - np.sqrt(states_post[:, 2]), facecolor='green', alpha=0.5)
# plt.fill_between(t_post, states_post[:, 1] + np.sqrt(states_post[:, 4]), states_post[:, 1] - np.sqrt(states_post[:, 4]), facecolor='blue', alpha=0.5)
# plt.xlabel('Time')
# plt.ylabel('State')
# plt.ylim(0, 600)
# plt.legend(loc=[0.61, 0.6])

# plot
plot_options.set_options(height=3)

plt.subplot(2, 1, 1)
plt.plot(t_sim, states_sim[:, 0], ':r', label='True State')
plt.plot(t_obs, observations[:, 0], 'xk', label='Observations')
plt.plot(t_prior, states_prior[:, 0], '-g', label='Prior')
plt.plot(t_post, states_post[:, 0], '-b', label='Variational Posterior')
plt.fill_between(t_prior, states_prior[:, 0] + np.sqrt(states_prior[:, 2]), states_prior[:, 0] - np.sqrt(states_prior[:, 2]), facecolor='g', alpha=0.5)
plt.fill_between(t_post, states_post[:, 0] + np.sqrt(states_post[:, 2]), states_post[:, 0] - np.sqrt(states_post[:, 2]), facecolor='b', alpha=0.5)
plt.ylim(0, 600)
plt.ylabel('$X_1$')
plt.legend(bbox_to_anchor=(0.18, 1.0), loc='lower left')
gca = plt.gca()
gca.tick_params(labelbottom=False) 

plt.subplot(2, 1, 2)
plt.plot(t_sim, states_sim[:, 1], ':r', label='True State')
plt.plot(t_obs, observations[:, 1], 'xk', label='Observations')
plt.plot(t_prior, states_prior[:, 1], '-g', label='Prior')
plt.plot(t_post, states_post[:, 1], '-b', label='Variational Posterior')
plt.fill_between(t_prior, states_prior[:, 1] + np.sqrt(states_prior[:, 4]), states_prior[:, 1] - np.sqrt(states_prior[:, 4]), facecolor='g', alpha=0.5)
plt.fill_between(t_post, states_post[:, 1] + np.sqrt(states_post[:, 4]), states_post[:, 1] - np.sqrt(states_post[:, 4]), facecolor='b', alpha=0.5)
plt.ylim(0, 600)
plt.ylabel('$X_2$')
plt.xlabel('Time in $s$')

fig = plt.gcf()
fig.subplots_adjust(left=0.1, bottom=0.1, right=0.9, top=0.7)

save_path = Path(__file__).parent.joinpath('lotka_volterra.pdf')
plt.savefig(save_path)

plt.show()