# test the sde variational engine with ou process example
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
from pymbvi.models.observation.kinetic_obs_model import SingleGaussObs
from pyssa.sde import Simulator
from pyssa.models.diffusion import Diffusion
import pyssa.ssa as ssa
from src.models.bistable import Bistable

# fix seed
np.random.seed(9134874+3)

# # create a custom model as a descendant of Diffusion
# class Bistable(Diffusion):

#     def __init__(self, sigma):
#         self.sigma = sigma

#     def eval(self, state, time):
#         drift = 4.0*state*(1.0-state**2)
#         return(drift, self.sigma)

#     def get_dimension(self):
#         return((1, 1))

# initialize model
sigma = np.array([0.5])
model = Bistable(sigma)

# set up an observation model
sigma_obs = np.sqrt(0.04)
obs_model = SingleGaussObs(sigma_obs, 1, 0)

# construct sde engine
timestep = 1e-2
simulator = Simulator(model, timestep)

# perform simulation
initial = np.array([1.0])
tspan = np.array([0.0, 613.5]) # burn in to capture transition
trajectory = simulator.simulate(initial, tspan)
tspan = np.array([0.0, 8.0]) # burn in to capture transition
trajectory = simulator.simulate(initial, tspan)
t_sim = trajectory['times']
states_sim = trajectory['states']

# produce observations 
delta_t = 1.0
t_obs = np.arange(delta_t, tspan[1], delta_t)
observations = ssa.discretize_trajectory(trajectory, t_obs, obs_model=obs_model)

# save data
save_path = Path(__file__).with_suffix('.npz')
np.savez(save_path, sigma=sigma, sigma_obs=sigma_obs, timestep=timestep, initial=initial, tspan=tspan, delta_t=delta_t, t_sim=t_sim, states_sim=states_sim, t_obs=t_obs, observations=observations)

# plot
plt.plot(t_sim, states_sim, '-r')
plt.plot(t_obs, observations, 'xk')
plt.show()
