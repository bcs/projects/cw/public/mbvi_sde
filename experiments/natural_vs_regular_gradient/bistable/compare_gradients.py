import numpy as np
from pathlib import Path
import os
import re
import matplotlib.pyplot as plt

# get files in directory
file_list = os.listdir(Path(__file__).parent)

# get natural gradients
ng_ind_list = []
for file_name in file_list:
    tmp = re.search('(?<=gp_natural_gradient_ind_)\d*', file_name)
    if tmp is not None:
        ng_ind_list.append(int(file_name[tmp.start():tmp.end()]))
ng_ind_list = sorted(ng_ind_list)

# get regular gradients
rg_ind_list = []
for file_name in file_list:
    tmp = re.search('(?<=gp_regular_gradient_ind_)\d*', file_name)
    if tmp is not None:
        rg_ind_list.append(int(file_name[tmp.start():tmp.end()]))
rg_ind_list = sorted(rg_ind_list)

# get joint
ind_list = list(set(ng_ind_list)&set(rg_ind_list))

# load ng elbos
ng_elbo = []
for ind in ind_list:
    load_path = Path(__file__).parent.joinpath(f'gp_natural_gradient_ind_{ind}.npz')
    ng_elbo.append(np.load(load_path)['elbo_hist'])
ng_elbo = np.mean(np.stack(ng_elbo), axis=0)

# load rg elbos
rg_elbo = []
for ind in ind_list:
    load_path = Path(__file__).parent.joinpath(f'gp_regular_gradient_ind_{ind}.py.npz')
    rg_elbo.append(np.load(load_path)['elbo_hist'])
rg_elbo = np.mean(np.stack(rg_elbo), axis=0)

# plot
plt.plot(np.log(ng_elbo), '-r', label='Natural Gradient')
plt.plot(np.log(rg_elbo), '-b', label='Regular Gradient')
plt.ylabel('log Obj')
plt.xlabel('Iteration')
plt.legend()
plt.show()