"""
Produce sample data for a bistable toy model
"""
import sys
import os
import numpy as np
import torch
from pathlib import Path
import pyssa.sde as sde
from pyssa.models.diffusion import Diffusion
from pymbvi.models.observation.kinetic_obs_model import SingleGaussObs
from src.models.bistable import Bistable

# fix seed
np.random.seed(2007231439)

# number of trajectories
num_samples = 100

# initialize model
sigma = np.array([0.5])
model = Bistable(sigma)

# construct sde engine
timestep = 1e-2
simulator = sde.Simulator(model, timestep)

# initial simulation to get time step
initial = np.array([0.0])
tspan = np.array([0.0, 20]) 
trajectory = simulator.simulate(initial, tspan)
t_sim = trajectory['times']

# set up observation model
sigma_obs = np.sqrt(0.04)
obs_model = SingleGaussObs(sigma_obs, 1, 0)
delta_t = 1.0
t_obs = np.arange(delta_t, tspan[1]-0.5*delta_t, delta_t)

# set up data store
data = {}
data['num_samples'] = num_samples
data['sigma'] = sigma
data['sigma_obs'] = sigma_obs
data['timestep'] = timestep
data['initial'] = initial
data['tspan'] = tspan
data['delta_t'] = delta_t
data['t_sim'] = t_sim
data['t_obs'] = t_obs

# simulate a number of trajectories
msg = "Sampling trajectory {0} of {1}"
for i in range(num_samples):
    if i % 100 == 0:
        print(msg.format(i, num_samples))
    trajectory = simulator.simulate(initial, tspan)
    observations = sde.discretize_trajectory(trajectory, t_obs, obs_model=obs_model)
    data[f'states_sim_{i}'] = trajectory['states']
    data[f'observations_{i}'] = observations

# store stuff
save_path = Path(__file__).with_suffix('.npz')
np.savez(save_path, **data)
