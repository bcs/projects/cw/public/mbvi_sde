# test the sde variational engine with ou process example
import sys
import numpy as np
import torch
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
from pathlib import Path
import time
import multiprocessing as mp
from pymbvi.variational_engine import VariationalEngine
from pymbvi.models.observation.kinetic_obs_model import SingleGaussObs
from pymbvi.models.diffusion.autograd_model import FullAutogradModel
from pymbvi.forward_backward import ForwardBackward
import pymbvi.models.diffusion.specific_models as vi_model
from pymbvi.optimize import robust_gradient_descent
from examples.bistable.models import BistableGP

def process(ind):

    # load data from file
    load_path = Path(__file__).parent.joinpath('produce_data.npz')
    data = np.load(load_path)

    # extract data
    sigma = data['sigma']
    sigma_obs = data['sigma_obs']
    timestep = data['timestep']
    initial = data['initial']
    tspan = data['tspan']
    delta_t = data['delta_t']
    t_sim = data['t_sim']
    t_obs = data[f't_obs']
    states_sim = data[f'states_sim_{ind}']
    observations = data[f'observations_{ind}']

    # set up an observation model
    obs_model = SingleGaussObs(sigma_obs, 1, 0)

    # set up bistable toy model
    moment_initial = np.zeros(2)
    moment_initial[0] = initial
    rates = np.array([sigma])
    model = BistableGP(moment_initial, rates, tspan, torch.tensor(sigma).reshape(1, 1), gradient_mode='natural')

    # set up variational engine
    vi_engine = VariationalEngine(moment_initial, model, obs_model, t_obs, observations, subsample=20, tspan=tspan)
    vi_engine.initialize()

    # optimize controls
    initial_control = vi_engine.control.copy()
    start_time = time.time()
    sol = robust_gradient_descent(vi_engine.objective_function, initial_control, iter=200, h=1e-3)
    end_time = time.time()
    optimal_control = sol[0]
    elbo_hist = sol[3]

    # exract data for plotting
    t_post = vi_engine.get_time()
    states_post = vi_engine.get_forward()

    # store stuff
    file_name = Path(__file__).name.replace('.py', f'_ind_{ind}.py')
    save_path = Path(__file__).parent.joinpath(file_name)
    data = {}
    data['t_sim'] = t_sim, 
    data['states_post'] = states_post
    data['control_post'] = vi_engine.control
    data['elbo_hist'] = elbo_hist
    data['run_time'] = end_time-start_time
    np.savez(save_path, **data)


if __name__ == '__main__':

    # load data from file
    load_path = Path(__file__).parent.joinpath('produce_data.npz')
    num_samples = np.load(load_path)['num_samples']

    # construct argument
    ind_list = [i for i in range(num_samples)]

    # parallel processing
    with mp.Pool(50) as pool:
        pool.map(process, ind_list)

