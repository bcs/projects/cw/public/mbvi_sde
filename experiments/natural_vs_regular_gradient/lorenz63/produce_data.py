"""
Produce sample data for a bistable toy model
"""
import numpy as np
import torch
from pathlib import Path
from scipy.stats import special_ortho_group
import pyssa.sde as sde
from pymbvi.models.observation.kinetic_obs_model import MultiGaussObs
from src.models.lorenz63 import SimLorenz63
import matplotlib.pyplot as plt

# fix seed
np.random.seed(2101241718)

# number of trajectories
num_samples = 100

# initialize model
theta = np.array([10.0, 28.0, 2.666])
sigma = np.sqrt(10.0)*np.eye(3)
model = SimLorenz63(theta, sigma)

# construct sde engine
timestep = 5e-4
simulator = sde.Simulator(model, timestep)

# initial simulation to get time grid
initial = np.array([0.0, 0.0, 0.0])
tspan = np.array([0.0, 2.0])
trajectory = simulator.simulate(initial, tspan)
t_sim = trajectory['times']

# set up an observation model
sigma_obs = np.sqrt(2) * np.ones(3)
obs_model = MultiGaussObs(sigma_obs)
delta_t = 0.2
t_obs = np.arange(delta_t, tspan[1], delta_t)

# set up data store
data = {}
data['num_samples'] = num_samples
data['theta'] = theta
data['sigma'] = sigma
data['sigma_obs'] = sigma_obs
data['timestep'] = timestep
data['initial'] = initial
data['tspan'] = tspan
data['delta_t'] = delta_t
data['t_sim'] = t_sim
data['t_obs'] = t_obs

# simulate a number of trajectories
msg = "Sampling trajectory {0} of {1}"
for i in range(num_samples):
    if i % 100 == 0:
        print(msg.format(i, num_samples))
    trajectory = simulator.simulate(initial, tspan)
    observations = sde.discretize_trajectory(trajectory, t_obs, obs_model=obs_model)
    data[f'states_sim_{i}'] = trajectory['states']
    data[f'observations_{i}'] = observations

# store stuff
save_path = Path(__file__).with_suffix('.npz')
np.savez(save_path, **data)
