"""
Produce sample data for a bistable toy model
"""
import numpy as np
import torch
from pathlib import Path
from scipy.stats import special_ortho_group
import pyssa.sde as sde
from pymbvi.models.observation.kinetic_obs_model import MultiGaussObs
from src.models.gbm import GBM

import matplotlib.pyplot as plt

# fix seed
np.random.seed(2005261729)

# number of trajectories
num_samples = 100

# construct a correlation structure
dim = 4
Q = special_ortho_group.rvs(dim)
eig = np.array([0.01, 0.02, 0.01, 0.01])**2
D = Q.T @ np.diag(eig) @ Q
R = np.linalg.cholesky(D)

# initialize model
gamma = np.array([1.0, 2.64, 1.5, 3.2])*1e-4
model = GBM(gamma, R)

# reflective boundary condtions ats zero
def reflect(state):
    new_state = np.abs(state)
    return(new_state)

# construct sde engine
timestep = 1e-2
simulator = sde.Simulator(model, timestep, reflect)

# # initial simulation to get time step
initial = np.ones([dim])
tspan = np.array([0, 90])
trajectory = simulator.simulate(initial, tspan)
t_sim = trajectory['times']

# set up an observation model
sigma_obs = np.array([0.01, 0.01, 0.01, 0.01])
obs_model = MultiGaussObs(sigma_obs)
delta_t = 7
t_obs = np.arange(delta_t, tspan[1], delta_t)

# set up data store
data = {}
data['num_samples'] = num_samples
data['gamma'] = gamma
data['R'] = R
data['sigma_obs'] = sigma_obs
data['timestep'] = timestep
data['initial'] = initial
data['tspan'] = tspan
data['delta_t'] = delta_t
data['t_sim'] = t_sim
data['t_obs'] = t_obs

# simulate a number of trajectories
msg = "Sampling trajectory {0} of {1}"
for i in range(num_samples):
    if i % 100 == 0:
        print(msg.format(i, num_samples))
    trajectory = simulator.simulate(initial, tspan)
    observations = sde.discretize_trajectory(trajectory, t_obs, obs_model=obs_model)
    data[f'states_sim_{i}'] = trajectory['states']
    data[f'observations_{i}'] = observations

# store stuff
save_path = Path(__file__).with_suffix('.npz')
np.savez(save_path, **data)
