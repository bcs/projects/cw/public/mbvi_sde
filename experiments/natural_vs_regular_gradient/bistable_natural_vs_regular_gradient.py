# test the sde variational engine with ou process example
import numpy as np
import torch
import matplotlib.pyplot as plt
from pathlib import Path
from scipy.integrate import solve_ivp
from pymbvi.variational_engine import VariationalEngine
from pymbvi.models.observation.kinetic_obs_model import SingleGaussObs
from pymbvi.models.diffusion.autograd_model import FullAutogradModel
from pymbvi.models.diffusion.gp_approximation import GpApproximationAutograd
from pymbvi.forward_backward import ForwardBackward
from pymbvi.models.diffusion.discretized_model import Dicrete1D
import pymbvi.models.diffusion.specific_models as vi_model
from pymbvi.optimize import robust_gradient_descent
from pyssa.sde import Simulator
from pyssa.models.diffusion import Diffusion
import pyssa.ssa as ssa

np.random.seed(2002062329)


class BistableGP(GpApproximationAutograd):
    """
    GP approximation
    """

    def get_drift_stats(self, m, M, rates):
        """
        Compute required expectations of the drift
        """
        # get higher order moments
        hom = self.gauss_moments(m, M)
        # E[a(x)]
        a = 4*m - 4*hom[0]
        # E[a(x) x]
        a_x = 4*(M+m**2) - 4*hom[1]
        # E[a(x) a(x)]
        a_a = 16*(M+m**2) - 32*hom[1] + 16*hom[2]
        return(a, a_x, a_a)

    def gauss_moments(self, m, M):
        """
        Gaussian closure for E[X^3] and E[X^4] and E[X^6]
        """
        hom = torch.zeros(3)
        hom[0] = m**3 + 3*m*M
        hom[1] = m**4 + 6*m**2*M + 3*M**2
        hom[2] = m**6 + 15*M*m**4 + 15*3*M**2*m**2 + 15*M**3
        return(hom)

    def get_diffusion(self, rates):
        return(rates**2)


# load data from file
load_path = Path(__file__).parent.joinpath('bistable_produce_data.npz')
data = np.load(load_path)

# extract data
sigma = data['sigma']
sigma_obs = data['sigma_obs']
timestep = data['timestep']
initial = data['initial']
tspan = data['tspan']
delta_t = data['delta_t']
t_sim = data['t_sim']
states_sim = data['states_sim']
t_obs = data['t_obs']
observations = data['observations']

# set up an observation model
obs_model = SingleGaussObs(sigma_obs, 1, 0)

# set up bistable toy model
moment_initial = np.zeros(2)
moment_initial[0] = initial
rates = np.array([sigma])
model_natural = BistableGP(moment_initial, rates, tspan, gradient_mode='natural')
model_regular = BistableGP(moment_initial, rates, tspan, gradient_mode='regular')

# compute prior
def odefun(time, state):
    return(model_natural.forward(time, state, np.zeros(model_natural.num_controls()), rates))
sol = solve_ivp(odefun, tspan, moment_initial, t_eval=t_sim)
t_prior = sol['t']
states_prior = sol['y'].transpose()

# set up variational engine
vi_natural = VariationalEngine(moment_initial, model_natural, obs_model, t_obs, observations, subsample=20, tspan=tspan)
vi_natural.initialize()
vi_regular = VariationalEngine(moment_initial, model_regular, obs_model, t_obs, observations, subsample=20, tspan=tspan)
vi_regular.initialize()

# optimize controls
num_iter = 100
num_samples = 100
learning_rate = 1e-6
elbo_hist_natural = np.zeros((num_samples, num_iter))
elbo_hist_regular = np.zeros((num_samples, num_iter))
for i in range(num_samples):
    initial_control = 3*np.random.standard_normal(vi_natural.control.shape)
    initial_control[initial_control>6] = 6
    initial_control[initial_control<-6] = -6
    elbo_hist_natural[i] = robust_gradient_descent(vi_natural.objective_function, initial_control, iter=num_iter, h=learning_rate)[3]
    elbo_hist_regular[i] = robust_gradient_descent(vi_regular.objective_function, initial_control, iter=num_iter, h=learning_rate)[3]


    # exract data for plotting
    t_post = vi_natural.get_time()
    states_post = vi_natural.get_forward()

    # store stuff
    save_path = Path(__file__).with_suffix('.npz')
    #file_name = 'bistable_vi_gp_natural2'
    np.savez(save_path, t_sim=t_sim, states_sim=states_sim, t_obs=t_obs, observations=observations, t_prior=t_prior, states_prior=states_prior, t_post=t_post, states_post=states_post, control_post=vi_natural.control, elbo_hist_natural=elbo_hist_natural, elbo_hist_regular=elbo_hist_regular)

