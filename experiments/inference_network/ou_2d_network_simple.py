# test the sde variational engine with a multivariate ornstein uhlenbeck process example
import numpy as np
import torch
import torch.nn as nn
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
from pathlib import Path
from pymbvi.variational_engine import VariationalEngine
from pymbvi.models.observation.kinetic_obs_model import MultiGaussObs
from pymbvi.models.diffusion.autograd_model import FullAutogradModel
import pymbvi.models.diffusion.specific_models as vi_model
import pymbvi.optimize as opt
from src.models.ou_pocess import MOU

# load data from file
load_path = Path(__file__).parent.joinpath('ou_2d_network_produce_data.npz')
data = np.load(load_path)

# extract data
gamma = data['gamma']
mu = data['mu']
sigma = data['sigma']
sigma_obs = data['sigma_obs']
timestep = data['timestep']
initial = data['initial']
tspan = data['tspan']
delta_t = data['delta_t']
t_sim = data['t_sim']
states_sim = data['states_sim']
t_obs = data['t_obs']
observations = data['observations']

# set up ou process model
moment_initial = np.zeros(5)
moment_initial[:2] = initial
model = MOU(moment_initial, tspan, torch.tensor(gamma), torch.tensor(sigma), torch.tensor(mu), gradient_mode='regular')

# observation model
obs_model = MultiGaussObs(sigma_obs)

# get parameters for nn
num_samples = observations.shape[0]
num_obs = len(t_obs)
num_control = 10
subsample = 10
control_dim = 6
control_shape = (num_obs+1, num_control, control_dim)

# set Up a network
inference_net = nn.Sequential(
    nn.Linear(18, 20),
    nn.ReLU(),
    nn.Linear(20, 40),
    nn.ReLU(),
    nn.Linear(40, 80),
    nn.ReLU(),
    nn.Linear(80, 160),
    nn.ReLU(),
    nn.Linear(160, 300),
    nn.ReLU(),
    nn.Linear(300, 600),
)

# load inference network
checkpoint = False
if checkpoint:
    load_path = Path(__file__).with_suffix('.npz')
    state_dict = torch.load(load_path)
    inference_net.load_state_dict(state_dict)

# set up variational engine
vi_engine = VariationalEngine(moment_initial, model, obs_model, t_obs, observations[0], num_controls=num_control, subsample=subsample, tspan=tspan)
vi_engine.initialize()


# autograd wrapper for variational engine
class MBVIFunction(torch.autograd.Function):

    @staticmethod
    def forward(ctx, control, obs_data):
        # convert to numpy and reshape
        control_np = control.detach().numpy().reshape(control_shape)
        obs_data_np = obs_data.numpy().reshape(vi_engine.obs_data.shape)
        # evlaute output and gradient using the variational engine
        vi_engine.obs_data = obs_data_np
        elbo_np, grad_np = vi_engine.objective_function(control_np)
        # convert back to torch, store, return
        elbo = torch.tensor(elbo_np)
        grad = torch.from_numpy(grad_np.flatten())
        ctx.save_for_backward(grad)
        return(elbo)

    @staticmethod
    def backward(ctx, grad_output):
        grad_control = ctx.saved_tensors[0]*grad_output
        grad_obs_data = None
        return(grad_control, grad_obs_data)


# neural net module wrapper for variational eninge

class MBVI(nn.Module):
    def __init__(self):
        super(MBVI, self).__init__()

    def forward(self, control):
        return(MBVIFunction.apply(control))


# set up an optimizer
optimizer = torch.optim.Adam(inference_net.parameters(), weight_decay=0.001)

data = torch.tensor(observations.reshape((num_samples, -1)))


msg = 'Running loss in epeoch {0} iteration {1}: {2}'
save_path = Path(__file__).with_suffix('.npz')
for epoch in range(50):  # loop over the dataset multiple times

    running_loss = 0.0
    for i, obs_data in enumerate(data):

        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize
        control = inference_net(obs_data)
        loss = MBVIFunction.apply(control, obs_data)
        loss.backward()

        optimizer.step()

        # print statistics
        running_loss += loss.item()
        print(msg.format(epoch, i, running_loss/(i+1)))

        # store stuff
        torch.save(inference_net.state_dict(), save_path)



print('Finished Training')
