# test the sde variational engine with a multivariate ornstein uhlenbeck process example

import sys
import numpy as np
import torch
import torch.nn as nn
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
from pathlib import Path
from pymbvi.variational_engine import VariationalEngine
from pymbvi.models.observation.kinetic_obs_model import MultiGaussObs
from pymbvi.models.diffusion.autograd_model import FullAutogradModel
import pymbvi.models.diffusion.specific_models as vi_model
import pymbvi.optimize as opt
from src.models.ou_pocess import MOU


# load data from file
load_path = Path(__file__).parent.joinpath('ou_2d_produce_data.npz')
data = np.load(load_path)

# extract data
gamma = data['gamma']
mu = data['mu']
sigma = data['sigma']
sigma_obs = data['sigma_obs']
timestep = data['timestep']
initial = data['initial']
tspan = data['tspan']
delta_t = data['delta_t']
t_sim = data['t_sim']
states_sim = data['states_sim']
t_obs = data['t_obs']
observations = data['observations']

# set up ou process model
moment_initial = np.zeros(5)
moment_initial[:2] = initial
model = MOU(moment_initial, tspan, torch.tensor(gamma), torch.tensor(sigma), torch.tensor(mu), gradient_mode='natural')

# compute prior
def odefun(time, state):
    return(model.forward(time, state, np.zeros(model.num_controls()), np.zeros(2)))
sol = solve_ivp(odefun, tspan, moment_initial, t_eval=t_sim)
states_prior = sol['y'].transpose()
t_prior = sol['t']

# observation model
obs_model = MultiGaussObs(sigma_obs)

# set up variational engine
vi_engine = VariationalEngine(moment_initial, model, obs_model, t_obs, observations, num_controls=10, subsample=10, tspan=tspan)
vi_engine.initialize()

# optimize controls
initial_control = vi_engine.control.copy()
optimal_control = opt.robust_gradient_descent(vi_engine.objective_function, initial_control, iter=100, h=1e-3)[0]

# exract data for plotting('
t_post = vi_engine.get_time()
states_post = vi_engine.get_forward()

# store stuff
save_path = Path(__file__).with_suffix('.npz')
np.savez(save_path, t_sim=t_sim, states_sim=states_sim, t_obs=t_obs, observations=observations, t_prior=t_prior, states_prior=states_prior, t_post=t_post, states_post=states_post, control_post=vi_engine.control)