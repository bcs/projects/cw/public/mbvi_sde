# test the sde variational engine with a multivariate ornstein uhlenbeck process example
import numpy as np
import torch
import torch.nn as nn
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
from pathlib import Path
from pymbvi.variational_engine import VariationalEngine
from pymbvi.models.observation.kinetic_obs_model import MultiGaussObs
from pymbvi.models.diffusion.autograd_model import FullAutogradModel
import pymbvi.models.diffusion.specific_models as vi_model
import pymbvi.optimize as opt
from src import plot_options
from src.models.ou_pocess import MOU


#rcParams['figure.autolayout'] = True
#rcParams['figure.constrained_layout.use'] = True

# load data from file
load_path = Path(__file__).parent.joinpath('ou_2d_network_produce_data.npz')
data = np.load(load_path)

# extract data
gamma = data['gamma']
mu = data['mu']
sigma = data['sigma']
sigma_obs = data['sigma_obs']
timestep = data['timestep']
initial = data['initial']
tspan = data['tspan']
delta_t = data['delta_t']
t_sim = data['t_sim']
states_sim = data['states_sim']
t_obs = data['t_obs']
observations = data['observations']

# load data from file
load_path = Path(__file__).parent.joinpath('ou_2d_linear.npz')
data = np.load(load_path)

# extract data
t_sim_test = data['t_sim']
states_sim_test = data['states_sim']
t_obs_test = data['t_obs']
observations_test = data['observations']
t_prior = data['t_prior']
states_prior = data['states_prior']
t_post_test = data['t_post']
states_post_test = data['states_post']

# set up inference network
inference_net = nn.Sequential(
    nn.Linear(18, 20),
    nn.ReLU(),
    nn.Linear(20, 40),
    nn.ReLU(),
    nn.Linear(40, 80),
    nn.ReLU(),
    nn.Linear(80, 160),
    nn.ReLU(),
    nn.Linear(160, 300),
    nn.ReLU(),
    nn.Linear(300, 600)
)

# load inference network
load_path = Path(__file__).parent.joinpath('ou_2d_network_simple.npz')
state_dict = torch.load(load_path)
inference_net.load_state_dict(state_dict)
inference_net.eval()

# set up ou process model
moment_initial = np.zeros(5)
moment_initial[:2] = initial
model = MOU(moment_initial, tspan, torch.tensor(gamma), torch.tensor(sigma), torch.tensor(mu), gradient_mode='regular')

# observation model
obs_model = MultiGaussObs(sigma_obs)

# get parameters for nn
ind = 10
num_samples = observations.shape[0]
num_obs = len(t_obs)
num_control = 10
subsample = 10
control_dim = 6
control_shape = (num_obs+1, num_control, control_dim)
data = torch.tensor(observations.reshape((num_samples, -1)))

# set up variational engine
vi_engine = VariationalEngine(moment_initial, model, obs_model, t_obs, observations[ind], num_controls=num_control, subsample=subsample, tspan=tspan)
vi_engine.initialize()

# evaluate for test data
obs_data = torch.tensor(observations_test.flatten())
control = inference_net(obs_data)
control_np = control.detach().numpy().reshape(control_shape)
vi_engine.objective_function(control_np)

# get posterior
t_post = vi_engine.get_time()
states_post = vi_engine.get_forward()

# plot
plot_options.set_options(height=2)

# plt.subplot(2, 1, 1)
# plt.text(-3.0, 4.0, '$a)$', fontsize=16)
# plt.plot(t_sim, states_sim[ind, :, 0], ':g')
# plt.plot(t_sim, states_sim[ind, :, 1], ':b')
# plt.plot(t_obs, observations[ind, :, 0], 'xk')
# plt.plot(t_obs, observations[ind, :, 1], 'xk')
# plt.plot(t_prior, states_prior[ind, :, 0], '-g')
# plt.plot(t_prior, states_prior[ind, :, 1], '-b')
# plt.fill_between(t_prior, states_prior[ind, :, 0] + np.sqrt(states_prior[ind, :, 2]), states_prior[ind, :, 0] - np.sqrt(states_prior[ind, :, 2]), facecolor='green', alpha=0.5)
# plt.fill_between(t_prior, states_prior[ind, :, 1] + np.sqrt(states_prior[ind, :, 4]), states_prior[ind, :, 1] - np.sqrt(states_prior[ind, :, 4]), facecolor='blue', alpha=0.5)
# plt.ylabel('State', fontsize=14)

plt.subplot(2, 1, 1)
#plt.text(-3.0, 4.0, '$a)$', fontsize=16)
#plt.plot(t_sim_test, states_sim_test[:, 0], ':r', label='True State')
plt.plot(t_obs_test, observations_test[:, 0], 'xk', label='Observations')
#plt.plot(t_prior, states_prior[:, 0], '-g', label='Prior')
plt.plot(t_post, states_post[:, 0], '-b', label='Predicted Posterior')
plt.plot(t_post_test, states_post_test[:, 0], '-m', label='True Posterior')
#plt.plot(t_prior, states_prior[ind, :, 1], '-b')
#plt.fill_between(t_prior, states_prior[:, 0] + np.sqrt(states_prior[:, 2]), states_prior[:, 0] - np.sqrt(states_prior[:, 2]), facecolor='g', alpha=0.5)
plt.fill_between(t_post, states_post[:, 0] + np.sqrt(states_post[:, 2]), states_post[:, 0] - np.sqrt(states_post[:, 2]), facecolor='b', alpha=0.5)
plt.fill_between(t_post_test, states_post_test[:, 0] + np.sqrt(states_post_test[:, 2]), states_post_test[:, 0] - np.sqrt(states_post_test[:, 2]), facecolor='m', alpha=0.5)
plt.xlim([0, 20])
plt.ylim([-1.5, 0.0])
plt.ylabel('$X_1$')
plt.legend(bbox_to_anchor=(0.18, 1.0), loc='lower left')
gca = plt.gca()
gca.tick_params(labelbottom=False) 
gca.yaxis.set_label_coords(-0.1, 1.0)

plt.subplot(2, 1, 2)
#plt.text(-3.0, 4.0, '$a)$', fontsize=16)
#plt.plot(t_sim_test, states_sim_test[:, 1], ':r', label='True State')
plt.plot(t_obs_test, observations_test[:, 1], 'xk', label='Observations')
#plt.plot(t_prior, states_prior[:, 1], '-g', label='Prior')
plt.plot(t_post, states_post[:, 1], '-b', label='Predicted Posterior')
plt.plot(t_post_test, states_post_test[:, 1], '-m', label='True Posterior')
#plt.plot(t_prior, states_prior[ind, :, 1], '-b')
#plt.fill_between(t_prior, states_prior[:, 1] + np.sqrt(states_prior[:, 4]), states_prior[:, 1] - np.sqrt(states_prior[:, 4]), facecolor='g', alpha=0.5)
plt.fill_between(t_post, states_post[:, 1] + np.sqrt(states_post[:, 4]), states_post[:, 1] - np.sqrt(states_post[:, 4]), facecolor='b', alpha=0.5)
plt.fill_between(t_post_test, states_post_test[:, 1] + np.sqrt(states_post_test[:, 4]), states_post_test[:, 1] - np.sqrt(states_post_test[:, 4]), facecolor='m', alpha=0.5)
plt.xlim([0, 20])
plt.ylim([0.0, 1.5])
plt.ylabel('$X_2$')
plt.xlabel('Time in $s$')
gca = plt.gca()
gca.yaxis.set_label_coords(-0.1, 1.0)

fig = plt.gcf()
#fig.subplots_adjust(left=0.1, bottom=0.1, right=0.9, top=0.8)

# plt.subplot(2, 1, 2)
# plt.text(-3.0, 4.0, '$b)$', fontsize=16)
# plt.plot(t_sim, states_sim[ind, :, 0], ':g')
# plt.plot(t_sim, states_sim[ind, :, 1], ':b')
# plt.plot(t_obs, observations[ind, :, 0], 'xk')
# plt.plot(t_obs, observations[ind, :, 1], 'xk')
# plt.plot(t_post, states_post[:, 0], '-g', label='$X_1$')
# plt.plot(t_post, states_post[:, 1], '-b', label='$X_2$')
# plt.fill_between(t_post, states_post[:, 0] + np.sqrt(states_post[:, 2]), states_post[:, 0] - np.sqrt(states_post[:, 2]), facecolor='green', alpha=0.5)
# plt.fill_between(t_post, states_post[:, 1] + np.sqrt(states_post[:, 4]), states_post[:, 1] - np.sqrt(states_post[:, 4]), facecolor='blue', alpha=0.5)
# plt.xlabel('Time', fontsize=14)
# plt.ylabel('State', fontsize=14)
# plt.legend(fontsize=14, loc='center right')

save_path = Path(__file__).parent.joinpath('ou_2d_network.pdf')
plt.savefig(save_path)

plt.show()