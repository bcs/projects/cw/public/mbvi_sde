# test the sde variational engine with a multivariate ornstein uhlenbeck process example
import numpy as np
import torch
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
from pathlib import Path
from pymbvi.variational_engine import VariationalEngine
from pymbvi.models.observation.kinetic_obs_model import MultiGaussObs
import pymbvi.models.diffusion.specific_models as vi_model
from pymbvi.optimize import robust_gradient_descent
import pyssa.sde as sde
from src.models.ou_pocess import OU_Process

# fix seed
np.random.seed(2002011959)

# number of trajectories
num_samples = 1000

# initialize model
gamma = np.array([[0.3, 0], [0, 0.4]])
sigma = np.array([[0.2, 0.1],[0.1, 0.15]])
mu = np.array([-1.0, 1.0])
model = OU_Process(gamma, sigma, mu)

# set up an observation model
sigma_obs = np.array([0.2, 0.2])
obs_model = MultiGaussObs(sigma_obs)

# construct sde engine
timestep = 1e-2
simulator = sde.Simulator(model, timestep)

# perform simulation
initial = np.array([0.0, 0.0])
tspan = np.array([0.0, 20.0])
trajectory = simulator.simulate(initial, tspan)
t_sim = trajectory['times']

# produce observations 
delta_t = 2.0
t_obs = np.arange(delta_t, tspan[1], delta_t)

# create output containers
states_sim = np.zeros((num_samples,) + trajectory['states'].shape)
observations = np.zeros((num_samples,) + sde.discretize_trajectory(trajectory, t_obs, obs_model=obs_model).shape)

# simulate a number of trajectories
msg = "Sampling trajectory {0} of {1}"
for i in range(num_samples):
    if i % 1 == 0:
        print(msg.format(i, num_samples))
    trajectory = simulator.simulate(initial, tspan)
    states_sim[i] = trajectory['states']
    observations[i] = sde.discretize_trajectory(trajectory, t_obs, obs_model=obs_model)

# store stuff
save_path = Path(__file__).with_suffix('.npz')
np.savez(save_path, gamma=gamma, mu=mu, sigma=sigma, sigma_obs=sigma_obs, timestep=timestep, initial=initial, tspan=tspan, delta_t=delta_t, t_sim=t_sim, states_sim=states_sim, t_obs=t_obs, observations=observations)

# # plot
# plt.plot(t_sim, states_sim[0, :, 0], '-b')
# plt.plot(t_sim, states_sim[0, :, 1], '-g')
# plt.plot(t_obs, observations[0, :, 0], 'xk')
# plt.plot(t_obs, observations[0, :, 1], 'xk')
# plt.show()