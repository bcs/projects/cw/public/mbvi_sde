This repository contains accompanying code for the paper

Christian Wildner, Heinz Koeppl. Moment-Based Variational Inference for Stochastic Differential Equations.
Proceedings of The 24th International Conference on Artificial Intelligence and Statistics, PMLR 130:1918-1926, 2021.

To use the code, clone the repository and install the required packages via 

pip install -r requirements.txt

It may also be necessary to add the top level of the project to PYTHONPATH. The code was tested with Python 3.8.5.
