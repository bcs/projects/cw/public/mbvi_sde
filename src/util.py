from pathlib import Path

# processing helper functions

def get_project_path():
    project_path = Path(__file__)
    while project_path.name != 'mbvi_sde_code':
        project_path = project_path.parent
    project_path = project_path.parent
    return(project_path)