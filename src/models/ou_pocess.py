import numpy as np
import torch
from pyssa.models.diffusion import Diffusion
from pymbvi.models.diffusion.autograd_model import FullAutogradModel


# create a custom model as a descendant of Diffusion
class OU_Process(Diffusion):

    def __init__(self, gamma, sigma, mu=0.0):
        self.gamma = gamma 
        self.sigma = sigma 
        self.mu = mu

    def eval(self, state, time):
        return(-self.gamma @ (state-self.mu), self.sigma)

    def get_dimension(self):
        return(self.sigma.shape)


class MOU(FullAutogradModel):
    """
    Multivariate Ornstein Uhlenbeck process with linear control of the form
    u(x,t) = sqrt_dff * (u0(t) + u1(t)*x)
    """

    def __init__(self, initial, tspan, drift, diff, mean, diffusion='raw', gradient_mode='natural'):
        """
        Overload constructor to include moment closure choice
        """
        FullAutogradModel.__init__(self, initial, torch.zeros(initial.shape), tspan, gradient_mode=gradient_mode)
        self.num_species = int(-1.5+np.sqrt(2*self.num_states()+2.25))
        self.drift = drift
        self.mean = mean
        if diffusion == 'raw':
            self.diff = diff @ diff.T
        elif diffusion == 'squared':
            self.diff = diff
        self.sqrt_diff = torch.tensor(np.linalg.cholesky(self.diff))
        self.cov_map = self.set_cov_map()

    def forward_torch(self, time, state, control, rates):
        # get first moment and covariance
        m, M = self.get_moments(state)
        u0, u1 = self.get_controls(control)
        # derivative of the mean
        dm = -self.drift @ (m - self.mean) + self.sqrt_diff @ (u0 + u1 @ m)
        # derivative of the covariance
        dM = -self.drift @ M + self.sqrt_diff @ u1 @ M + M.T @ u1.T @ self.sqrt_diff.T + self.diff
        # combine to single vector
        dydt = torch.zeros(state.shape)
        dydt[:self.num_species] = dm
        dydt[self.num_species:] = dM[self.cov_map[0], self.cov_map[1]]
        return(dydt)

    def num_controls(self):
        return(self.num_species*(self.num_species+1))

    def get_moments(self, state):
        """
        Convert state to mean and covariance matrix
        """
        # extract first moment
        m = state[:self.num_species]
        # construct second moment
        tmp = torch.zeros((self.num_species, self.num_species))
        tmp[self.cov_map[0], self.cov_map[1]] = state[self.num_species:]
        M = tmp+tmp.T-torch.diag(tmp.diag())
        # return mean and covariance
        return(m, M)
    
    def get_controls(self, control):
        """
        Convert vector to u0 vector and u1 matrix
        """
        u0 = control[:self.num_species]
        u1 = control[self.num_species:].reshape((self.num_species, self.num_species))
        return(u0, u1)

    def set_cov_map(self):
        """
        Construct a tensor that maps the reduced moments to a full covariance matrix
        """
        cov_map = torch.tril_indices(row=self.num_species, col=self.num_species, offset=0)
        return(cov_map)       

    def kl_contrib_torch(self, time, state, control, rates):
        # get first moment and covariance
        m, M = self.get_moments(state)
        u0, u1 = self.get_controls(control)
        # compute kl contribution
        kl_contrib = u0.T @ u0 + 2*u0.T @ u1 @ m + ((u1.T @ u1) * (M + torch.ger(m, m))).sum()
        return(0.5*kl_contrib)