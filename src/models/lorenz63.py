import numpy as np
import torch
from pyssa.models.diffusion import Diffusion
from pymbvi.models.diffusion.constant_diffusion import ConstantDiffusion


class SimLorenz63(Diffusion):

    def __init__(self, theta, sigma):
        self.theta = theta
        self.sigma = sigma

    def eval(self, state, time):
        drift = np.zeros(3)
        drift[0] = self.theta[0] * (state[1] - state[0])
        drift[1] = self.theta[1] * state[0] - state[1] - state[0]*state[2]
        drift[2] = state[0]*state[1] - self.theta[2]*state[2]
        return(drift, self.sigma)

    def get_dimension(self):
        return(self.sigma.shape)


class Lorenz63(ConstantDiffusion):

    def __init__(self, initial, tspan, theta, sigma, gradient_mode='natural'):
        """
        Overload constructor to include moment closure choice
        """
        ConstantDiffusion.__init__(self, initial, self.get_rates(theta, sigma), tspan, gradient_mode=gradient_mode)
        self.theta = torch.tensor(theta)
        self.sigma = torch.tensor(sigma)
        self.beta = self.set_closure()

    def get_expected_drift(self, m, M, rates):
        M2 = M + torch.ger(m, m)
        #M3 = cl.gaussian_third_order(m, M, self.beta)
        #M200, M210, M220, M100, M110 = cl.gaussian_third_order(m, M, self.beta)
        M210, M200, M100, M110, M220 = self.closure(m, M)
        # E[a(X_t)]
        a = torch.zeros(3) 
        a[0] = -m[0]*rates[0] + m[1]*rates[0]
        a[1] = -M2[2,0] + m[0]*rates[1] - m[1]
        a[2] = M2[1,0] - m[2]*rates[2]
        # E[a(X_t) X_t^T]
        a_x = torch.zeros(3, 3)
        a_x[0,0] = -M2[0,0]*rates[0] + M2[1,0]*rates[0]
        a_x[0,1] = -M2[1,0]*rates[0] + M2[1,1]*rates[0]
        a_x[0,2] = -M2[2,0]*rates[0] + M2[2,1]*rates[0]
        a_x[1,0] = -M200 + M2[0,0]*rates[1] - M2[1,0]
        a_x[1,1] = -M210 + M2[1,0]*rates[1] - M2[1,1]
        a_x[1,2] = -M220 + M2[2,0]*rates[1] - M2[2,1]
        a_x[2,0] = M100 - M2[2,0]*rates[2]
        a_x[2,1] = M110 - M2[2,1]*rates[2]
        a_x[2,2] = M210 - M2[2,2]*rates[2]
        return(a, a_x)

    def get_diff(self, rates):
        return(self.sigma)

    def get_rates(self, theta, sigma):
        rates = theta
        return(rates)

    def set_closure(self):
        beta = torch.tensor(
            [[0, 2, 0],
            [0, 2, 1],
            [0, 2, 2],
            [0, 1, 0],
            [0, 1, 1]])
        return(beta)

    def closure(self, m, M):
        # Third order closure
        M210 = M[1,0]*m[2] + M[2,0]*m[1] + M[2,1]*m[0] + m[0]*m[1]*m[2]
        M200 = 1.0*M[0,0]*m[2] + 2*M[2,0]*m[0] + m[0]**2*m[2]
        M100 = 1.0*M[0,0]*m[1] + 2*M[1,0]*m[0] + m[0]**2*m[1]
        M110 = 2*M[1,0]*m[1] + 1.0*M[1,1]*m[0] + m[0]*m[1]**2
        M220 = 2*M[2,0]*m[2] + 1.0*M[2,2]*m[0] + m[0]*m[2]**2
        cl = torch.stack([M210, M200, M100, M110, M220])
        return(cl)
