import numpy as np
import torch
from pyssa.models.diffusion import Diffusion
from pymbvi.models.diffusion.autograd_model import FullAutogradModel
from pymbvi.models.diffusion.linear_control import MultivariateDiffLinearControl


class GBM(Diffusion):

    def __init__(self, gamma, sigma):
        self.gamma = gamma 
        self.sigma = sigma 

    def eval(self, state, time):
        return(self.gamma*state, self.sigma*np.expand_dims(state, axis=1))

    def get_dimension(self):
        return(self.sigma.shape)


class GBM_Multivariate(MultivariateDiffLinearControl):
    """
    Goemetric Brownian motion with linear control of the form
    u(x,t) = delta * x * (u0(t) + u1(t)*x)
    """

    def __init__(self, initial, rates, tspan):
        """
        Overload constructor to include moment closure choice
        """
        super().__init__(initial, rates, tspan)
        self.num_species = int(-1.5+np.sqrt(2*self.num_states()+2.25))
        self.cov_map = self.set_cov_map()
        self.alpha = self.set_alpha()

    def forward_torch(self, time, state, control, rates):
        """
        forward equation for the ou process
        """
        # get first moment and covariance
        m, M = self.get_moments(state)
        u0, u1 = self.get_controls(control)
        # get closure for third order
        x3 = self.lognorm_closure(m, M, self.alpha).reshape(self.num_species, self.num_species, self.num_species)
        # get parameters
        r, R = self.get_params(rates)
        # derivative of the mean
        dm = r*m + (R@u0)*m + ((R@u1)*M).sum(axis=1)
        # derivative of the covariance
        tmp = M*(r.unsqueeze(1) + (R@u0).unsqueeze(1)) + (x3*(R@u1).unsqueeze(-1)).sum(axis=1)
        dM = tmp + tmp.T + (R@R.T)*M - torch.ger(m, dm) - torch.ger(dm, m)
        # convert to vector
        dydt = torch.zeros(state.shape)
        dydt[:self.num_species] = dm
        dydt[self.num_species:] = dM[self.cov_map[0], self.cov_map[1]]
        return(dydt)

    def stat_tensor_torch(self, forward, rates):
        # get first moment and covariance
        m, M = self.get_moments(forward)
        # u0 vs u0 contribution
        u0_u0 = torch.eye(self.num_species)
        # u1 vs u1 contribution
        u1_u1 = (M.reshape((1, self.num_species, 1, self.num_species))*torch.eye(self.num_species).reshape((self.num_species, 1, self.num_species, 1))).reshape((self.num_species**2, self.num_species**2))
        # u0 vs u1
        u0_u1 = (torch.eye(self.num_species).reshape((self.num_species, self.num_species, 1))*m.reshape((1, 1, self.num_species))).reshape((self.num_species, self.num_species**2))
        # concatenate and return
        tmp = rates.sum()
        stat_tensor = torch.cat([torch.cat([u0_u0, u0_u1], axis=1), torch.cat([u0_u1.T, u1_u1], axis=1)]) + tmp - tmp
        return(0.5*stat_tensor)

    def get_moments(self, state):
        """
        Convert state to mean and covariance matrix
        """
        # extract first moment
        m = state[:self.num_species]
        # construct second moment
        tmp = torch.zeros((self.num_species, self.num_species))
        tmp[self.cov_map[0], self.cov_map[1]] = state[self.num_species:]
        M = tmp+tmp.T-torch.diag(tmp.diag())+torch.ger(m, m)
        # return mean and covariance
        return(m, M)
    
    def get_controls(self, control):
        """
        Convert vector to u0 vector and u1 matrix
        """
        u0 = control[:self.num_species]
        u1 = control[self.num_species:].reshape((self.num_species, self.num_species))
        return(u0, u1)  

    def lognorm_closure(self, m, M, alpha):
        """
        General lognormal closure E[X^\alpha] (multi-index notation)
        """
        tmp1 = m**(2*alpha)/torch.diag(M)**(0.5*alpha)
        power = 0.5 * (alpha.unsqueeze(2) @ alpha.unsqueeze(1)).reshape((alpha.shape[0], -1))
        tmp2 = (M.flatten()/torch.ger(m, m).flatten())**power
        res = tmp1.prod(axis=1)*tmp2.prod(axis=1)
        return(res)

    def get_params(self, rates):
        # first components correspond to log growth rate
        r = torch.exp(rates[:self.num_species])
        # rest corresponds to vectorized R matrix
        R = torch.zeros((self.num_species, self.num_species))
        R[self.cov_map[0], self.cov_map[1]] = rates[self.num_species:]
        return(r, R)

    def set_cov_map(self):
        """
        Construct a tensor that maps the reduced moments to a full covariance matrix
        """
        cov_map = torch.tril_indices(row=self.num_species, col=self.num_species, offset=0)
        return(cov_map)

    def set_alpha(self):
        alpha = torch.zeros(self.num_species, self.num_species, self.num_species, self.num_species)
        for i in range(self.num_species):
            for j in range(self.num_species):
                for k in range(self.num_species):
                    alpha[i, j, k, i] += 1.0
                    alpha[i, j, k, j] += 1.0
                    alpha[i, j, k, k] += 1.0
        return(alpha.reshape((-1, self.num_species)))