import numpy as np
import torch
from pyssa.models.diffusion import Diffusion
from pymbvi.models.diffusion.gp_approximation import GpApproximationAutograd
from pymbvi.models.diffusion.discretized_model import Dicrete1D


class Bistable(Diffusion):

    def __init__(self, sigma):
        self.sigma = sigma

    def eval(self, state, time):
        drift = 4.0*state*(1.0-state**2)
        return(drift, self.sigma)

    def get_dimension(self):
        return((1, 1))


class BistableGP(GpApproximationAutograd):
    """
    GP approximation
    """

    def get_drift_stats(self, m, M, rates):
        """
        Compute required expectations of the drift
        """
        # get higher order moments
        hom = self.gauss_moments(m, M)
        # E[a(x)]
        a = 4*m - 4*hom[0]
        # E[a(x) x]
        a_x = 4*(M+m**2) - 4*hom[1]
        # E[a(x) a(x)]
        a_a = 16*(M+m**2) - 32*hom[1] + 16*hom[2]
        return(a, a_x, a_a)

    def gauss_moments(self, m, M):
        """
        Gaussian closure for E[X^3] and E[X^4] and E[X^6]
        """
        hom = torch.zeros(3)
        hom[0] = m**3 + 3*m*M
        hom[1] = m**4 + 6*m**2*M + 3*M**2
        hom[2] = m**6 + 15*M*m**4 + 15*3*M**2*m**2 + 15*M**3
        return(hom)

    def get_diffusion(self, rates):
        return(rates**2)


class BistableDiscretized(Dicrete1D):

    def drift(self, time, state):
        drift = 4.0*state*(1.0-state**2)
        return(drift)

    def diff(self, time, state):
        diff = self.rates[0]**2*np.ones(state.shape)
        return(diff)