
from matplotlib import rcParams
#rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})

def set_options(width=3.25, height=3):
    small = 8
    medium = 9
    big = 10
    rcParams['text.usetex'] = True
    rcParams['font.family'] = 'Times'
    rcParams['figure.figsize'] = (width, height)
    rcParams['font.size'] = small
    rcParams['axes.titlesize'] = small
    rcParams['axes.labelsize'] = medium
    rcParams['xtick.labelsize'] = small
    rcParams['ytick.labelsize'] = small
    rcParams['legend.fontsize'] = small
    rcParams['figure.titlesize'] = big
    rcParams['lines.linewidth'] = 1.0
    rcParams['lines.markersize'] = 3.0
    rcParams['savefig.bbox'] = 'tight'
    rcParams['savefig.pad_inches'] = 0.01
    return
